# Pygraph 3.2.6

Pygraph è una libreria grafica per Python (versioni: 2.6, 2.7, 3.x) 
rilasciata sotto licenza GPL. (www.gnu.org).

## Implementa:
- un piano cartesiano (pycart.py),
- un plotter di funzioni in una variabile (pyplot.py),
- la geometria della tartaruga (pyturtle.py),
- una geometria interattiva (pyig.py).

Alle 4 librerie ho aggiunto alcuni programmi di esempio e il manuale 
che serve da introduzione alla programmazione con Python e da 
introduzione all'uso delle librerie.

## Installazione
Per installare un qualunque software bisogna essere un po' pratici di 
computer. 
Conviene eventualmente farsi aiutare. 

## Installare Python

La versione più aggiornata di Python può essere scaricata da
[www.python.org](https://www.python.org/).

Python è un software libero ed è distribuito sotto una licenza compatibile
con la licenza GPL quindi è possibile usarlo, copiarlo e distribuirlo senza
restrizioni.

## Installare pygraph

L'ultima versione si può scaricare da:

[bitbucket.org/zambu/pygraph/downloads](https://bitbucket.org/zambu/pygraph/downloads)

1. scaricare pygraph33_rc03.zip;
1. scompattare il file in una cartella di lavoro;
1. eseguire il setup:
  1. Linux / Mac:
     eseguire: "python3 setup.py install --user" 
     (o: "python3.9 setup.py install --user" da superuser);
  1. Windows:
     eseguire: "python3.9 setup.py install --user" 
     con i permessi di amministratore.

## Contenuto del pacchetto

Oltre a questo file, il pacchetto contiene le
tre directory seguenti:

1. doc, la documentazione varia
1. examples, esempi d'uso
       1. functions: esplorazione del concetto di funzione
       1. manual: esempi utilizzati nel manuale
       1. pycart: esempi sul piano cartesiano
       1. pyig: esempi relativi alla geometria interattiva
       1. pyplot: esempi relativi al tracciamento di funzioni
       1. python: esempi sul linguaggio
       1. pyturtle: esempi relativi alla geometria della tartaruga
1. test
1. pygraph, le librerie del progetto:
       1. pycart.py  	un piano cartesiano
       1. pyturtle.py	la grafica della tartaruga
       1. pyplot.py  	grafico di funzioni nel piano, cartesiane e polari
       1. pyig.py    	geometria interattiva
1. Pygraph contiene anche un manuale che contiene un'introduzuione alla 
   programmazione con Python e tutti gli oggetti messi a disposizione 
   da pygraph con (più o meno) semplici esempi.

## Altra documentazione su Python si può trovare in:

1. www.python.org 
1. www.python.it 
1. Un'ottima introduzione all'informatica usando questo linguaggio di
programmazione è il testo: "How to think like a computer scientist: learning
with Python" di Downey Allen tradotto magnificamente in italiano (si trova su
internet partendo dai link precedenti).

## LICENZA
Software e documentazione presenti in pygraph sono di Zambelli Daniele e sono
rilasciati sotto la più recente licenza GPL.

## (NON) GARANZIA
Il software e la documentazione vengono ceduti così come sono, nella speranza
che possano essere di qualche utilità. L'AUTORE NON SI ASSUME ALCUNA
RESPONSABILITÀ PER DANNI, DIRETTI O INDIRETTI, CHE L'UTILIZZATORE POSSA SUBIRE
DALL'USO DEL PRESENTE SOFTWARE O DELLA DOCUMENTAZIONE.

## Feedback
Ogni commento, osservazione, richiesta, critica o complimento è ben accetto. 
Per comunicare con me potete scrivere a:

daniele.zambelli at gmail.com

In particolare spero che qualcuno voglia collaborare allo sviluppo di questa
libreria 

- con l'uso e la produzione di nuovi esempi e di materiale didattico;
- con osservazioni, richieste, segnalazioni di errori o malfunzionamenti;
- con correzione di errori o realizzazione di nuove funzionalità;
- con ...

Verona 26 agosto 2011                                  Daniele Zambelli

