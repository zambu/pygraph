#!/usr/bin/env python3

import os
import sys

from setuptools import setup

os.chdir(os.path.dirname(sys.argv[0]) or ".")

from pygraph import __version__

setup(
    name="pygraph",
    version=__version__,
    description="A python library to draw cartesian plane, plot functions, turtle graphics, interactive geometries.",
    long_description=open("readme.md", "rt").read(),
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/zambu/pygraph",
    author="Daniele Zambelli",
    author_email="daniele.zambelli@gmail.com",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
    ],
    packages=['pygraph'],
    package_dir={'pygraph': 'pygraph',},
    package_data={'pygraph': ['rgb.txt', ],},
    keywords=['turtle', 'geometry'],
)
