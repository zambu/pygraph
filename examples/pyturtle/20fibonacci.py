#!/usr/bin/env python
#--------------------------python-pyturtle--------------19fibonacci.py--#
#                                                                       #
#                             Fibonacci                                 #
#                                                                       #
#--Daniele Zambelli------------------------------------------------2014-#

import pygraph.pyturtle as tg


def quadrato(l):
    for i in range(4):
        tina.forward(l)
        tina.left(90)


def quadfib0(n, l0, l1):
    for cont in range(n):
        quadrato(l1)
        tina.forward(l1)
        tina.left(90)
        tina.forward(l1)
        l0, l1 = l1, l0 + l1


def quadfib1(l0, l1):
    if l1 > 800:
        return
    quadrato(l1)
    tina.forward(l1)
    tina.left(90)
    tina.forward(l1)
    quadfib1(l1, l0 + l1)


def quadfib(l0, l1):
    if l1 > 800:
        tina.color = 'gold'
        return
    quadrato(l1)
    tina.forward(l1)
    tina.left(90)
    tina.forward(l1)
    quadfib(l1, l0 + l1)
    tina.circle(l1, -90)


piano = tg.TurtlePlane(w=1000, h=620)
tina = tg.Turtle(x=220, y=-140, width=5, color='green')
#quadfib0(15, 0, 1)
#quadfib1(0, 1)
quadfib(0, 1)
piano.mainloop()
