#!/usr/bin/env python
# -*- coding: utf-8 -*-
#--------------------------python-pyturtle-----------26barnsleyfern.py--#
#                                                                       #
#                           Barnsley fern                               #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2018--#

##import pygraph.pyturtle as tg
import matplotlib.pyplot as plt
import random
import numpy as np

num = 100000
X = np.zeros(num)
Y = np.zeros(num)
for n in range(num-1):
   r = random.uniform(0, 100)
   if r < 1.0:
       X[n+1] = 0
       Y[n+1] = 0.16*Y[n]
   elif r < 86.0:
       X[n+1] = 0.85*X[n] + 0.04*Y[n]
       Y[n+1] = -0.04*X[n] + 0.85*Y[n]+1.6
   elif r < 93.0:
       X[n+1] = 0.2*X[n] - 0.26*Y[n]
       Y[n+1] = 0.23*X[n] + 0.22*Y[n] + 1.6
   else:
       X[n+1] = -0.15*X[n] + 0.28*Y[n]
       Y[n+1] = 0.26*X[n] + 0.24*Y[n] + 0.44

'''Make a plot'''
plt.figure(figsize = [15,15])
plt.scatter(X,Y,color = 'g',marker = '.')
plt.show()

"""
##X = [0]
##Y = [0]
##for n in range(100000):
##   r = random.uniform(0, 100)
##   if r < 1.0:
##       x = 0
##       y = 0.16*Y[n-1]
##   elif r < 86.0:
##       x = 0.85*X[n-1] + 0.04*Y[n-1]
##       y = -0.04*X[n-1] + 0.85*Y[n-1]+1.6
##   elif r < 93.0:
##       x = 0.2*X[n-1] - 0.26*Y[n-1]
##       y = 0.23*X[n-1] + 0.22*Y[n-1] + 1.6
##   else:
##       x = -0.15*X[n-1] + 0.28*Y[n-1]
##       y = 0.26*X[n-1] + 0.24*Y[n-1] + 0.44
##   X.append(x);Y.append(y)

prev_x = 0
prev_y = 0
X = [prev_x]
Y = [prev_y]
for n in range(100000):
   r = random.uniform(0, 100)
   if r < 1.0:
       x = 0
       y = 0.16*prev_y
   elif r < 86.0:
       x = 0.85*prev_x + 0.04*prev_y
       y = -0.04*prev_x + 0.85*prev_y+1.6
   elif r < 93.0:
       x = 0.2*prev_x - 0.26*prev_y
       y = 0.23*prev_x + 0.22*prev_y + 1.6
   else:
       x = -0.15*prev_x + 0.28*prev_y
       y = 0.26*prev_x + 0.24*prev_y + 0.44
   X.append(x)
   Y.append(y)
   prev_x, prev_y = x, y

'''Make a plot'''
plt.figure(figsize = [15,15])
plt.scatter(X,Y,color = 'g',marker = '.')
plt.show()
"""
'''
def dragonsin(lato, livello):
    if livello == 0:
        tina.forward(lato)
        return
    dragonsin(lato, livello-1)
    tina.left(90)
    dragondes(lato, livello-1)

def dragondes(lato, livello):
    if livello == 0:
        tina.forward(lato)
        return
    dragonsin(lato, livello-1)
    tina.right(90)
    dragondes(lato, livello-1)

def dragonsinb(lato, livello):
    if livello == 0:
        tina.forward(lato)
        return
    dragonsinb(lato, livello-1)
    tina.left(45); tina.forward(lato/5); tina.left(45)
    dragondesb(lato, livello-1)

def dragondesb(lato, livello):
    if livello == 0:
        tina.forward(lato)
        return
    dragonsinb(lato, livello-1)
    tina.right(45); tina.forward(lato/5); tina.right(45)
    dragondesb(lato, livello-1)

for liv in range(7):
    piano = tg.TurtlePlane(f"livello={liv}")
    tina = tg.Turtle(color='brown', y=100)
    dragonsinb(25, liv)

piano = tg.TurtlePlane(14, h=600)
tina = tg.Turtle(color='brown', y=100)
tina.hide()
dragonsin(2, 7)

piano.mainloop()
'''
