#!/usr/bin/env python
#-------------------------------python-----------------------luoghi.py--#
#                                                                       #
#                    Luoghi geometrici: coniche                         #
#                                                                       #
#--Bruno Stecca - Daniele Zambelli--------------------------------2003--#

import pygraph.pyturtle as tg
import math


class Geometra(tg.Turtle):

    def conica1(self, raggio, p):
        numpassi = 72
        lato = raggio * 2 * math.pi / numpassi
        ang = 180. / numpassi
        self.up()
        self.forward(raggio)
        self.left(90)
        self.down()
        for i in range(numpassi):
            self.asse(p)
            self.left(ang)
            self.forward(lato)
            self.left(ang)

    def conica2(self, p):
        numpassi = 80
        lato = 5
        self.up()
        self.back(200)
        self.down()
        for i in range(numpassi):
            self.asse(p)
            self.forward(lato)

    def asse(self, punto):
        direction = self.direction
        position = self.position
        self.up()
        self.lookat(punto)
        self.forward(self.distance(punto) / 2.)
        self.left(90)
        self.back(1000)
        self.down()
        self.forward(2000)
        self.up()
        self.position = position
        self.direction = direction
        self.down()


piano = tg.TurtlePlane('Ellisse')
gina = Geometra(color='red')
gina.conica1(200, (150, 0))
piano = tg.TurtlePlane('Parabola')
gina = Geometra(color='blue')
gina.conica2((0, 50))
piano = tg.TurtlePlane('Iperbole')
gina = Geometra(color='green')
gina.conica1(100, (150, 0))
piano.mainloop()
