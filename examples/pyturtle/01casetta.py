#!/usr/bin/env python
#--------------------------python-pyturtle----------------01casetta.py--#
#                                                                       #
#                              Casetta                                  #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2005--#

import pygraph.pyturtle as tg

piano = tg.TurtlePlane()
tina = tg.Turtle()

tina.forward(100)
tina.left(90)
tina.forward(100)
tina.left(45)
tina.forward(71)
tina.left(90)
tina.forward(71)
tina.left(135)
tina.forward(100)
tina.right(135)
tina.forward(141)
tina.right(135)
tina.forward(100)
tina.right(135)
tina.forward(141)

piano.mainloop()
