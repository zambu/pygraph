#!/usr/bin/env python
#-------------------------------python----------------------trirett.py--#
#                                                                       #
#                              Trirett                                  #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2002--#

import pygraph.pyturtle as tg


def trirett(c1, c2):
    tina.forward(c1)
    a = tina.position
    tina.back(c1)
    tina.left(90)
    tina.forward(c2)
    tina.position = a
    tina.right(90)
    tina.back(c1)


def inviluppo(l1, l2, inc=10):
    if l1 < 0:
        return
    trirett(l1, l2)
    inviluppo(l1 - inc, l2 + inc)


def quasar(l):
    for i in range(4):
        inviluppo(l, 0)
        tina.left(90)


def occhio(l):
    for i in range(2):
        inviluppo(l, 0)
        tina.forward(l)
        tina.left(90)
        tina.forward(l)
        tina.left(90)


def quattrocchi(l):
    for i in range(4):
        occhio(l)
        tina.left(90)


piano = tg.TurtlePlane('Inviluppi di triangoli rettangoli', 600, 600)
tina = tg.Turtle()
quattrocchi(290)
piano.mainloop()
