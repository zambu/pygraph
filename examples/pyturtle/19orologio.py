#!/usr/bin/env python
#-------------------------------python---------------------orologio.py--#
#                                                                       #
#                              Orologio                                 #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2003--#

import pygraph.pyturtle as tg
from time import time, localtime


class Orologio:

    def __init__(self, d=100):
        self.d = d * 0.9
        self.luore = self.d * 0.6
        self.laore = self.d * 0.1
        self.aore = 0
        self.lo = None
        self.luminuti = self.d * 0.8
        self.laminuti = self.d * 0.05
        self.aminuti = 0
        self.lm = None
        self.lusecondi = self.d * 0.9
        self.lasecondi = self.d * 0.02
        self.asecondi = 0
        self.ls = None
        self.p = tg.TurtlePlane("PyOrologio", d * 2, d * 2)
        self.tina = tg.Turtle()
        self.cassa()
        try:
            while 1:
                self.p.after(1000, self.lancette(localtime(time())[3:6]))
        except:
            pass

    def cassa(self):
        self.tina.up()
        self.tina.forward(self.d)
        self.tina.down()
        self.tina.left(90)
        self.tina.circle(self.d)
        self.tina.right(90)
        self.tina.up()
        self.tina.back(self.d)
        self.tina.down()
        d1 = self.d * 0.9
        d2 = self.d - d1
        for i in range(12):
            self.tina.up()
            self.tina.forward(d1)
            self.tina.down()
            self.tina.forward(d2)
            self.tina.up()
            self.tina.back(self.d)
            self.tina.down()
            self.tina.left(30)

    def lancette(self, tina):
        ore, minuti, secondi = tina
        aore = round(90 - ore * 30 - minuti * 0.5)
        aminuti = round(90 - minuti * 6 - secondi * 0.1)
        asecondi = round(90 - secondi * 6)
        if self.aore != aore:
            if self.lo:
                self.p.delete(self.lo)
            self.tina.direction = aore
            self.tina.width = self.laore
            self.lo = self.tina.forward(self.luore)
            self.tina.up()
            self.tina.back(self.luore)
            self.tina.down()
            self.aore = aore
        if self.aminuti != aminuti:
            if self.lm:
                self.p.delete(self.lm)
            self.tina.direction = aminuti
            self.tina.width = self.laminuti
            self.lm = self.tina.forward(self.luminuti)
            self.tina.up()
            self.tina.back(self.luminuti)
            self.tina.down()
            self.aminuti = aminuti
        if self.asecondi != asecondi:
            if self.ls:
                self.p.delete(self.ls)
            self.tina.direction = asecondi
            self.tina.width = self.lasecondi
            self.ls = self.tina.forward(self.lusecondi)
            self.tina.up()
            self.tina.back(self.lusecondi)
            self.tina.down()
            self.asecondi = asecondi


o = Orologio(150)
