#!/usr/bin/env python
#--------------------------python-pyturtle-------------------crollo.py--#
#                                                                       #
#                           Crollo colore                               #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2002--#

import random
import pygraph.pyturtle as tg


class MiaTurtle(tg.Turtle):

    def sposta(self, avanti, sinistra):
        """ Effettua uno spostamento orizzontale e verticale di Tartaruga
        senza disegnare la traccia """
        self.up()
        self.forward(avanti)
        self.left(90)
        self.forward(sinistra)
        self.right(90)
        self.down()

    def quadrato(self, lato, fill):
        """ Disegna un quadrato di dato lato, vuoto o pieno """
        self.fill(fill)
        for i in range(4):
            self.forward(lato)
            self.left(90)
        self.fill(0)

    def mattone(self, lato, spostamento, angolo, fill):
        """ Disegna un mattone spostato rispetto alla posizione attuale
        di Tartaruga """
        self.up()
        self.right(angolo)
        self.forward(spostamento)
        self.down()
        self.quadrato(lato, fill)
        self.up()
        self.back(spostamento)
        self.left(angolo)
        self.down()

    def muro(self, lato, righe, colonne, caos=0,
             spazio_righe=5, spazio_colonne=5):
        """ Disegna un muro piu' o meno stabile
        di mattoni con colore cangiante"""
        for i in range(righe):
            for j in range(colonne):
                self.color = ((float(colonne) + righe - i - j) / (colonne + righe),
                              float(i) / righe, float(j) / colonne)
                self.mattone(lato, random.randrange(i + j + 1) * caos,
                             random.randrange(i + j + 1) * caos, 1)
                self.sposta(lato + spazio_colonne, 0)
            self.sposta(-colonne * (lato + spazio_colonne),
                        lato + spazio_righe)
        self.sposta(0, -righe * (lato + spazio_righe))


piano = tg.TurtlePlane()
gina = MiaTurtle(x=-250, y=-190)
# gina.tracer(1)
gina.muro(20, 15, 20, 1)
piano.mainloop()
