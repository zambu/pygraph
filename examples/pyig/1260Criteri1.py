#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-----------------1260Criteri1.py--#
#                                                                       #
#             Primo criterio di congruenza di triangoli                 #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Applicare il primo criterio di congruenza di triangoli.

Problema
- Disegna un triangolo.
- Disegna una semiretta.
- Disegna il triangolo con due lati e l'angolo compreso congruenti al primo.

Soluzione
- da inventare

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Primo triangolo
###
a0 = ig.Point(-7, 2, color="#00f050", width=6, name="A")
b0 = ig.Point(-4, -2, color="#00f050", width=6, name="B")
c0 = ig.Point(-5, 6, color="#00f050", width=6, name="C")
t0 = ig.Polygon((a0, b0, c0), color="#a87234")

# Elementi congruenti
ab0 = ig.Segment(a0, b0)
ac0 = ig.Segment(a0, c0)
bac0 = ig.Angle(b0, a0, c0)

# Semiretta
a1 = ig.Point(2, 2, color="#00f050", width=6, name="A'")
d = ig.Point(8, -6, color="#00f050", width=6, name="D")
r1 = ig.Ray(a1, d, width=1)

# Triangolo congruente
circ1 = ig.Circle(a1, ab0, width=1)
b1 = ig.Intersection(r1, circ1, 1, name="B'")
bac1 = ig.Angle(b1, a1, bac0)
ac1 = bac1.side1(width=1)
circ2 = ig.Circle(a1, ac0, width=1)
c1 = ig.Intersection(ac1, circ2, 1, name="C'")
t1 = ig.Polygon((a1, b1, c1), color="#72a834")

ig.Text(piano, -5, -6, """Primo criterio di congruenza di triangoli.""")

piano.mainloop()
