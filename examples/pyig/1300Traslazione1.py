#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-------------1300Traslazione1.py--#
#                                                                       #
#                             Traslazione                               #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Traslazioni

Problema
- Disegna un segmento AB
- Disegna un poligono.
- Disegna il poligono traslato di AB.

Soluzione
- Disegnare il fascio di rette parallele a AB, passanti
  per i vertici del poligono.
- Riporta su queste rette il segmento AB.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Segmento
###
aa = ig.Point(-7, 8, color="#00f050", width=6, name="A")
bb = ig.Point(-5, 9, color="#00f050", width=6, name="B")
traslazione = ig.Segment(aa, bb)

###
# Poligono0
###
a0 = ig.Point(-7, 5, color="#00f050", width=6, name="A")
b0 = ig.Point(-2, -2, color="#00f050", width=6, name="B")
c0 = ig.Point(-3, 6, color="#00f050", width=6, name="C")
d0 = ig.Point(-4, 2, color="#00f050", width=6, name="C")
ig.Polygon((a0, b0, c0, d0), color="#a87234")

# Rette parallele a r0
ra = ig.Parallel(traslazione, a0, width=1)
rb = ig.Parallel(traslazione, b0, width=1)
rc = ig.Parallel(traslazione, c0, width=1)
rd = ig.Parallel(traslazione, d0, width=1)

# Vertici del poligono traslato
ca = ig.Circle(a0, traslazione, width=1)
a1 = ig.Intersection(ra, ca, 1, name="A'")
cb = ig.Circle(b0, traslazione, width=1)
b1 = ig.Intersection(rb, cb, 1, name="B'")
cc = ig.Circle(c0, traslazione, width=1)
c1 = ig.Intersection(rc, cc, 1, name="C'")
cd = ig.Circle(d0, traslazione, width=1)
d1 = ig.Intersection(rd, cd, 1, name="D'")
##c1=ConstrainedPoint(rc, 1, name="C'")
##d1=ConstrainedPoint(rd, 1, name="D'")

# Poligono traslato
ig.Polygon((a1, b1, c1, d1), color="#72a834")

ig.Text(-5, -6, """Traslazione di un poligono.""")

piano.mainloop()
