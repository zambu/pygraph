#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1090Puntimedi.py--#
#                                                                       #
#                       Punti medi di segmenti                          #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Gli oggetti pyig hanno delle propriet�. Le propriet� sono oggetti pyig che
possono essere creati attraverso dei metodi.
Ad esempio gli oggetti di tipo Segmento hanno un punto medio. Se esse �
un segmento, il suo punto medio viene creato chiamando il metodo:

esse.midpoint()

Problema
Disegnare un triangolo per tre punti e il triangolo che ha per vertici i
punti medi dei suoi lati.

Soluzione
Posizionare tre punti nel piano e un segmento per ogni coppia di punti,
trovare i punti medi dei tre lati e disegnare il nuovo triangolo.
"""

###
# Chiamata della libreria pyig
# Creazione di una finestra interattiva
###

import pygraph.pyig as ig

piano = ig.InteractivePlane()


def triangolo(v1, v2, v3, spessore, colore):
    """Disegna un triangolo, dati i vertici, e restituisce i suoi lati."""
    l1 = ig.Segment(v1, v2, width=spessore, color=colore)
    l2 = ig.Segment(v2, v3, width=spessore, color=colore)
    l3 = ig.Segment(v3, v1, width=spessore, color=colore)
    return l1, l2, l3


a = ig.Point(-4, 0)
b = ig.Point(4, 0)
c = ig.Point(0, 4 * 1.732050808)

l1, l2, l3 = triangolo(a, b, c, 4, "#aa0000")

pm1 = l1.midpoint(width=6)
pm2 = l2.midpoint(width=6)
pm3 = l3.midpoint(width=6)

triangolo(pm1, pm2, pm3, 2, "#aaaa00")

###
# Messaggio finale
###

ig.Text(-7, -5, """Triangolo annidato in un altro triangolo 
Muovi i vertici o i lati.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
