#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------- ---1280Criteri3.py--#
#                                                                       #
#             Terzo criterio di congruenza di triangoli                 #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Applicare il terzo criterio di congruenza di triangoli.

Problema
- Disegna un triangolo.
- Disegna una semiretta.
- Disegna il triangolo con tre lati congruienti al primo.

Soluzione
- Si possono usare le circonferenze per riportare segmenti congruenti.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Primo triangolo
###
a0 = ig.Point(-7, 2, color="#00f050", width=6, name="A")
b0 = ig.Point(-4, -2, color="#00f050", width=6, name="B")
c0 = ig.Point(-5, 6, color="#00f050", width=6, name="C")
# t0 = Polygon((a0, b0, c0), color="#a87234")

# Elementi congruenti
ab0 = ig.Segment(a0, b0)
bc0 = ig.Segment(b0, c0)
ca0 = ig.Segment(c0, a0)

# Semiretta
a1 = ig.Point(2, 2, color="#00f050", width=6, name="A'")
d = ig.Point(8, -6, color="#00f050", width=6, name="D")
r1 = ig.Ray(a1, d, width=1)

# Triangolo congruente
circ1 = ig.Circle(a1, ab0, width=1)
b1 = ig.Intersection(r1, circ1, 1, name="B'")
circ2 = ig.Circle(a1, ca0, width=1)
circ3 = ig.Circle(b1, bc0, width=1)
c1 = ig.Intersection(circ2, circ3, 1, name="C'")
t1 = ig.Polygon((a1, b1, c1), color="#72a834")

ig.Text(-5, -6, """Terzo criterio di congruenza di triangoli.""")

piano.mainloop()
