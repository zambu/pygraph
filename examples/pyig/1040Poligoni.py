#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pkig-----------------1040Poligoni.py--#
#                                                                       #
#                               Poligoni                                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Nella geometria si studiano spesso dei poligoni, il pi� semplice � il 
triangolo.
Disegare un poigono con Pyig � semplice si pu� colorare la sua superficie
e definire colore e spessore del suo controno

Problema
Disegnare:
- un triangolo per tre punti vuoto,
- un quadrilatero dati i vertici vuoto,
- un triangolo colorato,
- un quadrilatero colorato,

Soluzione
Disegnare i vertici e poi utilizzare l'oggetto Polygon().
"""

###
# Chiamata della libreria pyig
# Creazione di una finestra interattiva
###

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Triangolo
###

a = ig.Point(-6, -1)
b = ig.Point(-4, -1)
c = ig.Point(-5, 5)
ig.Polygon((a, b, c))

###
# Poligono
###

# In questo caso creo i vertici al volo nella chiammata di Polygon()

ig.Polygon((ig.Point(-4, 5), ig.Point(-2, -1),
            ig.Point(0, 5), ig.Point(-2, 3)))

###
# Triangolo colorato
###

a = ig.Point(6, -1)
b = ig.Point(4, -1)
c = ig.Point(5, 5)
ig.Polygon((a, b, c), intcolor="#f22ff2")

###
# Quadrilatero colorato
###

ig.Polygon((ig.Point(4, 5), ig.Point(2, -1), ig.Point(0, 5), ig.Point(2, 3)),
           intcolor="#2ff2f2", color="#f2f22f")

###
# Messaggio finale
###

ig.Text(-7, -5, """Poligoni dati i vertici.
Solo i vertici, che sono punti liberi,
si possono muovere con il mouse.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
