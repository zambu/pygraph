#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1360Nefroide0.py--#
#                                                                       #
#                               Nefroide                                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Nefroide

Problema
- Disegna una Nefroide come inviluppo di circonferenze

Soluzione
- Traccia la circonferenza base.
- traccia un suo diametro.
- Usa i punti vincolati e le rette perpendicolari per tracciare
  l'inviluppo di circonferenze.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

# Circonferenza base
centro = ig.Point(-1, 2, color="#00f050", width=7, name="C")
punto = ig.Point(-2, -2, color="#00f050", width=7, name="P")
c = ig.Circle(centro, punto, color="#a87234")

# Diametro
d = ig.Line(centro, punto, color="#a87234")

# Funzione che restituisce una Circonferenza dell'inviluppo


def circ(c, d, costante):
    centro = ig.ConstrainedPoint(c, costante, width=7)
    n = ig.Orthogonal(d, centro, width=1)
    punto = ig.Intersection(n, d, width=1)
    return ig.Circle(centro, punto, color="#72a834")


circ(c, d, 1)

ig.Text(-5, -6, """Nefroide.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
