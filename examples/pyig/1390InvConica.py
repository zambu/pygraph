#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1390InvConica.py--#
#                                                                       #
#                    Conica come inviluppo di rette                     #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Ellisse e iperbole come inviluppo di rette

Problema
- Disegna una conica come inviluppo di rette
  dati una circonferenza e un fuoco

Soluzione
- Traccia la circonferenza.
- traccia il fuoco.
- traccia gli assi dei segmenti che hanno per estremi
  un punto della circonferenza e il fuoco.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

# Numero di rette dell'inviluppo,
N = 20

# Direttrice e Fuoco
centro = ig.Point(-2, 2, color="#00f050", width=10)
p = ig.Point(-1, -4, color="#00f050", width=10)
circonf = ig.Circle(centro, p, color="#a87234", name="C")
fuoco = ig.Point(2, 2, color="#00f050", width=5, name="F")

# Funzione che restituisce l'asse dati i due stremi di un segmento


def asse(p0, p1):
    l = ig.Segment(p0, p1, width=1)
    m = ig.MidPoints(p0, p1)
    return ig.Orthogonal(l, m, color="#72a834", width=1)


for i in range(N * 2):
    asse(fuoco, ig.PointOn(circonf, float(i) / N))

aa = asse(fuoco, ig.ConstrainedPoint(circonf, 0.5, width=7))
aa.color = "red"

ig.Text(-5, -6, """Conica come inviluppo.""")

piano.mainloop()
