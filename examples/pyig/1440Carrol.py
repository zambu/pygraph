# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-------------------1440Carrol.py--#
#                                                                       #
#                   Un falso teorema di Lewis Carrol                    #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2010---#

"""
Teorema: Qualche volta un angolo ottuso pu� essere uguale a un angolo retto.
Sia dato un quadrato ABCD.
Sia m l'asse del lato AB.
Si prenda un punto G per cui CG congruente CB.
Sia n l'asse del segmento AG.
Sia K l'intersezione di m e n.
Il triangolo KAD � congruente al triangolo KGC infatti:
- AD congr CB congr CG
- KD congr KC perch� m � anche asse di CD
- KA congr KG perch� K appartiene all'asse di AG.
In particolare angolo ADK congr angolo GCK.
E angolo ADC congr angolo GCD perch� differenza di angoli uguali.
(vedi file 1440Carrol.png)

"""

import pygraph.pyig as ig


def asse(p0, p1, **args):
    """Restituisce l'asse del segmento di estremi p0 e p1."""
    return ig.Orthogonal(ig.Line(p0, p1, visible=False),
                         ig.MidPoints(p0, p1, visible=False), **args)


def quadrato(p0, p1, name2, name3, **args):
    """Restituisce un quadrato dati due vertici consecutivi."""
    p3 = ig.Intersection(ig.Orthogonal(ig.Line(p0, p1, visible=False), p0,
                         visible=False),
                         ig.Circle(p0, p1, visible=False), 1, name=name3)
    p2 = ig.Intersection(ig.Circle(p3, p0, visible=False),
                         ig.Circle(p1, p0, visible=False), 1, name=name2)
    return ig.Polygon((p0, p1, p2, p3), **args)


piano = ig.InteractivePlane()
a = ig.Point(2, -2, width=5, name='A')
b = ig.Point(7, -1, width=5, name='B')
#asse(p0, p1)
q = quadrato(a, b, 'C', 'D')
piano.defwidth = 1
c, d = q.vertices[-2:]
n = asse(a, b)
circ = ig.Circle(c, b)
g = ig.ConstrainedPoint(circ, 0.2, width=5, name='G')
s = ig.Segment(a, g)
m = asse(a, g)
k = ig.Intersection(n, m, name='K')
piano.defwidth = 2
piano.defcolor = 'brown'
t0 = ig.Polygon((k, a, d))
t1 = ig.Polygon((k, g, c))
piano.mainloop()
