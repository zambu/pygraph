#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------------1420AngCirc.py--#
#                                                                       #
#                 Angoli al centro e alla circonferenza                 #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2007---#

"""
Angoli al centro e alla circonferenza

Problema
- Disegna una circonferenza non modificabile.
- Disegna un angolo al centro.
- Disegna il corrispondente angolo alla circonferenza.
- Disegna la retta che congiunge i due vertici.

Soluzione
- I punti base della circonferenza non devono essere visibili
- Per i punti sulla circonferenza usa i punti vincolati.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Circonferenza
###
o = ig.Point(0, 0, False, name="O")
c0 = ig.Circle(o, ig.Point(7, 0, False))

###
# Estremi dell'arco
###
a = ig.ConstrainedPoint(c0, -0.2, width=6, name="A")
b = ig.ConstrainedPoint(c0, 0.2, width=6, name="B")

###
# Vertice sulla circonferenza
###
v = ig.ConstrainedPoint(c0, 0.5, width=5, name="V")

###
# Angoli
###
r0 = ig.Ray(v, a)
r1 = ig.Ray(v, b)
a1 = ig.Angle(a, v, b)
r2 = ig.Ray(o, a)
r3 = ig.Ray(o, b)
a0 = ig.Angle(a, o, b)

r4 = ig.Ray(v, o, width=1, color="orange")

ig.Text(-5, -9, """Angoli al centro e alla circonferenza.""")

ig.VarText(-5, -10, """Angolo al centro: {0}.""", a0.extent())
ig.VarText(-5, -11, """Angolo alla circonferenza: {0}.""", a1.extent())

piano.mainloop()
