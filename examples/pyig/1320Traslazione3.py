#!/usr/bin/env python
# -*- coding: utf-8 -*-
#---------------------------------pyig-------------1320Traslazione3.py--#
#                                                                       #
#                             Traslazione                               #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Traslazioni

Problema
- Disegna un segmento AB
- Disegna un poligono.
- Disegna il poligono traslato di AB.

Soluzione
- Disegnare il fascio di rette parallele a AB, passanti
  per i vertici del poligono.
- Riporta su queste rette il segmento AB.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

###
# Trasla
###
# def trasla(punto, traslazione):
##  par=Parallel(traslazione, punto, width=1)
##  circ=Circle(punto, traslazione, width=1)
# return Intersection(par, circ, 1, name=str(punto.name())+"'")
##


def trasla(punto, traslazione):
    par = ig.Parallel(traslazione, punto, width=1)
    return ig.PointOn(par, 1, name=str(punto.name) + "'")


###
# Segmento
###
p = ig.Point(-7, 8, color="#00f050", width=6, name="P")
q = ig.Point(-1, 6, color="#00f050", width=6, name="Q")
vettore = ig.Segment(p, q)

###
# Poligono0
###
pol = (ig.Point(-7, 5, color="#00f050", width=6, name="A"),
       ig.Point(-2, -2, color="#00f050", width=6, name="B"),
       ig.Point(-3, 6, color="#00f050", width=6, name="C"),
       ig.Point(-4, 2, color="#00f050", width=6, name="C"))
ig.Polygon(pol, color="#a87234", width=4)

poltras = [trasla(vertice, vettore) for vertice in pol]
ig.Polygon(poltras, color="#7934b3", width=4)

ig.Text(-5, -6, """Traslazione di un poligono.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
