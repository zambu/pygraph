#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------1190Circocentro2.kpy--#
#                                                                       #
#      Circocentro di un triangolo e circonferenza circoscritta         #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Riprendiamo il problema del circocentro.
Per risolverlo bisognava tracciare gli assi di alcuni segmenti: i lati.
Estendiamo la classe Segment in modo da aggiungere, ad un segmento,
la capacit� di tracciare il suo asse.
La Programmazione Orientata agli Oggetti (OOP) fornisce dei meccanismi
semplici per fare questo.

Problema
- amplia la classe "Segment" aggiungendo il meto "asse"
- disegna la circoferenza circoscritta a un triangolo

Soluzione
- utilizza la classe "Segmento", derivata dalla classe "Segment"
  presente nel modulo _1170Asse.py
- disegnare la circonferenza circoscritta al triangolo

"""

import pygraph.pyig as ig
import _1190Asse

piano = ig.InteractivePlane()

# Dal modulo "_1170Asse" viene caricata la definizione della
# classe "Segmento".

# tre punti
a = ig.Point(-2, 4, width=6)
b = ig.Point(-3, -3, width=6)
c = ig.Point(5, 2, width=6)

# I seguenti 3 lati non sono oggetti della classe "Segment" ma della
# nuova classe "Segmento", che *aggiunge* ai metodi della classe "Segment"
# anche il metodo "asse".
l1 = _1190Asse.Segmento(a, b)
l2 = _1190Asse.Segmento(b, c)
l3 = _1190Asse.Segmento(c, a)

# assi
a1 = l1.asse()
a2 = l2.asse()
a3 = l3.asse()

# corcocentro
circocentro = ig.Intersection(a1, a2, width=10, color="#ff66aa")

# circonferenza circoscritta:
ig.Circle(circocentro, a)

###
# Messaggio finale
###

ig.Text(-2, -3.5, """Triangolo,
circocentro
e circonferenza circoscritta.
Modifica colori e spessori del circocentro e della circonferenza circoscritta.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
