#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-----------------1200Triequi.kpy--#
#                                                                       #
#                        Triangolo equilatero                           #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Disegnare un triangolo equilatero.

Problema
- Disegna due punti a, b.
- Disegna il triangolo equilatero che ha per vertici a e b.

Soluzione
- crea la funzione triequi(a, b) che realizza il triangolo.
- crea due punti
- chiama la funzione passandole i due punti come vertici.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()


def triequi(a, b):
    """Funzione che crea e restituisce un triangolo equilatero."""
    c1 = ig.Circle(a, b, width=1)
    c2 = ig.Circle(b, a, width=1)
    c = ig.Intersection(c1, c2, 1)
    return ig.Polygon((a, b, c))


p1 = ig.Point(-3, 2, color="#00aa00", width=6)
p2 = ig.Point(2, -1, color="#00aa00", width=6)
t = triequi(p1, p2)
t.color = "#2390a8"
t.width = 4
lato = ig.Segment(p1, p2).length()
perimetro = t.perimeter()
area = t.surface()

ig.VarText(-5, -6, """Triangolo equilatero,
lato = {0}
Perimetro = {1}
Area = {2}

Deforma il triangolo.""",
        (lato, perimetro, area))

###
# Attivazione della finestra interattiva
###

piano.mainloop()
