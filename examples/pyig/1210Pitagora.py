#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-----------------1210Pitagora.py--#
#                                                                       #
#                Estensione del teorema di Pitagora                     #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Estensione del teorema di Pitagora
in ogni triangolo la somma dei cerchi costruiti su due lati �:
1. se l'angolo tra i due lati � acuto: maggiore del cerchio costruito
   sul terzo laro.
2. se l'angolo tra i due lati � ottuso: minore del cerchio costruito
   sul terzo laro.
3. se l'angolo tra i due lati � retto: equivalente del cerchio costruito
   sul terzo laro.

"""

import pygraph.pyig as ig

piano = ig.InteractivePlane()

# Disegno un triangolo rettangolo
a = ig.Point(-3, -2, width=6, name="A")
b = ig.Point(5, -2, width=6, name="B")
c = ig.Point(-3, 7, width=6, name="C")

t = ig.Polygon((a, b, c))
t.intcolor = "#56f6e6"
t.color = "#e656f6"

# Angolo A
bac = ig.Angle(b, a, c)

# Punti medi dei lati, non visibili!
l = ig.MidPoints(a, b, visible=False)
m = ig.MidPoints(b, c, visible=False)
n = ig.MidPoints(c, a, visible=False)

# Cerchi con i lati per diametro
c_ab = ig.Circle(l, a)
c_bc = ig.Circle(m, b, color="#0f0f0f")
c_ca = ig.Circle(n, c)

area_ab = c_ab.surface()
area_bc = c_bc.surface()
area_ca = c_ca.surface()

somma_aree_ab_ca = ig.Calc(lambda x, y: x + y, (area_ab, area_ca))

ig.VarText(-4, -6, "Angolo A: {0}", (bac.extent(),))
ig.VarText(-4, -7, "Area cerchio su bc: {0}", (area_bc,))
ig.VarText(-4, -8, "Somma cerchi ab e ca: {0}", (somma_aree_ab_ca,))

piano.mainloop()
