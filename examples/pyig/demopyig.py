#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
#-------------------------------python---------------------demopyig.py--#
#                                                                       #
#                     Python Interactive Geometry                       #
#                                                                       #
#--Daniele Zambelli-----------Licence-GPL------------------------2005---#

import math
import pygraph.pyig as ig

piano = ig.InteractivePlane()


def prog():
    """Test for objects and methords."""
    base = -1
    inter = 0.75
    piano.defwidth = 8
    piano.defcolor = "#80d040"
    punto1 = ig.Point(-2, 0, name="A")
    punto2 = ig.Point(2, 0, name="B")
    punto3 = ig.Point(0, 2 * math.sqrt(3), name="C")
#  punto3 = ig.Point(-8, 4)
    piano.defwidth = 3
    lato1 = ig.Line(punto1, punto2)
    lato2 = ig.Segment(punto2, punto3, color="#10ff10")
    lato3 = ig.Segment(punto3, punto1, width=10)
    circ1 = ig.Circle(punto1, punto2, width=1)
    punto4 = ig.Point(0, 5)
    punto5 = ig.Point(-2, 3)
    retta1 = ig.Line(punto4, punto5)
    intersezione1 = ig.Intersection(lato1, retta1)
    intersezione1.width = 10
    intersezione1.color = "#40f040"
    punto6 = ig.Point(-2, 6)
    punto7 = ig.Point(-2, 8)
    retta2 = ig.Line(punto6, punto7)
    intersezione2 = ig.Intersection(lato1, retta2)
    intersezione3 = ig.Intersection(retta1, circ1, 1)
    circ2 = ig.Circle(punto6, punto7, intcolor="#f0f800", width=1)
    intersezione4 = ig.Intersection(circ1, circ2, 1)
    t1 = ig.Text(-6, 9, "Test delle funzionalitą della libreria Pyig")
    t2 = ig.Label(intersezione1, 0, -20, "Punto P")
    ci1 = intersezione1.coords()
    t3 = ig.VarText(2, base - inter * 0, "Coordinate del Punto P: {}", (ci1,))
    ci4 = intersezione4.coords()
    t4 = ig.VarLabel(intersezione4, 0, 20, "{}", (ci4,))
    l1 = lato2.length()
    t5 = ig.VarLabel(lato2, 0, 20, "{}", (l1,))
    poli1 = ig.Polygon((intersezione2, intersezione4, punto3), "#f2f4f6")
    peri1 = poli1.perimeter()
    area1 = poli1.surface()
    t6 = ig.VarText(2, base - inter * 1,
                 "perimetro del poligono: {}; area: {}", (peri1, area1))
    peri2 = circ2.perimeter()
    area2 = circ2.surface()
    t6 = ig.VarText(2, base - inter * 2,
                 "circonf. cerchio giallo: {}; area: {}", (peri2, area2))
    c1 = ig.Calc(lambda x, y: x + y, (area1, area2))
    t7 = ig.VarText(2, base - inter * 3,
                 "somma delle aree del poligono e del cerchio: {}", (c1,))
    r1 = ig.Ray(intersezione4, punto3, color="#a0a0a0")
    s3 = ig.Segment(Point(2, 7), ig.Point(5, 7))
    circ3 = ig.Circle(intersezione4, s3)
    t8 = ig.VarText(2, base - inter * 4.5,
                 "In questo testo ci sono oggetti dei seguenti tipi:\n"
                 "{}, {}, {}, {}, ...", (punto1.type(),
                                         lato1.type(),
                                         lato2.type(),
                                         circ1.type()))
    punto8 = ig.MidPoints(intersezione4, 
                          ig.Point(7, 7), width=7, color="#808080")
    lato4 = ig.Segment(intersezione4, ig.Point(-7, 7))
    punto9 = MidPoint(lato4, width=7, color="#808080")
    t9 = ig.VarText(2, base - inter * 6.5, "s3: lunghezza={}; equazione: {},\
  pendenza: {}",
                 (s3.length(), s3.equation(), s3.slope()))
    s3.point0().color = "#faaf12"
    s3.point1().width = 15
    ortogonale = ig.Orthogonal(lato1, intersezione4, color="#56a7f0", width=5)
    parallela = ig.Parallel(lato1, intersezione4, color="#56a7f0", width=5)
    a1 = ig.Angle(punto1, intersezione4, punto3, color="#fa0a00")
    ampiezza = a1.extent()
    t9 = ig.VarText(2, base - inter * 7.5, "a1: ampiezza={}", (ampiezza,))
    r2 = ig.Bisector(a1, color="#0aa000")
    cp0_1_3 = ig.ConstrainedPoint(lato1, 1. / 3, width=4, color="#fa1010")
    cp0_2_3 = ig.ConstrainedPoint(lato1, 2. / 3, width=4, color="#fa1010")
    cp0_2_1 = ig.ConstrainedPoint(lato1, 2, width=4, color="#fa1010")
    cp0_3_1 = ig.ConstrainedPoint(lato1, 3, width=4, color="#fa1010")
    cp1_1_3 = ig.ConstrainedPoint(ortogonale, 1. / 3, width=4, color="#fa1010")
    cp1_2_3 = ig.ConstrainedPoint(ortogonale, 2. / 3, width=4, color="#fa1010")
    cp1_2_1 = ig.ConstrainedPoint(ortogonale, 2, width=4, color="#fa1010")
    cp1_3_1 = ig.ConstrainedPoint(ortogonale, 3, width=4, color="#fa1010")
    cp2_1_3 = ig.ConstrainedPoint(parallela, 1. / 3, width=4, color="#fa1010")
    cp2_2_3 = ig.ConstrainedPoint(parallela, 2. / 3, width=4, color="#fa1010")
    cp2_2_1 = ig.ConstrainedPoint(parallela, 2, width=4, color="#fa1010")
    cp2_3_1 = ig.ConstrainedPoint(parallela, 3, width=4, color="#fa1010")
    cp3_1_3 = ig.ConstrainedPoint(r2, 1. / 3, width=4, color="#fa1010")
    cp3_2_3 = ig.ConstrainedPoint(r2, 2. / 3, width=4, color="#fa1010")
    cp3_2_1 = ig.ConstrainedPoint(r2, 2, width=4, color="#fa1010")
    cp3_3_1 = ig.ConstrainedPoint(r2, 3, width=4, color="#fa1010")
    cp4_3_1 = ig.ConstrainedPoint(circ3, 0, width=4, color="#fa8010")
    cp4_3_1 = ig.ConstrainedPoint(circ3, 0.5, width=4, color="#fa8010")
    cp4_3_1 = ig.ConstrainedPoint(circ3, 1, width=4, color="#fa8010")
    cp4_3_1 = ig.ConstrainedPoint(circ3, -0.5, width=4, color="#fa8010")
    r3 = ig.Ray(intersezione4, ig.Point(-8, 1, width=6, color="#80d040"))
    a2 = ig.Angle(intersezione4, ig.Point(-8, 1, width=6, color="#80d040"), a1,
               color="#50ff00")
    vertici = (ig.Point(-8, 6), intersezione4,
               ig.Point(-8, 3), ig.Point(-6, -1))
    ig.Polygonal(vertici, color="#ffaa00")
    ig.CurviLine(vertici, color="#00ffaa")


prog()
piano.mainloop()
