#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------------1470RettePC.py--#
#                                                                       #
#                      Retta nel piano cartesiano                       #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2013---#

"""
Un semplice gioco:
Posiziona una retta in modo che abbia 
intercetta e coefficiente angolare dati.

Problema
Disegnare:
- Visualizza un numero a caso compreso tra -10 e 10;
- visuualizza il coeff.ang. a caso compreso tra -5 e +5;
- disegna una retta a caso;
- visualizza l'equazione della retta.

Soluzione
Per calcolare i numeri a caso utilizza la libreria random.
Particolare attenzione per il coefficiente angolare 
calcolato come rapporto tra due interi compresi tra -10 e +10
"""

from __future__ import division, print_function
import pygraph.pyig as ig
import random

piano = ig.InteractivePlane()

# Numeri casuali

x0, y0 = random.randint(-10, +10), random.randint(-10, +10)
x1, y1 = random.randint(-10, +10), random.randint(-10, +10)
mn, md = random.randint(-10, +10), random.randint(-10, +10)
q = random.randint(-10, +10)

r = ig.Line(ig.Point(x0, y0), ig.Point(x1, y1))

#ig.VarText(-10, 14, "m = {0}", r.slope())
#ig.VarText(-10, 13, "q = {0}", r.intercept())

ig.Text(-10, 14, "m = {0}".format(.5))
ig.Text(-10, 13, "q = {0}".format(-4))

##e = r.equation()

ig.VarText(-5, -13.5, "Equazione della retta: {0}", r.equation())
#ig.VarText(-5, -13, "Equazione della retta: {0}", r.getqm())

###
# Messaggio finale
###

ig.Text(-5, -11, """Muovi la retta in modo che abbia
coefficiente angolare e intercetta dati.""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
