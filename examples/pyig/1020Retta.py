#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig--------------------1020Retta.py--#
#                                                                       #
#                         Retta per due punti                           #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Il primo strumento della geometria � quello che permette di tracciare
Una retta passante per due punti.
Per due punti passa una e una sola retta.

Problema
disegnare due punti e la retta che li contiene.

Soluzione
Mettere in due variabili due oggetti della classe Point.
Creare un oggetto della classe Line passandogli come argomenti i due 
oggetti Point.
"""

###
# Chiamata della libreria pyig
###

import pygraph.pyig as ig

###
# Creazione di una finestra interattiva
###

piano = ig.InteractivePlane()

###
# Disegna due punti
###

a = ig.Point(7, -8)
b = ig.Point(-5, 1)

###
# disegna una retta per i due punti
###

ig.Line(a, b)

###
# Messaggio finale
###

ig.Text(-7, -5, """Retta passante per due punti
Muovi i punti, prova a muovere la retta""")

###
# Attivazione della finestra interattiva
###

piano.mainloop()
