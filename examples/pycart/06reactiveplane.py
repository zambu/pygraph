#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python--------------06reactiveplane.py--#
#                                                                       #
#                           Piano reattivo                              #
#                                                                       #
# Copyright (c) 2013 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
Reactive Plane
"""

from __future__ import division, print_function
import pygraph.pycart as cg


def onkeypress(event):
    print("Key pressed:", event.char)


def onkeyrelease(event):
    print("Key released:", event.char)


def onenter(event):
    print("Enter at", event)


def onleave(event):
    print("Leave at", event)


def onpress1(event):
    print("Left clicked at", event.x, event.y)


def onpress2(event):
    print("Center clicked at", event.x, event.y)


def onpress3(event):
    print("Right clicked at", event.x, event.y)


def onmotion1(event):
    print("Left motion at", event.x, event.y)


def onmotion2(event):
    print("Center motion at", event.x, event.y)


def onmotion3(event):
    print("Right motion at", event.x, event.y)


def onrelease1(event):
    print("Left released at", event.x, event.y)


def onrelease2(event):
    print("Center released at", event.x, event.y)


def onrelease3(event):
    print("Right released at", event.x, event.y)


piano = cg.Plane('04. events', color='pink')
piano.onkeypress(onkeypress)
piano.onkeyrelease(onkeyrelease)
piano.onenter(onenter)
piano.onleave(onleave)
piano.onpress1(onpress1)
piano.onpress2(onpress2)
piano.onpress3(onpress3)
piano.onmotion1(onmotion1)
piano.onmotion2(onmotion2)
piano.onmotion3(onmotion3)
piano.onrelease1(onrelease1)
piano.onrelease2(onrelease2)
piano.onrelease3(onrelease3)
biro = cg.Pen()
biro.drawtext('Clicca in vari punti del piano', (0, 9), 'navy', 2)
biro.drawtext('con i tre tasti del mouse', (0, 7), 'navy', 2)

piano.mainloop()
