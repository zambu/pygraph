#!/usr/bin/env python
#-------------------------------python-----------------------figure.py--#
#                                                                       #
#                               Figure                                  #
#                                                                       #
#--Bruno Stecca--Daniele Zambelli--------------------------------2002---#

import pygraph.pycart as cg

#------------
# Posizione
#------------


class Posizione(object):
    def __init__(self, x=0, y=0):
        self.x, self.y = x, y

    def get_coordinate(self):
        return (self.x, self.y)

    def set_coordinate(self, x, y):
        self.x, self.y = x, y

    def move(self, x, y):
        self.x += x
        self.y += y

#------------
# Punto
#------------


class Punto(Posizione):
    def __init__(self, x=0, y=0, dim=1, colore='black'):
        Posizione.__init__(self, x, y)
        self.dim = dim
        self.colore = colore
        self.setbounds()
        self.crea()

    def setbounds(self):
        self.bbox = piano._r2s((self.x - self.dim, self.y + self.dim,
                                self.x + self.dim, self.y - self.dim))

    def crea(self):
        self.o = canvas.create_oval(self.bbox, fill=self.colore)

    def disegna(self):
        self.setbounds()
        canvas.coords(self.o, self.bbox)

    def delete(self):
        piano.delete(self.o)

    def move_to(self, x, y):
        Posizione.set_coordinate(self, x, y)
        self.disegna()

    def move(self, x, y):
        Posizione.move(self, x, y)
        canvas.move(self.o, piano.scala((x, y)))

#------------
# Cerchio
#------------


class Cerchio(Punto):
    def __init__(self, x=0, y=0, r=0, dim=1, colore='black'):
        '''Disegna un cerchio'''
        self.raggio = r
        Punto.__init__(self, x, y, dim, colore)

    def setbounds(self):
        self.bbox = piano._r2s((self.x - self.raggio, self.y - self.raggio,
                                self.x + self.raggio, self.y + self.raggio))

    def crea(self):
        self.o = canvas.create_oval(self.bbox, width=self.dim,
                                    fill=self.colore)

    def set_raggio(self, r):
        self.raggio = r
        self.setbounds()
        self.disegna()

#------------
# Quadrato
#------------


class Quadrato(Cerchio):
    def __init__(self, x=0, y=0, l=0, dim=1, colore='black'):
        self.lato = l
        Cerchio.__init__(self, x, y, l / 2, dim, colore)

    def crea(self):
        self.o = canvas.create_rectangle(
            self.bbox, width=self.dim, fill=self.colore)

    def set_lato(self, l):
        self.lato = l
        self.raggio = l / 2
        self.setbounds()
        self.disegna()

#------------
# Ellisse
#------------


class Ellisse(Cerchio):
    def __init__(self, x=0, y=0, aa=0, ab=0, dim=1, colore='black'):
        self.raggio1 = ab
        Cerchio.__init__(self, x, y, aa, dim, colore)

    def setbounds(self):
        self.bbox = piano._r2s((self.x - self.raggio, self.y - self.raggio1,
                                self.x + self.raggio, self.y + self.raggio1))

    def set_assi(self, aa, ab):
        self.raggio = aa
        self.raggio1 = ab
        self.setbounds()
        self.disegna()

#------------
# Rettangolo
#------------


pass

piano = cg.Plane('figure in movimento', sx=1, axes=False)
canvas = piano.getcanvas()


def prova():
    # Esempi di uso delle classi precedenti e prova di alcuni metodi
    q1 = Quadrato(dim=5, colore='green')
    c1 = Cerchio(dim=5, colore='blue')
    e1 = Ellisse(dim=5, colore='yellow')
    p1 = Punto(dim=5, colore='red')
    for i in range(1, 90, 2):
        p1.move_to(i, i * i / 50)
        c1.set_raggio(i)
        q1.set_lato(i * 2)
        e1.move_to(i, i * i / 50)
        e1.set_assi(i / 2, i / 4)
        canvas.update()
        piano.after(50)
    piano.mainloop()


if __name__ == "__main__":
    prova()
