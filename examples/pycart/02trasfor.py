#!/usr/bin/env python
#-------------------------------python----------------------trasfor.py--#
#                                                                       #
#                Trasformazioni nel piano cartesiano                    #
#                                                                       #
#--Daniele Zambelli----------------------------------------------2003---#

import pygraph.pycart as cg

# def trasla(vertici, t):
##  def traslapunto(v): return (v[0]+t[0], v[1]+t[1])
# return map(traslapunto, vertici)
##
# def simmo(vertici):
##  def simmopunto(v): return (-v[0], -v[1])
# return map(simmopunto, vertici)
##
# def simmx(vertici):
##  def simmxpunto(v): return (v[0], -v[1])
# return map(simmxpunto, vertici)
##
# def simmy(vertici):
##  def simmypunto(v): return (-v[0], v[1])
# return map(simmypunto, vertici)
##
# def omotetia(vertici, k):
##  def omotetiapunto(v): return (v[0]*k, v[1]*k)
# return map(omotetiapunto, vertici)
##


def trasla(vertici, t):
    return tuple(map(lambda v: (v[0] + t[0], v[1] + t[1]), vertici))


def simmo(vertici):
    return tuple(map(lambda v: (-v[0], -v[1]), vertici))


def simmx(vertici):
    return tuple(map(lambda v: (v[0], -v[1]), vertici))


def simmy(vertici):
    return tuple(map(lambda v: (-v[0], v[1]), vertici))


def omotetia(vertici, k):
    return tuple(map(lambda v: (v[0] * k, v[1] * k), vertici))


piano = cg.Plane("Trasformazioni nel piano cartesiano", sx=1, axes=True)
biro = cg.Pen(width=2)
poly_1 = [(20, 20), (80, 20), (80, 60), (40, 60)]
poly_2 = trasla(poly_1, (-30, 50))
poly_3 = simmo(poly_1)
poly_4 = simmx(poly_1)
poly_5 = simmy(poly_2)
poly_6 = omotetia(poly_1, 0.5)
poly_7 = omotetia(poly_1, -3.2)
biro.drawpoly(poly_1)
biro.color = 'red'
biro.drawpoly(poly_2)
biro.color = 'green'
biro.drawpoly(poly_3)
biro.color = 'pink'
biro.drawpoly(poly_4)
biro.color = 'blue'
biro.drawpoly(poly_5)
biro.color = 'brown'
biro.drawpoly(poly_6)
biro.drawpoly(poly_7)
piano.mainloop()
