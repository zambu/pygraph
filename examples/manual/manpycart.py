# -*- coding: iso-8859-15 -*-
#-------------------------------python--------------------manpycart.py--#
#                                                                       #
#          Esempi del manuale relativi al piano cartesiano              #
#                                                                       #
#--Daniele Zambelli-----------Licence GPL------------------------2010---#

from __future__ import division, print_function
import random
import libmanex
import pygraph.pycart as cg

######################################
#        ``class Plane``             #
#        ---------------             #
######################################


def ex_00():
    """version():
    Controllare la versione della libreria."""
    piano = cg.Plane('Version')
    import pycart as pc
    if pc.version() < "02.05.00":
        print("versione un po' vecchiotta")
    else:
        print("versione:", pc.version())
    libmanex.end(piano)


def ex_01():
    """origin:
    Crea un piano, sposta l'origine e ridisegna gli assi."""
    piano = cg.Plane('origin')
    piano.origin = (10, 300)
    piano.axes()
    libmanex.end(piano)


def ex_02():
    """scale:
    Crea un piano, modifica la scala e l'origine ridisegna assi e griglia."""
    piano = cg.Plane('scale', sx=15, axescolor='green')
    x_o, y_o = piano.origin
    sx, sy = piano.scale
    piano.origin = (x_o - 4 * sx, y_o + 6 * sy)
    piano.scale = (30, 20)
    piano.axes(color='blue')
    piano.grid(color='red')
    libmanex.end(piano)


def ex_03():
    """__init__():
    Creare 3 piani cartesiani,
    il primo: con tutti i valori di default,
    il secondo: quadrato, con un titolo e con gli assi,
    il terzo: con un titolo, con le dimensioni di 400 per 200,
    con la scala e l'origine cambiati, con assi e griglia colorati."""
    p_0 = cg.Plane('Primo piano')
    p_1 = cg.Plane(name="Secondo piano", w=400, h=400, axes=True)
    p_2 = cg.Plane(name="Terzo piano",
                   w=400, h=200,
                   sx=10, sy=30,
                   ox=40, oy=None,
                   axescolor='orange', gridcolor='red')
    libmanex.end(p_0)


def ex_051():
    """mainloop():
    Disegna un piano se ``IDLE`` � stato avviato senza sottoprocessi,
    con il parametro "-n"."""
    piano = cg.Plane('ex_051: un piano', axescolor='pink')


def ex_052():
    """mainloop():
    Disegna un piano se ``IDLE`` � stato avviato con sottoprocessi,
    senza il parametro "-n"."""
    piano = cg.Plane('Altro piano', axescolor='olive drab')
    piano.mainloop()


def ex_06():
    """after():
    Disegna un quadrato in movimento."""
    piano = cg.Plane('Quadrato in movimento',
                     sx=1, sy=1, axes=False, grid=False)
    biro = piano.newPen(width=4)
    for i in range(0, 500, 2):
        color = '#ff{0:02x}00'.format(i // 2)
        verts = ((-300 + i, -30), (-220 + i, -50),
                 (-200 + i, 30), (-280 + i, 50))
        id = biro.drawpoly(verts, color)
        piano.after(10)
        piano.delete(id)
    id = biro.drawpoly(verts, 'green')
    libmanex.end(piano)


def ex_07():
    """axes():
    Disegna due sistemi di riferimento con uguale scala ma origini diverse."""
    piano = cg.Plane('Sistema di riferimento traslato')
    piano.origin = (80, 60)
    piano.axes('red')
    libmanex.end(piano)


def ex_08():
    """grid():
    Disegna in sequenza un piano con assi e griglia, senza assi,
    senza griglia e completamente bianco."""
    piano = cg.Plane('ex_08: Assi e griglie',  sx=12,  sy=36,
                     axescolor='green', gridcolor='red')
    biro = piano.newPen(x=-10, y=3, color='red', width=2)
    biro.drawtext('assi e griglia, attendere... 4')
    ritardo = 1000
    piano.after(ritardo)
    piano.clean()
    piano.axes('purple')
    biro.drawtext('solo assi, attendere... 3')
    piano.after(ritardo)
    piano.clean()
    piano.grid('#22aa55')
    biro.drawtext('solo griglia, attendere... 2')
    piano.after(ritardo)
    piano.reset()
    biro.drawtext('situazione iniziale, attendere... 1')
    piano.after(ritardo)
    piano.clean()
    biro.drawtext('bianco, finito!')
    libmanex.end(piano)


def ex_11():
    """newPen():
    Disegna un quadrato."""
    piano = cg.Plane('Quadrato', w=400, h=400)
    biro = piano.newPen(width=4, color='blue')
    biro.drawpoly(((-7, -7), (7, -7), (7, 7), (-7, 7)))
    libmanex.end(piano)


def ex_13():
    """clean():
    Disegna un quadrato che scoppia."""
    piano = cg.Plane('Quadrato che scoppia', sx=1, axes=False, grid=False)
    biro = piano.newPen(width=4, color='blue')
    a = 3
    b = 1
    for i in range(162):
        verts = (((-a * i, -b * i), (b * i, -a * i),
                  (a * i,  b * i), (-b * i, a * i)))
        biro.drawpoly(verts)
        piano.after(10)
        piano.clean()
        biro.drawpoly(verts)
    libmanex.end(piano)


def ex_14():
    """reset():
    Disegna gli assi con la griglia, un poligono verde, attendere un po',
    poi cancellare tutto ridisegnando gli assi e ririsegna il poligono con un
    altro colore."""
    piano = cg.Plane('Poligono', axes=False, grid=False)
    piano.axes()
    vertici = ((-2, -3), (4, -1), (6, 4), (-5, 6))
    biro = piano.newPen(width=20, color='blue')
    biro.drawpoly(vertici)
    piano.after(500)
    piano.reset()
    biro.color = "green"
    biro.width = 10
    biro.drawpoly(vertici)
    libmanex.end(piano)


def ex_15():
    """delete()
    Disegna un segmento che scivola sugli assi."""
    piano = cg.Plane('Segmento che scivola sugli assi', sx=1)
    biro = piano.newPen(width=5, color='navy')
    lung = 200
    for y in range(200, 0, -1):
        x = (lung * lung - y * y)**.5
        s = biro.drawsegment((0, y), (x, 0))
        piano.after(4)
        piano.delete(s)
    biro.drawsegment((0, 0), (lung, 0))
    libmanex.end(piano)


def ex_16():
    """save():
    Produrre un file che contiene il disegno di un quadrato."""
    piano = cg.Plane('ex_16: Quadrato')
    biro = piano.newPen(width=6, color='pink')
    q = ((-5, -3), (3, -5), (5, 3), (-3, 5))
    biro.drawpoly(q)
    piano.save('quadrato')
    libmanex.end(piano)


def ex_17():
    """getcanvaswidth, getcanvaseight():
    Disegna un rettangolo che circonda la finestra grafica."""
    piano = cg.Plane('Cornice', sx=1, axes=False, grid=False)
    biro = piano.newPen(width=4, color='green')
    bordo = 10
    w = piano.getcanvaswidth() // 2 - bordo
    h = piano.getcanvasheight() // 2 - bordo
    biro.drawpoly(((-w, -h), (w, -h), (w, h), (-w, h)))
    libmanex.end(piano)


def ex_18():
    """getcanvas():
    Questo metodo prevede la conoscenza
    della libreria Tkinter, essendo ci� al di fuori della portata di questo
    manuale, non viene proposto nessun esempio.

  Si pu� trovare un loro uso nel programma ``viewfun.py`` distribuito
  assieme alla libreria pygraph."""
    pass

######################################
#        ``class Pen``               #
#        -------------               #
######################################


def ex_30():
    """__init__():
    Crea 4 penne diverse e le fa convergere nell'origine."""
    piano = cg.Plane('__init__', sx=100, sy=100, grid=False)
    p_0 = cg.Pen(x=1,  y=1,  color='violet', width=9)
    p_1 = cg.Pen(x=-1, y=1,  color='magenta', width=7)
    p_2 = cg.Pen(x=-1, y=-1, color='gold', width=5)
    p_3 = cg.Pen(x=1,  y=-1, color='navy', width=3)
    for biro in (p_0, p_1, p_2, p_3):
        biro.drawto((0, 0))
    libmanex.end(piano)


def ex_31():
    """__init__():
    Crea 4 piani e in ognuno una penna e fa disegnare un punto ad ogni penna."""
    p_0 = cg.Plane('piano 0 violet', sx=100, sy=100, grid=False)
    biro0 = cg.Pen(x=1,  y=1,  color='violet', width=20)
    p_1 = cg.Plane('piano 1 green', sx=100, sy=100, grid=False)
    biro1 = cg.Pen(x=-1, y=1,  color='green', width=20)
    p_2 = cg.Plane('piano 2 gold', sx=100, sy=100, grid=False)
    p_3 = cg.Plane('piano 3 navy', sx=100, sy=100, grid=False)
    biro2 = cg.Pen(x=-1, y=-1, color='gold', width=20, plane=p_2)
    biro3 = p_3.newPen(x=1,  y=-1, color='navy', width=20)
    for biro in (biro0, biro1, biro2, biro3):
        biro.drawpoint()
    libmanex.end(p_0)


def ex_32():
    """position:
    Disegna 2 quadrati in posizioni casuali."""
    piano = cg.Plane('position')
    biro = cg.Pen()
    lato = random.randrange(5) + 3
    biro.position = (random.randrange(-14, 10), random.randrange(-9, 5))
    x, y = biro.position
    vertici = ((x, y), (x + lato, y), (x + lato, y + lato), (x, y + lato))
    biro.drawpoly(vertici, color='gold', width=6)
    lato = random.randrange(5) + 3
    biro.position = (random.randrange(-14, 10), random.randrange(-9, 5))
    x, y = biro.position
    vertici = ((x, y), (x + lato, y), (x + lato, y + lato), (x, y + lato))
    biro.drawpoly(vertici, color='pink', width=6)
    libmanex.end(piano)


def ex_33():
    """color:
    Disegna linee di diverso colore."""
    piano = cg.Plane('color')
    colors = ['red', 'green', 'blue', 'pink', 'yellow',
              'navy', 'gold', 'magenta', '#a0a0a0', (0.7, 0.5, 0.1)]
    biro = cg.Pen(width=10)
    for i, color in enumerate(colors):
        biro.color = color
        print(biro.color)
        biro.drawsegment((-5, i - 4), (5, i - 4))
    libmanex.end(piano)


def ex_34():
    """width:
    Disegna linee di diverso spessore."""
    piano = cg.Plane('width')
    biro = cg.Pen()
    for i in range(10):
        biro.width = i * 2
        biro.drawsegment((-5, i - 4), (5, i - 4))
    libmanex.end(piano)


def ex_35():
    """drawto():
    Tracciare una linea tratteggiata in diagonale sullo schermo."""
    piano = cg.Plane('drawto', sx=1, axes=False, grid=False)
    biro = cg.Pen(width=2, color='blue')
    x = -200
    y = -100
    biro.position = (x, y)
    for i in range(25):
        x += 10
        y += 5
        biro.drawto((x, y))
        x += 6
        y += 3
        biro.position = (x, y)
    libmanex.end(piano)


def ex_36():
    """drawsegment():
    Disegna i lati e le diagonali di un pentagono."""
    piano = cg.Plane('drawsegment', sx=30)
    biro = cg.Pen(width=4, color='gold')
    vertici = ((-3, -4), (+3, -4), (+5, +1), (0, +5), (-5, +1))
    for i, v0 in enumerate(vertici):
        for v1 in vertici[i + 1:]:
            biro.drawsegment(v0, v1)
    libmanex.end(piano)


def ex_37():
    """drawpoint():
    Disegna 100 punti di colore e spessore diverso."""
    def randcolor():
        return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256),
                                               random.randrange(256),
                                               random.randrange(256))

    piano = cg.Plane('drawpoint', sx=1, grid=False)
    biro = cg.Pen(color='red', width=200)
    biro.drawpoint()
    for cont in range(100):
        biro.drawpoint((random.randrange(-100, 100),
                        random.randrange(-100, 100)),
                       color=randcolor(), width=random.randrange(30) + 1)
    libmanex.end(piano)


def ex_38():
    """drawcircle():
    Disegna 100 circonferenze."""
    def randcolor():
        return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256),
                                               random.randrange(256),
                                               random.randrange(256))

    piano = cg.Plane('drawcircle', sx=1, axes=False, grid=False)
    biro = cg.Pen(color='red')
    biro.drawcircle(200)
    for cont in range(100):
        biro.drawcircle(radius=random.randrange(80),
                        center=(random.randrange(-100, 100),
                                random.randrange(-100, 100)),
                        color=randcolor(), width=random.randrange(10),
                        incolor=randcolor())
    libmanex.end(piano)


def ex_39():
    """drawpoly():
    Disegna un pentagono casuale."""
    piano = cg.Plane('drawpoly', sx=10, grid=False)
    biro = cg.Pen(width=4, color='blue')
    q = []
    for i in range(5):
        q.append((10 - random.randrange(20), 10 - random.randrange(20)))
    biro.drawpoly(q, incolor='pink')
    libmanex.end(piano)


def ex_40():
    """drawtext():
    Scrive alcune parole poco sensate."""
    piano = cg.Plane('drawpoly')
    biro = cg.Pen(x=-5, y=7, color='blue')
    biro.drawtext('testino')
    biro.position = (-5, 3)
    biro.drawtext('testo', color='magenta', width=2)
    biro.position = (-5, -2)
    biro.drawtext('testone', (-5, -2), color='green', width=4)
    libmanex.end(piano)


def ex_41():
    """Disegna "punti" con diverse forme.::"""

    piano = cg.Plane('"Punti"', w=400, h=400, sx=10, axes=False, grid=False)
    biro = piano.newPen(width=15, shape=cg.ICS)
    colors = ('red', 'gold', 'blue', 'green', 'pink', 'violet',
              'turquoise', 'light cyan')
    for forma, colore in zip(cg.SHAPES, colors):
        posizione = (random.randrange(-10, 10),
                     random.randrange(-10, 10))
        biro.drawpoint(position=posizione, color=colore, shape=forma)
    piano.mainloop()


###
# Main
##
ex_41()
#libmanex.doall(locals(), 'ex_')
