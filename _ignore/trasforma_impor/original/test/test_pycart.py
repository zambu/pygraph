#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python------------------test_pycart.py--#
#                                                                       #
#                           Test for pycart                             #
#                                                                       #
#--Daniele Zambelli-----------License-GPL------------------------2013---#

"""
pycart tests
"""

from __future__ import division, print_function
from libtest import end,  alltests
import pycart as pc
import random

def randcolor():
    """Return a random RGB color."""
    return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256),
                                           random.randrange(256),
                                           random.randrange(256))

def test_00():
    """Controllare la versione della libreria.
       uso di tutti i valori predefiniti."""
    if pc.version() < '2.9.00':
        print("versione un po' vecchiotta")
        exit(1)
    else:
        print("versione ", pc.version(), sep=' ')

def test_01():
    """Piano con tutti i valori predefiniti."""
    c_p = pc.Plane('01. Plane vanilla')
    end(c_p)

def test_02():
    """Modifica alcuni valori predefiniti."""
    c_p = pc.Plane('02. sx=30; sy=20',
                   w=400, h=600,
                   sx=30,  sy=20,
                   ox=100,  oy=200,
                   axescolor='green', gridcolor='red', color='yellow')
    end(c_p)

def test_03():
    """scale:
    Crea un piano, modifica la scala e l'origine ridisegna assi e griglia."""
    c_p = pc.Plane('03. scale', sx=15, axescolor='green', color='AntiqueWhite3')
    c_p.after(500)
    x_o, y_o = c_p.origin
    s_x, s_y = c_p.scale
    c_p.origin = (x_o-4*s_x, y_o+6*s_y)
    c_p.scale = (30, 20)
    prevcolor = c_p.color
    c_p.color = 'yellow'
    c_p.axes(color='blue')
    c_p.grid(color='red')
    c_p.after(1000)
    c_p.color = prevcolor
    end(c_p)

def test_04():
    """Reactive Plane."""
    def onkeypress(event):
        print("Key pressed:", event.char)
    def onkeyrelease(event):
        print("Key released:", event.char)
    def onenter(event):
        print("Enter at", event)
    def onleave(event):
        print("Leave at", event)
    def onpress1(event):
        print("Left clicked at", event.x, event.y)
    def onpress2(event):
        print("Center clicked at", event.x, event.y)
    def onpress3(event):
        print("Right clicked at", event.x, event.y)
    def onmotion1(event):
        print("Left motion at", event.x, event.y)
    def onmotion2(event):
        print("Center motion at", event.x, event.y)
    def onmotion3(event):
        print("Right motion at", event.x, event.y)
    def onrelease1(event):
        print("Left released at", event.x, event.y)
    def onrelease2(event):
        print("Center released at", event.x, event.y)
    def onrelease3(event):
        print("Right released at", event.x, event.y)
    c_p = pc.Plane('04. events', color='pink')
    c_p.onkeypress(onkeypress)
    c_p.onkeyrelease(onkeyrelease)
    c_p.onenter(onenter)
    c_p.onleave(onleave)
    c_p.onpress1(onpress1)
    c_p.onpress2(onpress2)
    c_p.onpress3(onpress3)
    c_p.onmotion1(onmotion1)
    c_p.onmotion2(onmotion2)
    c_p.onmotion3(onmotion3)
    c_p.onrelease1(onrelease1)
    c_p.onrelease2(onrelease2)
    c_p.onrelease3(onrelease3)
    biro = pc.Pen()
    biro.drawtext('Clicca in vari punti del piano', (0, 9), 'navy', 2)
    biro.drawtext('con i tre tasti del mouse', (0, 7), 'navy', 2)
    end(c_p)

def test_10():
    """Disegnare un quadrato con spessore di 4 pixel."""
    c_p = pc.Plane('10. Poligono', sx=5, gridcolor='grey', color='AliceBlue')
    c_p.axes()
    biro = pc.Pen(color='magenta', width=4)
    a = 25
    b = 15
    square = ((-a, -b), (b, -a), (a, b), (-b, a))
    biro.drawpoly(square)
    end(c_p)

def test_11():
    """Disegnare tre figure di diverso colore."""
    c_p = pc.Plane('11. Poligoni colorati', sx=1, color='#f0f0f0')
    biro = c_p.newPen(width=4)
    square = ((-50-200, -30),(30-200, -50),(50-200, 30),(-30-200, 50))
    biro.drawpoly(square, color='red')
    square = ((-50-150, -30),(30-150, -50),(50-150, 30),(-30-150, 50))
    biro.drawpoly(square, color='#fabdc3')
    square = ((-50-100, -30),(30-100, -50),(50-100, 30),(-30-100, 50))
    biro.drawpoly(square, color=(0.8, 0.75, 0.56))
    end(c_p)

def test_12():
    """Disegnare un quadrato in movimento."""
    c_p = pc.Plane('12. Quadrato in movimento', 
                 sx=1, sy=1, axes=False, grid=False)
    biro = pc.Pen(width=4)
    for i in range(0, 500, 2):
        color = '#ff{0:02x}00'.format(i//2)
        verts = ((-300+i, -30), (-220+i, -50), (-200+i, 30), (-280+i, 50))
        poly = biro.drawpoly(verts, color)
        c_p.after(10)
        c_p.delete(poly)
    biro.drawpoly(verts, 'green')
    end(c_p)

def test_13():
    """Disegnare un segmento che scivola sugli assi."""
    c_p = pc.Plane('13. Segmento che scivola sugli assi', sx=1)
    biro = pc.Pen(width=4, color='navy')
    lung = 200
    for y in range(200, 0, -1):
        x = (lung*lung-y*y)**.5
        seg = biro.drawsegment((0, y), (x, 0))
        c_p.after(4)
        c_p.delete(seg)
    for y in range(0, -200, -1):
        x = (lung*lung-y*y)**.5
        seg = biro.drawsegment((0, y), (x, 0))
        c_p.after(4)
        c_p.delete(seg)
    for y in range(-200, 0):
        x = -(lung*lung-y*y)**.5
        seg = biro.drawsegment((0, y), (x, 0))
        c_p.after(4)
        c_p.delete(seg)
    for y in range(200):
        x = -(lung*lung-y*y)**.5
        seg = biro.drawsegment((0, y), (x, 0))
        c_p.after(4)
        c_p.delete(seg)
    biro.drawsegment((0, 0), (0, lung))
    end(c_p)

def test_14():
    """Disegnare un rettangolo che circonda la finestra grafica."""
    c_p = pc.Plane('14. Cornice', sx=1, axes=False, grid=False)
    biro = pc.Pen(width=4, color='green')
    bordo = 10
    w_2 = c_p.getcanvaswidth()//2-bordo
    h_2 = c_p.getcanvasheight()//2-bordo
    biro.drawpoly(((-w_2, -h_2), (w_2, -h_2), (w_2, h_2), (-w_2, h_2)))
    end(c_p)

def test_15():
    """Disegnare un poligono, dopo mezzo secondo cancellare tutto e
       ridisegnarlo con un altro colore."""
    c_p = pc.Plane('15. Poligono')
    vertici = ((-2, -3), (4, -1), (6, 4), (-5, 6))
    biro = pc.Pen(color="red", width=20)
    biro.drawpoly(vertici)
    c_p.after(500)
    c_p.reset()
    biro.drawpoly(vertici, color="green")
    end(c_p)

def test_16():
    """Tracciare una linea tratteggiata in diagonale sullo schermo."""
    c_p = pc.Plane('16. Linea tratteggiata', sx=1, axes=False, grid=False)
    x_i = -200
    y_i = -100
    biro = pc.Pen(x=x_i, y=y_i, color='blue', width=4)
    for _ in range(25):
        x_i += 10
        y_i += 5
        biro.drawto((x_i, y_i))
        x_i += 6
        y_i += 3
        biro.position = (x_i, y_i)
    end(c_p)

def test_17():
    """Produrre un file che contiene il disegno di un esagono."""
    c_p = pc.Plane("17. Salva l'immagine", color=randcolor())
    biro = pc.Pen(color=randcolor(), width=4)
    exa = []
    for _ in range(6):
        exa.append((10-random.randrange(20), 10-random.randrange(20)))
    biro.drawpoly(exa)
##    c_p.save(str(b'esagono', encoding='utf-8'))
    c_p.save('esagono')
    end(c_p)

def test_18():
    """Disegnare un pentagono casuale."""
    c_p = pc.Plane('18. Pentagono casuale', axes=True)
    biro = pc.Pen(color='blue', width=4)
    penta = []
    for _ in range(5):
        penta.append((10-random.random()*20, 10-random.random()*20))
    biro.drawpoly(penta)
    end(c_p)

def test_19():
    """Disegna un pentagono casuale con i vertici sulla griglia."""
    c_p = pc.Plane('19. Pentagono casuale con i vertici sulla griglia',
               axes=False)
    biro = pc.Pen(color='red', width=4)
    penta = []
    for _ in range(5):
        penta.append((10-random.randrange(20), 10-random.randrange(20)))
    biro.drawpoly(penta)
    end(c_p)

def test_20():
    """Disegna elementi geometrici in diversi piani."""
    def drawpoints(pen):
        pen.position = (6, -2)
        pen.drawpoint(shape=pc.RECT)
        pen.drawpoint((-6, 9), color=(0.2, 0.8, 0.55))
    def drawcircles(pen):
        pen.drawcircle(6, (-6, -2), 
                       incolor='DarkOrchid4')
        pen.drawcircle(2, (6, 7), 
                       color='DarkOrchid4', incolor='dark khaki')
    def drawsegments(pen):
        pen.drawsegment((-4, 5), color='#228b22', width=5)
        pen.drawsegment((-4, 5), (7, 2), color='orange', width=8)
    def drawpolys(pen):
        pen.drawpoly(((-4, -1), (-1, -5), (3, -3), (1, -1)), 
                       color = (0.5, 0.6, 0.7), width = 10, incolor='grey84')
        pen.drawpoly(((4, -6), (7, -9), (9, -5), (5, -3)), 
                        color = (0.5, 0.6, 0.7), width = 6, incolor='DarkOrange')
    
    c_p = pc.Plane('20.0. Punti, segmenti, circonferenze',
                  sx=20, axes=True, grid=True, 
                  axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro0 = pc.Pen(color='dark khaki', width=5)
    pc.Plane('20.1. Punti',
             sx=20, axes=True, grid=True, 
             axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro1 = pc.Pen(color='dark khaki', width=5)
    drawpoints(biro0)
    drawpoints(biro1)
    pc.Plane('20.2. Circonferenze',
             sx=20, axes=True, grid=True, 
             axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro1 = pc.Pen(color='dark khaki', width=5)
    drawcircles(biro0)
    drawcircles(biro1)
    pc.Plane('20.3. Segmenti',
             sx=20, axes=True, grid=True, 
             axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro1 = pc.Pen(color='dark khaki', width=5)
    drawsegments(biro0)
    drawsegments(biro1)
    pc.Plane('20.4. Poligono',
             sx=20, axes=True, grid=True, 
             axescolor='olive drab', gridcolor='DeepSkyBlue2')
    biro1 = pc.Pen(color='dark khaki', width=5)
    drawpolys(biro0)
    drawpolys(biro1)
    end(c_p)

def test_21():
    """Verifica del funzionamento di assi e griglia.
    Attendere, impiega 4 secondi."""
    c_p = pc.Plane('21. Assi e griglie',  sx=10,  sy=20,
                   axescolor='grey', gridcolor='tomato')
    pen = c_p.newPen(x=-10, y=3, color='red', width=2)
    pen.drawtext('attendere qualche secondo... 4')
    ritardo = 1000
    c_p.origin = (342, 218)
    c_p.scale = (20, 40)
    c_p.after(ritardo)
    c_p.axes('purple')
    c_p.after(ritardo)
    c_p.grid('#22aa55')
    c_p.after(ritardo)
    c_p.reset()
    pen.drawtext('attendere qualche secondo... 3')
    c_p.after(ritardo)
    c_p.clean()
    pen.drawtext('attendere qualche secondo... 2')
    c_p.grid('#aa5522')
    c_p.after(ritardo)
    c_p.clean()
    pen.drawtext('attendere qualche secondo... 1')
    c_p.after(ritardo)
    c_p.axes('#55bb22')
    c_p.after(ritardo)
    c_p.reset()
    pen.drawtext('finito!')
    end(c_p)

def test_22():
    """Verifica del funzionamento di penwidth."""
    c_p = pc.Plane('22. penwidth', sx=20,  sy=20)
    biro = pc.Pen()
    for i in range(10):
        biro.width = i*2
        biro.drawsegment((-5, i-4), (5, i-4))
    end(c_p)

def test_23():
    """Verifica del funzionamento di color."""
    c_p = pc.Plane('23. color', w=620, h=700, sx=1,
                   axescolor='#4000a0', gridcolor='#a00040')
    colors = ['AliceBlue', 'AntiqueWhite', 'AntiqueWhite1', 'AntiqueWhite2',
              'AntiqueWhite3', 'AntiqueWhite4', 'BlanchedAlmond', 'BlueViolet',
              'CadetBlue', 'CadetBlue1', 'CadetBlue2', 'CadetBlue3',
              'CadetBlue4', 'CornflowerBlue', 'DarkBlue', 'DarkCyan',
              'DarkGoldenrod', 'DarkGoldenrod1', 'DarkGoldenrod2',
              'DarkGoldenrod3', 'DarkGoldenrod4', 'DarkGray', 'DarkGreen',
              'DarkGrey', 'DarkKhaki', 'DarkMagenta', 'DarkOliveGreen',
              'DarkOliveGreen1', 'DarkOliveGreen2', 'DarkOliveGreen3',
              'DarkOliveGreen4', 'DarkOrange', 'DarkOrange1', 'DarkOrange2',
              'DarkOrange3', 'DarkOrange4', 'DarkOrchid', 'DarkOrchid1',
              'DarkOrchid2', 'DarkOrchid3', 'DarkOrchid4', 'DarkRed',
              'DarkSalmon', 'DarkSeaGreen', 'DarkSeaGreen1', 'DarkSeaGreen2',
              'DarkSeaGreen3', 'DarkSeaGreen4', 'DarkSlateBlue',
              'DarkSlateGray', 'DarkSlateGray1', 'DarkSlateGray2',
              'DarkSlateGray3', 'DarkSlateGray4', 'DarkSlateGrey',
              'DarkTurquoise', 'DarkViolet', 'DebianRed', 'DeepPink',
              'DeepPink1', 'DeepPink2', 'DeepPink3', 'DeepPink4', 'DeepSkyBlue',
              'DeepSkyBlue1', 'DeepSkyBlue2', 'DeepSkyBlue3', 'DeepSkyBlue4',
              'DimGray', 'DimGrey', 'DodgerBlue', 'DodgerBlue1', 'DodgerBlue2',
              'DodgerBlue3', 'DodgerBlue4', 'FloralWhite', 'ForestGreen',
              'GhostWhite', 'GreenYellow', 'HotPink', 'HotPink1', 'HotPink2',
              'HotPink3', 'HotPink4', 'IndianRed', 'IndianRed1', 'IndianRed2',
              'IndianRed3', 'IndianRed4', 'LavenderBlush', 'LavenderBlush1',
              'LavenderBlush2', 'LavenderBlush3', 'LavenderBlush4', 'LawnGreen',
              'LemonChiffon', 'LemonChiffon1', 'LemonChiffon2', 'LemonChiffon3',
              'LemonChiffon4', 'LightBlue', 'LightBlue1', 'LightBlue2',
              'LightBlue3', 'LightBlue4', 'LightCoral', 'LightCyan',
              'LightCyan1', 'LightCyan2', 'LightCyan3', 'LightCyan4',
              'LightGoldenrod', 'LightGoldenrod1']
    biro = pc.Pen(width=2)
    num = 100
    for i in range(num):
        biro.color = (i/num, 1-i/num, 0)
        biro.drawsegment((1, i*3+1), (300, i*3+1))
    for i in range(num):
        biro.color = (i/num, 0, 1-i/num)
        biro.drawsegment((-(i*3+1), 1), (-(i*3+1), 300))
    for i in range(num):
        biro.color = (0, i/num, 1-i/num)
        biro.drawsegment((-1, -(i*3+1)), (-300, -(i*3+1)))
    print(len(colors))
    for i, color in enumerate(colors):
        biro.color = color
        biro.drawsegment((1, -(i*3+1)), (300, -(i*3+1)))
    end(c_p)

def test_24():
    """Verifica il funzionamento di position."""
    points = ((200, 0), (100, 173), (-100, 173),
              (-200, 0), (-100, -173), (100, -173), (200, 0))
    c_p = pc.Plane('24. Esagono', sx=1, axes=False)
    biro = pc.Pen(color='red', width=8)
    biro.drawpoint()
    biro.penwidth = 4
    for point in points:
        biro.drawsegment(point, color='blue')
        biro.position = point
        biro.drawpoint(color='red')
    end(c_p)

def test_25():
    """Verifica il funzionamento di drawto."""
    points0 = ((200, 0), (100, 173), (-100, 173),
               (-200, 0), (-100, -173), (100, -173), (200, 0))
    c_p = pc.Plane('25. Esagono', sx=1, axes=False)
    biro = pc.Pen(color='red', width=8)
    for point in points0:
        biro.color = 'blue'
        biro.drawto(point)
        biro.color = 'red'
        biro.drawpoint()
    for point in points0:
        biro.drawto((point[0]/2, point[1]/2), color='green', width=4)
        biro.drawpoint()
    end(c_p)

def test_26():
    """Verifica il funzionamento di drawsegment."""
    points = ((200, 0), (100, 173), (-100, 173),
              (-200, 0), (-100, -173), (100, -173), (200, 0))
    c_p = pc.Plane('26. Esagono', sx=1, axes=False)
    biro = pc.Pen(color='red', width=8)
    biro.drawpoint()
    for point in points:
        biro.drawpoint(point)
    biro.color = 'green'
    for point in points:
        biro.drawsegment(point)
    for point in points:
        biro.drawsegment((point[0]/2, point[1]/2), color='blue', width=4)
    for point in points:
        biro.drawpoint((point[0]/2, point[1]/2), color='red', width=4)
    end(c_p)

def test_27():
    """Verifica il funzionamento di drawpoint."""
    c_p = pc.Plane('27. punti', sx=1, grid=False)
    biro = c_p.newPen(color='red', width=200)
    biro.drawpoint()
    for _ in range(100):
        biro.drawpoint((random.randrange(-100, 100),
                       random.randrange(-100, 100)),
                       color=randcolor(),
                       width=random.randrange(30)+1,
                       shape=random.randrange(2))
    end(c_p)

def test_28():
    """Verifica il funzionamento di drawcircle."""
    c_p = pc.Plane('28. cerchi', sx=1, axes=False, grid=False)
    biro = pc.Pen(color='red')
    biro.drawcircle(200)
    for _ in range(100):
        biro.drawcircle(radius=random.randrange(80),
                        center=(random.randrange(-100, 100),
                        random.randrange(-100, 100)),
                        color=randcolor(), width=random.randrange(10),
                        incolor=randcolor())
    end(c_p)

def test_29():
    """Verifica il funzionamento di drawpoly."""
    c_p = pc.Plane('29. poligoni', sx=1, axes=False)
    biro = pc.Pen(color='red', width=6)
    polygon = [(random.randrange(-300, 300, 20),
                random.randrange(-200, 200, 20)) for
               _ in range(7)]
    biro.drawpoly(polygon)
    polygon = [(random.randrange(-300, 300, 20),
                random.randrange(-200, 200, 20)) for
                _ in range(7)]
    biro.drawpoly(polygon,
                  color=randcolor(), width=random.randrange(10),
                  incolor=randcolor())
    end(c_p)

def test_30():
    """Verifica il funzionamento di drawtext."""
    # Diverse cose che non vanno: da sistemare!
    c_p = pc.Plane('30. drawtext', w=400, h=400)
    biro = c_p.newPen(color='red')
    biro.drawtext("a", (-8, 5))
    biro.width = 2
#    biro.drawtext(u"ambarab� cici coc�", (0, 3)) # in Python2.x
    biro.drawtext(u"ambarab� cici coc�", (0, 3))
    biro.width = 3
    biro.drawtext("c", (-2, 1))
    biro.width = 4
    biro.drawtext("d", (1, -2))
    end(c_p)

loc = locals()

if __name__ == '__main__':
#    test_04()
#    test_11()
#    test_12()
#    test_13()
#    test_27()
#    test_20()
    alltests(locals())
