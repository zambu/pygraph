#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-----------------1260Criteri1.py--#
#                                                                       #
#             Primo criterio di congruenza di triangoli                 #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Applicare il primo criterio di congruenza di triangoli.

Problema
- Disegna un triangolo.
- Disegna una semiretta.
- Disegna il triangolo con due lati e l'angolo compreso congruenti al primo.

Soluzione
- da inventare

"""

from pyig import *
ip = InteractivePlane()

###
# Primo triangolo
###
a0 = Point(-7, 2, color="#00f050", width=6, name="A")
b0 = Point(-4, -2, color="#00f050", width=6, name="B")
c0 = Point(-5, 6, color="#00f050", width=6, name="C")
t0 = Polygon((a0, b0, c0), color="#a87234")

# Elementi congruenti
ab0 = Segment(a0, b0)
ac0 = Segment(a0, c0)
bac0 = Angle(b0, a0, c0)

# Semiretta
a1 = Point(2, 2, color="#00f050", width=6, name="A'")
d = Point(8, -6, color="#00f050", width=6, name="D")
r1 = Ray(a1, d, width=1)

# Triangolo congruente
circ1 = Circle(a1, ab0, width=1)
b1 = Intersection(r1, circ1, 1, name="B'")
bac1 = Angle(b1, a1, bac0)
ac1 = bac1.side1(width=1)
circ2 = Circle(a1, ac0, width=1)
c1 = Intersection(ac1, circ2, 1, name="C'")
t1 = Polygon((a1, b1, c1), color="#72a834")

Text(ip, -5, -6, """Primo criterio di congruenza di triangoli.""")

ip.mainloop()
