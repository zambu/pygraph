#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-----------------1080Segmenti.py--#
#                                                                       #
#                              Segmenti                                 #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Un segmento � la parte di retta compresa tra due punti detti anche estremi
del segmento. Nel disegno di figure geometriche i segmenti hanno grande importanza.
In pyig � possibile disegnare segmenti diversificandoli per colore o
spessore. 
Lo spessore � un numero intero.
Il colore � una stringa nel formato: "#RRGGBB" dove RR, GG, BB sono numeri esadecimali che rappresentano l'intensit� della componente rispettivamente: rossa, verde e blu.

Problema
Disegnare segmenti con colori e spessori diversi; con o senza estremi.

Soluzione
Creare dei punti e i segmenti definiti da quei punti
Gli estremi del segmento possono essere creati prima o durante la creazione
del segmento stesso.
L'aspetto dei segmenti pu� essere modificato con i metodi setcolor() e setwidth().
"""

###
# Chiamata della libreria pyig
# Creazione di una finestra interattiva
###

from pyig import *
ip = InteractivePlane()

"""
 Punti creati al volo durante la creazione del segmento

   segmento con estremi visibili
"""
s1 = Segment(Point(-5, 3), Point(-4, -3.5))
"""
   segmento con estremi invisibili
"""
s2 = Segment(Point(-4, 11/3, visible=False),
           Point(-3, -7./2, visible=False))

"""
 Modifica di  stili, colori e spessori di default degli estremi e del segmento.
"""

a = Point(-3, 3)      # creo un punto cos�
a.color = "#aa0000"     # modifico colore...
a.width = 7             # e dimensione
b = Point(-2, -3.5, color="#aa0000", width=7) # oppure cos�
s3 = Segment(a, b)        # creo il segmento
s3.color = "#00aaaa"    # modifico colore
s3.width = 7            # e dimensione

x1 = 2                    # x1 � un oggetto Python che contiene il numero 2
y1 = 3
x2 = 5
y2 = -3

p1 = Point(x1, y1)    # p1 ha per coordinate il contenuto di x1 e di y1
p2 = Point(x1, y2)    # p2 ha la stessa ascissa di p1
p3 = Point(x2, y1)    # p3 ha la stessa ordinata di p1
p4 = Point(x2, y2)
# rettangolo che ha per punti p1, p2, p3, p4
s4 = Segment(p1, p2, color="#a0c0f0", width=8)
s5 = Segment(p2, p4, color="#0a0c0f", width=10)
s6 = Segment(p4, p3, color="#a0c0f0", width=12)
s7 = Segment(p3, p1, color="#0a0c0f", width=14)

###
# Messaggio finale
###

Text(-7, -5, """Segmenti di diverso colore e spessore
Muovi i segmenti.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
