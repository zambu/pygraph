#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1180Incentro.kpy--#
#                                                                       #
#         Incentro di un triangolo e circonferenza inscritta            #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Le tre bisettrici di un triangolo si intersecano in *un* punto, questo punto 
� il centro della circonferenza inscritta al triangolo. Per questo motivo
viene chiamato "incentro".

Problema
Disegnare:
- un triangolo,
- le tre bisettrici,
- la circoferenza inscritta al triangolo

Soluzione
- disegnare i tre vertici,
- disegnare il triangolo,
- creare i tre angoli del triangolo, non visibili,
- disegnare le tre bisettrici,
- disegnare il punto di intersezione di due bisettrici: l'incentro,
Per disegnare la circonferenza inscritta, devo avere un altro punto iltre
all'incentro, il punto in cui la circonferenza tocca il lato.
Come faccio a trovarlo?

"""

from pyig import *
ip = InteractivePlane()

# procedura di supporto: poligono((<vertici>))

# def poligono(vertici):
#   """Funzione che disegna i lati di un poligono dati i vertici."""
#   vertici+=(vertici[0],)                # Trucco per chiudere il poligono
#   for i in range(len(vertici)-1):
#     Segment(vertici[i], vertici[i+1])
  
# tre punti
ip.defwidth = 5
a = Point(-5, 1, name="A")
b = Point(-4, -4, name="B")
c = Point(6, 6, name="C")

# triangolo
ip.defwidth = 2
Polygon((a, b, c))

# angoli
ip.defwidth = 6
a1 = Angle(c, b, a)
a2 = Angle(b, a, c)
a3 = Angle(a, c, b)

# bisettrici
ip.defwidth = 1
# primo metodo
b1 = Bisector(a1)
# secondo metodo:
b2 = a2.bisector()
b3 = a3.bisector()

# incentro
incentro = Intersection(b1, b2, width=10)

# altro punto delle circonferenza:
# la proiezione dell'incentro sul lato
# cio� il piede della perpendicolare al lato tracciata dall'incentro
l = Line(a, b, visible=False)
n = Orthogonal(l, incentro, visible=False)
p = Intersection(l, n, visible=False)

# e ora, la circonferenza inscritta:
Circle(incentro, p)

###
# Messaggio finale
###

Text(-2, -5, """Triangolo,
incentro
e circonferenza inscritta.
Modifica colori e spessori dell'incentro e della circonferenza inscritta.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
