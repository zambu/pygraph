#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-------------1170Circocentro.kpy--#
#                                                                       #
#      Circocentro di un triangolo e circonferenza circoscritta         #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
I tre assi di un triangolo si intersecano in *un* punto, questo punto 
� il centro della circonferenza circoscritta al triangolo. Per questo
motivo viene chiamato "circocentro".

Problema
Disegnare:
- un triangolo,
- i tre assi dei lati,
- la circoferenza circoscritta al triangolo

Soluzione
- disegnare i tre vertici,
- disegnare il triangolo,
- creare i tre assi dei lati del triangolo, non visibili,
- disegnare il punto di intersezione di due assi: il circocentro,
- disegnare la circonferenza circoscritta al triangolo

"""

from pyig import *
ip = InteractivePlane()

# Funzione che restituisce l'asse di un segmento
def asse(segmento):
  """Funzione che restituisce l'asse di un segmento."""
  puntomedio=segmento.midpoint()
  return Orthogonal(segmento, puntomedio, width=1)

# tre punti
a = Point(-3, 4, width=6)
b = Point(-2, -4, width=6)
c = Point(4, 5, width=6)

# triangolo
l1 = Segment(a, b)
l2 = Segment(b, c)
l3 = Segment(c, a)

# assi
a1 = asse(l1)
a2 = asse(l2)
a3 = asse(l3)

# circocentro
circocentro = Intersection(a1, a2, width=10, color="#ff66aa")

# circonferenza circoscritta:
Circle(circocentro, a)

###
# Messaggio finale
###

Text(-7, -5, """Triangolo,
circocentro
e circonferenza circoscritta.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
