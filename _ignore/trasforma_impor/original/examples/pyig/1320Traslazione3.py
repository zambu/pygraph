#!/usr/bin/env python
# -*- coding: utf-8 -*-
#---------------------------------pyig-------------1320Traslazione3.py--#
#                                                                       #
#                             Traslazione                               #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Traslazioni

Problema
- Disegna un segmento AB
- Disegna un poligono.
- Disegna il poligono traslato di AB.

Soluzione
- Disegnare il fascio di rette parallele a AB, passanti
  per i vertici del poligono.
- Riporta su queste rette il segmento AB.

"""

from pyig import *
ip = InteractivePlane()

###
# Trasla
###
##def trasla(punto, traslazione):
##  par=Parallel(traslazione, punto, width=1)
##  circ=Circle(punto, traslazione, width=1)
##  return Intersection(par, circ, 1, name=str(punto.name())+"'")
##
def trasla(punto, traslazione):
  par = Parallel(traslazione, punto, width=1)
  return PointOn(par, 1, name=str(punto.name)+"'")

###
# Segmento
###
p = Point(-7, 8, color="#00f050", width=6, name="P")
q = Point(-1, 6, color="#00f050", width=6, name="Q")
vettore = Segment(p, q)

###
# Poligono0
###
pol = (Point(-7, 5, color="#00f050", width=6, name="A"),
       Point(-2, -2, color="#00f050", width=6, name="B"),
       Point(-3, 6, color="#00f050", width=6, name="C"),
       Point(-4, 2, color="#00f050", width=6, name="C"))
Polygon(pol, color="#a87234", width=4)

poltras = [trasla(vertice, vettore) for vertice in pol]
Polygon(poltras, color="#7934b3", width=4)

Text(-5, -6, """Traslazione di un poligono.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
