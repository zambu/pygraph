#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1330Simmetria.py--#
#                                                                       #
#                              Simmetria                                #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Simmetrie

Problema
- Disegna un poligono.
- Disegna una retta
- Disegna il poligono simmetrico rispetto alla retta.

Soluzione
- Traccia le retta perpendicolari alla retta passanti per i vertici
  del poligono
- Usa i punti vincolati.

"""

from pyig import *
ip = InteractivePlane()

###
# Poligono0
###
a0 = Point(-7, 5, color="#00f050", width=6, name="A")
b0 = Point(-2, -2, color="#00f050", width=6, name="B")
c0 = Point(-3, 6, color="#00f050", width=6, name="C")
d0 = Point(-4, 2, color="#00f050", width=6, name="D")
Polygon((a0, b0, c0, d0), color="#a87234")

# Asse di simmetria
l = Line(Point(-1, -7, color="#00f050", width=6), 
         Point(2, 8, color="#00f050", width=6))

# Rette perpendicolari all'asse di simmetria
ra = Orthogonal(l, a0, width=1)
rb = Orthogonal(l, b0, width=1)
rc = Orthogonal(l, c0, width=1)
rd = Orthogonal(l, d0, width=1)

# Vertici del poligono simmetrico
a1 = PointOn(ra, -1, name="A'")
b1 = PointOn(rb, -1, name="B'")
c1 = PointOn(rc, -1, name="C'")
d1 = PointOn(rd, -1, name="D'")

#Poligono simmetrico
Polygon((a1, b1, c1, d1), color="#72a834")

Text(-5, -6, """Simmetria assiale di un poligono.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
