#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig--------------1160Baricentro.kpy--#
#                                                                       #
#                      Baricentro di un triangolo                       #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
Le tre mediane di un triangolo si intersecano in *un* punto, questo punto 
� il centro di massa del triangolo: il "baricentro".

Problema
Disegnare:
- un triangolo,
- le tre mediane,
- il baricentro.

Soluzione
- disegnare i tre vertici,
- disegnare il triangolo,
- creare i punti medi dei tre lati,
- disegnare le tre mediane,
- disegnare il punto di intersezione di due mediane: il baricentro.
"""

from pyig import *
ip = InteractivePlane()

# tre punti
a = Point(-5, 1, width=6)
b = Point(-3, -3, width=6)
c = Point(4, 5, width=6)

# triangolo
l1 = Segment(a, b)
l2 = Segment(b, c)
l3 = Segment(c, a)

# punti medi dei lati
# primo metodo:
p1 = MidPoint(l1)
# altro metodo:
p2 = l2.midpoint()
p3 = l3.midpoint()

# mediane
# primo metodo
m1 = Line(c, p1, width=1)
m2 = Line(a, p2, width=1)
m3 = Line(b, p3, width=1)

# baricentro
baricentro = Intersection(m1, m2, width=10, color="#ff66aa")

###
# Messaggio finale
###

Text(-2, -5, """Triangolo e Baricentro.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
