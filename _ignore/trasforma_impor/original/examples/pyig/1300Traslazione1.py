#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig-------------1300Traslazione1.py--#
#                                                                       #
#                             Traslazione                               #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Traslazioni

Problema
- Disegna un segmento AB
- Disegna un poligono.
- Disegna il poligono traslato di AB.

Soluzione
- Disegnare il fascio di rette parallele a AB, passanti
  per i vertici del poligono.
- Riporta su queste rette il segmento AB.

"""

from pyig import *
ip = InteractivePlane()

###
# Segmento
###
aa = Point(-7, 8, color="#00f050", width=6, name="A")
bb = Point(-5, 9, color="#00f050", width=6, name="B")
traslazione = Segment(aa, bb)

###
# Poligono0
###
a0 = Point(-7, 5, color="#00f050", width=6, name="A")
b0 = Point(-2, -2, color="#00f050", width=6, name="B")
c0 = Point(-3, 6, color="#00f050", width=6, name="C")
d0 = Point(-4, 2, color="#00f050", width=6, name="C")
Polygon((a0, b0, c0, d0), color="#a87234")

# Rette parallele a r0
ra = Parallel(traslazione, a0, width=1)
rb = Parallel(traslazione, b0, width=1)
rc = Parallel(traslazione, c0, width=1)
rd = Parallel(traslazione, d0, width=1)

# Vertici del poligono traslato
ca = Circle(a0, traslazione, width=1)
a1 = Intersection(ra, ca, 1, name="A'")
cb = Circle(b0, traslazione, width=1)
b1 = Intersection(rb, cb, 1, name="B'")
cc = Circle(c0, traslazione, width=1)
c1 = Intersection(rc, cc, 1, name="C'")
cd = Circle(d0, traslazione, width=1)
d1 = Intersection(rd, cd, 1, name="D'")
##c1=ConstrainedPoint(rc, 1, name="C'")
##d1=ConstrainedPoint(rd, 1, name="D'")

#Poligono traslato
Polygon((a1, b1, c1, d1), color="#72a834")

Text(-5, -6, """Traslazione di un poligono.""")

ip.mainloop()
