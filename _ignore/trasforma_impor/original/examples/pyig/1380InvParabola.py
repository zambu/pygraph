#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig--------------1380InvParabola.py--#
#                                                                       #
#                    Parabola come inviluppo di rette                   #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Parabola come inviluppo di rette

Problema
- Disegna una parabola come inviluppo di rette
  dati la direttrice e il fuoco

Soluzione
- Traccia la direttrice.
- traccia il fuoco.
- traccia gli assi dei segmenti che hanno per estremi
  un punto della retta e il fuoco.

"""

from pyig import *
ip = InteractivePlane()

# Numero di rette dell'inviluppo,
# deve essere un numero float.
N = 10.

# Direttrice e Fuoco
p0 = Point(-2, 2, color="#00f050", width=10)
p1 = Point(-1, -2, color="#00f050", width=10)
direttrice = Line(p0, p1, color="#a87234", name="d")
fuoco = Point(2, 2, color="#00f050", width=5, name="F")

# Funzione che restituisce l'asse dati i due estremi di un segmento
def asse(p0, p1):
  l = Segment(p0, p1, width=1)
  m = MidPoints(p0, p1)
  return Orthogonal(l, m, color="#72a834", width=1)

for i in range(int(N)*2):
  asse(fuoco, PointOn(direttrice, i/N))
  asse(fuoco, PointOn(direttrice, -i/N))

aa = asse(fuoco, ConstrainedPoint(direttrice, 0.5, width=7))
aa.color = "red"

Text(-5, -6, """Parabola come inviluppo.""")

ip.mainloop()
