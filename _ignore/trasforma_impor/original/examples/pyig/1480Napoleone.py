# -*- coding: utf_8 -*-
#---------------------------------pyig----------------1480Napoleone.py--#
#                                                                       #
#                         Teorema di Napoleone                          #
#                                                                       #
#--Daniele Zambelli----------------GPL---------------------------2016---#

"""
Disegnare un triangolo con un triangolo equilatero
costruito esternamente ad ogni lato
"""
# lettura delle librerie
import pyig

# funzioni
def triequi(p_0, p_1, **kargs):
    """Funzione che crea e restituisce un triangolo equilatero."""
    c_01 = pyig.Circle(p_0, p_1, visible=False)
    c_10 = pyig.Circle(p_1, p_0, visible=False)
    p_2 = pyig.Intersection(c_01, c_10, -1)
    return pyig.Polygon((p_0, p_1, p_2), **kargs)

def baricentro(vertici, **kargs):
    """Funzione che restituisce il baricentro di un triangolo."""
    p_0, p_1, p_2 = vertici
    m_01 = pyig.MidPoints(p_0, p_1, visible=False)
    m_12 = pyig.MidPoints(p_1, p_2, visible=False)
    r_m2 = pyig.Line(m_01, p_2, visible=False)
    r_m0 = pyig.Line(m_12, p_0, visible=False)
    return pyig.Intersection(r_m2, r_m0, **kargs)
    
# programma principale
ip = pyig.InteractivePlane("Teorema di Napoleone")
p_a = pyig.Point(-3, 2, width=6)
p_b = pyig.Point(-2, -5, width=6)
p_c = pyig.Point(7, -1, width=6)
t_0 = triequi(p_a, p_b, width = 4, color="DebianRed", intcolor="gold")
t_1 = triequi(p_b, p_c, width = 4, color="DebianRed", intcolor="gold")
t_2 = triequi(p_c, p_a, width = 4, color="DebianRed", intcolor="gold")
b_0 = baricentro(t_0.vertices, color="orchid4", name="A")
b_1 = baricentro(t_1.vertices, color="orchid4", name="B")
b_2 = baricentro(t_2.vertices, color="orchid4", name="C")
pyig.Polygon((b_0, b_1, b_2), color="orchid4", intcolor="orchid1")

pyig.VarText(-9, -12, """Teorema di Napoleone,
lato 1 = {0}
lato 2 = {1}
lato 3 = {2}
""",
(pyig.Segment(b_0, b_1).length(),
 pyig.Segment(b_1, b_2).length(),
 pyig.Segment(b_2, b_0).length()))

## attivazione della finestra grafica
ip.mainloop()
