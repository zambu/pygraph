#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig--------------------bersaglio.py--#
#                                                                       #
#                  Un bersaglio colorato casualmente                    #
#                                                                       #
#--Bruno Stecca---------------GPL----------------------------2006---#

"""
Disegnna un bersaglio con fasce di colori scelti a caso dal sistema.
"""

import random
from pyig import *
ip = InteractivePlane()
    
def coloreacaso():
  return "#{0:02x}{1:02x}{2:02x}".format(random.randrange(256), 
                                         random.randrange(256),
                                         random.randrange(256))

def bersaglio():
    Point(ip, 0,0,width=120, color=coloreacaso())
    Point(ip, 0,0,width=100, color=coloreacaso())
    Point(ip, 0,0,width=80, color=coloreacaso())
    Point(ip, 0,0,width=60, color=coloreacaso())
    Point(ip, 0,0,width=40, color=coloreacaso())
    Point(ip, 0,0,width=20, color=coloreacaso())

bersaglio()
ip.mainloop()
    
    
