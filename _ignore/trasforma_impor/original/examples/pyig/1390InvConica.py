#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig----------------1390InvConica.py--#
#                                                                       #
#                    Conica come inviluppo di rette                     #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2006---#

"""
Ellisse e iperbole come inviluppo di rette

Problema
- Disegna una conica come inviluppo di rette
  dati una circonferenza e un fuoco

Soluzione
- Traccia la circonferenza.
- traccia il fuoco.
- traccia gli assi dei segmenti che hanno per estremi
  un punto della circonferenza e il fuoco.

"""

from pyig import *
ip = InteractivePlane()

# Numero di rette dell'inviluppo,
N = 20

# Direttrice e Fuoco
centro = Point(-2, 2, color="#00f050", width=10)
p = Point(-1, -4, color="#00f050", width=10)
circonf = Circle(centro, p, color="#a87234", name="C")
fuoco = Point(2, 2, color="#00f050", width=5, name="F")

# Funzione che restituisce l'asse dati i due stremi di un segmento
def asse(p0, p1):
  l = Segment(p0, p1, width=1)
  m = MidPoints(p0, p1)
  return Orthogonal(l, m, color="#72a834", width=1)

for i in range(N*2):
  asse(fuoco, PointOn(circonf, float(i)/N))

aa = asse(fuoco, ConstrainedPoint(circonf, 0.5, width=7))
aa.color = "red"

Text(-5, -6, """Conica come inviluppo.""")

ip.mainloop()
