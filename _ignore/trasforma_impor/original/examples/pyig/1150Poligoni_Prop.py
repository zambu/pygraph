#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#---------------------------------pyig------------1150Poligoni_Prop.py--#
#                                                                       #
#                         ProprietÓ dei Poligoni                        #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2005---#

"""
I poligoni hanno le seguenti proprietÓ:
- Type: il tipo dell'oggetto,
- NumOfSides: il numero di lati,
- Perimeter: la lunghezza del perimetro,
- Surface: l'area della superficie,

Problema
Disegnare:
- un poligono,
- dei testi variabili con le informazioni sulle proprietÓ dell'oggetto,

Soluzione
Analoga a quella dei problemi precedenti.
"""

from pyig import *
ip = InteractivePlane()

# Primo metodo

p = Polygon((Point(-7, 2), Point(-6, -1,), 
           Point(-5, 1), Point(-4, -3)))

Text(-7, 8, "Tipo: {0}".format(p.type()))

VarText(-7, 7, "Perimetro: {0} suddiviso in {1} lati.",
        (p.perimeter(), p.numofsides()))

VarText(-7, 6, "Area: {0}", p.surface())

# Secondo metodo

p = Polygon((Point(5, 2), Point(6, 0,), 
           Point(7, 1), Point(5, -3)))

Text(7, 9, "Tipo: {0}".format(p.type()))

VarText(7, 8, "Perimetro: {0} suddiviso in {1} lati.",
        (p.perimeter(), p.numofsides()))

VarText(7, 7, "Area: {0}", p.surface())

###
# Messaggio finale
###

Text(-2, -5, """Sopra al poligono vengono scritte delle etichette
che contengono le proprietÓ del poligono.""")

###
# Attivazione della finestra interattiva
###

ip.mainloop()
