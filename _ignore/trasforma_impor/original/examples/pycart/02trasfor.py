#!/usr/bin/env python
#-------------------------------python----------------------trasfor.py--#
#                                                                       #
#                Trasformazioni nel piano cartesiano                    #
#                                                                       #
#--Daniele Zambelli----------------------------------------------2003---#

from pycart import Plane, Pen

##def trasla(vertici, t):
##  def traslapunto(v): return (v[0]+t[0], v[1]+t[1])
##  return map(traslapunto, vertici)
##
##def simmo(vertici):
##  def simmopunto(v): return (-v[0], -v[1])
##  return map(simmopunto, vertici)
##
##def simmx(vertici):
##  def simmxpunto(v): return (v[0], -v[1])
##  return map(simmxpunto, vertici)
##
##def simmy(vertici):
##  def simmypunto(v): return (-v[0], v[1])
##  return map(simmypunto, vertici)
##
##def omotetia(vertici, k):
##  def omotetiapunto(v): return (v[0]*k, v[1]*k)
##  return map(omotetiapunto, vertici)
##
def trasla(vertici, t):
  return map(lambda v: (v[0]+t[0], v[1]+t[1]), vertici)

def simmo(vertici):
  return map(lambda v: (-v[0], -v[1]), vertici)

def simmx(vertici):
  return map(lambda v: (v[0], -v[1]), vertici)

def simmy(vertici):
  return map(lambda v: (-v[0], v[1]), vertici)

def omotetia(vertici, k):
  return map(lambda v: (v[0]*k, v[1]*k), vertici)

pp = Plane("Trasformazioni nel piano cartesiano", sx=1, axes=True)
p = Pen(width=2)
p1 = [(20, 20), (80, 20), (80, 60), (40, 60)]
p2 = trasla(p1, (-30, 50))
p3 = simmo(p1)
p4 = simmx(p1)
p5 = simmy(p2)
p6 = omotetia(p1, 0.5)
p7 = omotetia(p1, -3.2)
p.drawpoly(p1)
p.color = 'red'
p.drawpoly(p2)
p.color = 'green'
p.drawpoly(p3)
p.color = 'pink'
p.drawpoly(p4)
p.color = 'blue'
p.drawpoly(p5)
p.color = 'brown'
p.drawpoly(p6)
p.drawpoly(p7)
pp.mainloop()
