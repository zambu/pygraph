#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
#-------------------------------python----------------reactiveplane.py--#
#                                                                       #
#                           Piano reattivo                              #
#                                                                       #
# Copyright (c) 2013 Daniele Zambelli
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
Reactive Plane
"""

from __future__ import division, print_function
import pycart as pc

SEMIBASE = 20
BASE = SEMIBASE * 2
#SEMIDIM = SEMIBASE * 3
DIM = BASE * 3
CONVERT = {(0, 0): 0, (1, 0): 1, (2, 0): 2,
           (0, 1): 3, (1, 1): 4, (2, 1): 5,
           (0, 2): 6, (1, 2): 7, (2, 2): 8,}

def onkeypress(event):
  print("Key pressed:", event.char)
def onkeyrelease(event):
  print("Key released:", event.char)
def onenter(event):
  print("Enter at", event)
def onleave(event):
  print("Leave at", event)
def onclick(event):
  print("Left clicked at", event.x, event.y)
def onpress1(event):
  print("Left clicked at", event.x, event.y)
def onpress2(event):
  print("Center clicked at", event.x, event.y)
def onpress3(event):
  print("Right clicked at", event.x, event.y)
def onmotion1(event):
  print("Left motion at", event.x, event.y)
def onmotion2(event):
  print("Center motion at", event.x, event.y)
def onmotion3(event):
  print("Right motion at", event.x, event.y)
def onrelease1(event):
  print("Left released at", event.x, event.y)
def onrelease2(event):
  print("Center released at", event.x, event.y)
def onrelease3(event):
  print("Right released at", event.x, event.y)
c_p = pc.Plane('04. events', color='pink')
c_p.onkeypress(onkeypress)
c_p.onkeyrelease(onkeyrelease)
c_p.onenter(onenter)
c_p.onleave(onleave)
c_p.onpress1(onpress1)
c_p.onpress2(onpress2)
c_p.onpress3(onpress3)
c_p.onmotion1(onmotion1)
c_p.onmotion2(onmotion2)
c_p.onmotion3(onmotion3)
c_p.onrelease1(onrelease1)
c_p.onrelease2(onrelease2)
c_p.onrelease3(onrelease3)
biro = pc.Pen()
biro.drawtext('Clicca in vari punti del piano', (0, 9), 'navy', 2)
biro.drawtext('con i tre tasti del mouse', (0, 7), 'navy', 2)

c_p.mainloop()
