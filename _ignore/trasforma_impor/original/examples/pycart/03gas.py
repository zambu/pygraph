#!/usr/bin/env python
#-------------------------------python--------------------------gas.py--#
#                                                                       #
#                                 Gas                                   #
#                                                                       #
#--Daniele Zambelli---------------GPL----------------------------2002---#

from math import pi, sin, cos
from random import randrange, random
from pycart import Plane

DIM_MOL=2
class Bombola:
  def __init__(self, w=50, h=100, px=0, py=0, nmol=50, dim_mol=2):
    self.dim_mol = dim_mol
    self.bxmi, self.bymi = -w/2+px, -h/2+py
    self.bxma, self.byma = w/2+px, +h/2+py
    penna.drawpoly(((self.bxmi, self.bymi), (self.bxma, self.bymi),
                    (self.bxma, self.byma), (self.bxmi, self.byma)))
    angolo = random()*2*pi
    self.gas = [Molecola(bombola=self,
                         x=randrange(self.bxmi+dim_mol*2, self.bxma-dim_mol*2),
                         y=randrange(self.bymi+dim_mol*2, self.byma-dim_mol*2),
                         velocita= random()*2+1,
                         colore='red')
                for cont in range(nmol)]

  def impattos(self, x):
    return x <= self.bxmi+self.dim_mol*2

  def impattod(self, x):
    return x >= self.bxma-self.dim_mol*2

  def impattoa(self, y):
    return y >= self.byma-self.dim_mol*2

  def impattob(self, y):
    return y <= self.bymi+self.dim_mol*2

  def anima(self):
    for i in range(1000):
      for m in self.gas:
        m.muovi()
      canvas.update()
#      canvas.after(20)

class Molecola:
  def __init__(self, bombola, x, y, velocita, colore):
    self.bombola = bombola
    self.x, self.y = x, y
    angolo = random()*2*pi
    self.vx = velocita*cos(angolo)
    self.vy = velocita*sin(angolo)
    self.o = penna.drawpoint((self.x, self.y), color=colore)

  def muovi(self):
    if (self.bombola.impattos(self.x) or 
        self.bombola.impattod(self.x)):
      self.vx = -self.vx+random()*.25-.125
      self.vy = self.vy+random()*.25-.125
    if (self.bombola.impattoa(self.y) or 
        self.bombola.impattob(self.y)):
      self.vx = self.vx+random()*.25-.125
      self.vy = -self.vy+random()*.25-.125
    self.x += self.vx
    self.y += self.vy
    canvas.move(self.o, self.vx, -self.vy)
    
piano = Plane("Gas", w=400, h=400, sx=1, axes=False)
penna = piano.newPen(width=DIM_MOL)
canvas = piano.getcanvas()
b = Bombola(w=100, h=200, nmol=50, dim_mol=DIM_MOL)
b.anima()
piano.mainloop()


