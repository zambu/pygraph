#!/usr/bin/env python
#-------------------------------python---------------------orologio.py--#
#                                                                       #
#                              Orologio                                 #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2003--#

from pyturtle import TurtlePlane, Turtle 
from time import time, localtime

class Orologio:

  def __init__(self, d=100):
    self.d = d*0.9
    self.luore = self.d*0.6
    self.laore = self.d*0.1
    self.aore = 0
    self.lo = None
    self.luminuti = self.d*0.8
    self.laminuti = self.d*0.05
    self.aminuti = 0
    self.lm = None
    self.lusecondi = self.d*0.9
    self.lasecondi = self.d*0.02
    self.asecondi = 0
    self.ls = None
    self.p = TurtlePlane("PyOrologio", d*2, d*2)
    self.t = Turtle()
    self.cassa()
    try:
      while 1:
        self.p.after(1000, self.lancette(localtime(time())[3:6]))
    except:
      pass

  def cassa(self):
    self.t.up()
    self.t.forward(self.d)
    self.t.down()
    self.t.left(90)
    self.t.circle(self.d)
    self.t.right(90)
    self.t.up()
    self.t.back(self.d)
    self.t.down()
    d1 = self.d*0.9; d2=self.d-d1
    for i in range(12):
      self.t.up()
      self.t.forward(d1)
      self.t.down()
      self.t.forward(d2)
      self.t.up()
      self.t.back(self.d)
      self.t.down()
      self.t.left(30)

  def lancette(self, t):
    ore, minuti, secondi = t
    aore = round(90 - ore*30 - minuti*0.5)
    aminuti = round(90 - minuti*6 - secondi*0.1)
    asecondi = round(90 - secondi*6)
    if self.aore != aore:
      if self.lo: self.p.delete(self.lo)
      self.t.direction = aore
      self.t.width = self.laore
      self.lo = self.t.forward(self.luore)
      self.t.up()
      self.t.back(self.luore)
      self.t.down()
      self.aore=aore
    if self.aminuti != aminuti:
      if self.lm: self.p.delete(self.lm)
      self.t.direction = aminuti
      self.t.width = self.laminuti
      self.lm = self.t.forward(self.luminuti)
      self.t.up()
      self.t.back(self.luminuti)
      self.t.down()
      self.aminuti = aminuti
    if self.asecondi != asecondi:
      if self.ls: self.p.delete(self.ls)
      self.t.direction = asecondi
      self.t.width = self.lasecondi
      self.ls=self.t.forward(self.lusecondi)
      self.t.up()
      self.t.back(self.lusecondi)
      self.t.down()
      self.asecondi = asecondi
    
o=Orologio(150)
