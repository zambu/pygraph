#!/usr/bin/env python
#--------------------------python-pyturtle---------------ricorsione.py--#
#                                                                       #
#                         Figure ricorsive                              #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2003--#

from pyturtle import TurtlePlane, Turtle
from random import randrange

def albero(lung):
  if lung < 2: return
  t.forward(lung)
  t.left(45)
  albero(lung/2)
  t.right(90)
  albero(lung/2)
  t.left(45)
  t.back(lung)

##def alberobin(lung, angolo):
##  if lung<2: return
##  t.forward(lung)
##  t.left(angolo)
##  alberobin(lung/2, angolo)
##  t.right(2*angolo)
##  alberobin(lung/2, angolo)
##  t.left(angolo)
##  t.back(lung)

##def alberobin(lung, angolo):
##  if lung<2: return
##  t.width(lung/5)
##  t.forward(lung)
##  t.left(angolo)
##  alberobin(lung/2, angolo)
##  t.right(2*angolo)
##  alberobin(lung/2, angolo)
##  t.left(angolo)
##  t.back(lung)

def alberobin(lung, angolo, larg, decsx, decdx):
  if lung < 2: return
  t.width = lung*larg
  t.forward(lung)
  t.left(angolo)
  alberobin(lung*decsx, angolo, larg, decsx, decdx)
  t.right(2*angolo)
  alberobin(lung*decdx, angolo, larg, decsx, decdx)
  t.left(angolo)
  t.back(lung)

def alberobincas(lung, angolo, larg, decsx, decdx, caos):
  if lung < 2: return
  var = int(lung*caos)+1
  l = lung-var+randrange(2*var)
  t.width = lung*larg
  t.forward(lung)
  t.left(angolo)
  alberobincas(l*decsx, angolo, larg, decsx, decdx, caos)
  t.right(2*angolo)
  alberobincas(l*decdx, angolo, larg, decsx, decdx, caos)
  t.left(angolo)
  t.back(lung)

def alberoter(lung, angolo):
  if lung < 2: return
  t.width = lung/5
  t.forward(lung)
  t.left(angolo)
  alberoter(lung/2, angolo)
  t.right(angolo)
  alberoter(lung/2, angolo)
  t.right(angolo)
  alberoter(lung/2, angolo)
  t.left(angolo)
  t.back(lung)

def koch(lung, liv):
  if liv == 0: t.forward(lung); return
  koch(lung/3.0, liv-1)
  t.left(60)
  koch(lung/3.0, liv-1)
  t.right(120)
  koch(lung/3.0, liv-1)
  t.left(60)
  koch(lung/3.0, liv-1)
  
def fiocco(lato, liv):
  for i in range(3):
    koch(lato, liv)
    t.right(120)
    
def fioccol(lato, liv):
  for i in range(3):
    koch(lato, liv)
    t.left(120)

def kochcas(lung, liv):
  if liv == 0: t.forward(lung); return
  verso=randrange(-1, 2, 2)
  kochcas(lung/3.0, liv-1)
  t.left(60*verso)
  koch(lung/3.0, liv-1)
  t.right(120*verso)
  kochcas(lung/3.0, liv-1)
  t.left(60*verso)
  koch(lung/3.0, liv-1)
  
def fioccocas(lato, liv):
  for i in range(3):
    kochcas(lato, liv)
    t.right(120)
    
tp = TurtlePlane()
t = Turtle(y=-100, d=90)
#albero(100)
#alberobin(100, 60, 0.1, 0.8, 0.6)
alberobincas(100, 60, 0.1, 0.7, 0.5, 0.3)
tp = TurtlePlane()
t = Turtle(d=90)
alberoter(100, 60)
tp = TurtlePlane()
t = Turtle()
koch(200, 4)
tp = TurtlePlane()
t = Turtle()
fioccocas(200, 4)
tp.mainloop()
