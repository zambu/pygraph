#!/usr/bin/env python
#--------------------------python-pyturtle--------------------fiore.py--#
#                                                                       #
#                               Fiore                                   #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2002--#

from pyturtle import TurtlePlane, Turtle

def petalo(r):
  t.right(45)
  t.circle(r, 90)
  t.left(90)
  t.circle(r, 90)
  t.left(135)

def corolla(r):
  t.color = 'gold'
  for i in range(12):
    petalo(r)
    t.left(30)

def dalia(l):
  t.color = 'green'
  t.forward(l)
  t.left(45)
  petalo(l/2)
  t.right(45)
  t.forward(l/2)
  t.right(45)
  petalo(l/2)
  t.left(45)
  t.forward(l)
  corolla(l/2)
  t.up()
  t.back(l*2.5)

tp = TurtlePlane()
t = Turtle(y=-190, d=90, width=2)
dalia(100)
tp.mainloop()
