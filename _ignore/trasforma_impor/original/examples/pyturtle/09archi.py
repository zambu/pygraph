#!/usr/bin/env python
#-------------------------------python-----------------------archi.py--#
#                                                                      #
#                      Demo di circle e ccircle                        #
#                                                                      #
#--Daniele Zambelli----------------------------------------------2003--#

from random import random, randrange
from pyturtle import TurtlePlane, Turtle

def spostati(tarta, x, y):
  '''Trasla Tartaruga delle componenti x e y'''
  tarta.up()
  tarta.forward(x)
  tarta.left(90)
  tarta.forward(y)
  tarta.right(90)
  tarta.down()

def saltaacaso(tarta):
  '''Sposta casualmente Tartaruga all'interno del rettangolo
  di vertici (0, 0) e (600, 400)'''
  tarta.up()
  tarta.pos = (randrange(600)-300, randrange(400)-200)
  tarta.down()

def coloraacaso(tarta):
  '''Assegna a Tartaruga un colore con componenti RGB casuali'''
  tarta.color = (random(), random(), random())

def macchie(tarta, n):
  for j in range(n):
    saltaacaso(tarta)
    coloraacaso(tarta)
    tarta.fill(1)
    tarta.circle(randrange(100), randrange(360))
    tarta.fill(0)

def cmacchie(tarta, n):
  for j in range(n):
    saltaacaso(tarta)
    coloraacaso(tarta)
    tarta.fill(1)
    tarta.ccircle(randrange(100), randrange(360))
    tarta.fill(0)

def tricircle(tarta):
  '''Produce uno strano disegno'''
  tarta.tracer(1)
  tarta.width = 10
  tarta.fill(1)
  for i in range(3):
    tarta.forward(100)
    tarta.right(90)
    tarta.color = "green"
    tarta.circle(30)
    tarta.color = "black"
    tarta.left(90)
    tarta.back(80)
    tarta.left(120)
  tarta.color = "maroon"
  tarta.fill(0)

# ccircle
def bersaglio(tarta):
  '''Disegna un bersaglio'''
  saltaacaso(tarta)
#  stella(tarta, 4, 5)
  larghezza=tarta.getwidth()
  tarta.width = 2
  tarta.ccircle(20)
  tarta.width = larghezza
  tarta.ccircle(10)
  return(tarta.getpos())

def segmentocirc(tarta, r):
  tarta.circle(r, 180)
  tarta.left(90)
  tarta.forward(r+r)
  tarta.left(90)

def petalo(tarta, r):
  tarta.right(45)
  tarta.circle(r, 90)
  tarta.left(90)
  tarta.circle(r, 90)
  tarta.left(135)
  
def sequenza1(tarta):
  coloraacaso(tarta)
  segmentocirc(tarta, 50)   # semicerchio vuoto
  spostati(tarta, 100, 0)
  coloraacaso(tarta)
  tarta.fill(1)
  segmentocirc(tarta, 50)   # semicerchio con
  coloraacaso(tarta)      # bordo e interno
  tarta.fill(0)       # colorati
  spostati(tarta, 100, 0)
  coloraacaso(tarta)
  tarta.up()
  tarta.fill(1)
  segmentocirc(tarta, 50)   # semicerchio colorato
  coloraacaso(tarta)      # senza bordo
  tarta.fill(0)
  tarta.down()
  spostati(tarta, 100, 0)
  tarta.left(90)
  coloraacaso(tarta)
  petalo(tarta, 70)         # petalo vuoto
  tarta.right(90)
  spostati(tarta, 100, 0)
  coloraacaso(tarta)
  tarta.left(90)
  tarta.fill(1)
  petalo(tarta, 70)         # petalo con
  coloraacaso(tarta)      # bordo e interno
  tarta.fill(0)       # colorati
  tarta.right(90)
  spostati(tarta, 100, 0)
  coloraacaso(tarta)
  tarta.left(90)
  tarta.up()
  tarta.fill(1)
  petalo(tarta, 70)         # petalo colorato
  coloraacaso(tarta)      # senza bordo
  tarta.fill(0)
  tarta.down()
  tarta.right(90)

def settorecirc(tarta, r):
  tarta.left(45)
  tarta.ccircle(r, 90)
  tarta.right(135)

def radio(tarta, r):
  for i in range(3):
    tarta.ccircle(r, 60)
    tarta.left(60)

def sequenza2(tarta):
  coloraacaso(tarta)
  settorecirc(tarta, 50)    # settorecirc vuoto
  spostati(tarta, 100, 0)
  coloraacaso(tarta)
  tarta.fill(1)
  settorecirc(tarta, 50)    # settorecirc con
  coloraacaso(tarta)      # bordo e interno
  tarta.fill(0)       # colorati
  spostati(tarta, 100, 0)
  coloraacaso(tarta)
  tarta.up()
  tarta.fill(1)
  settorecirc(tarta, 50)    # settorecirc colorato
  coloraacaso(tarta)      # senza bordo
  tarta.fill(0)
  tarta.down()
  spostati(tarta, 100, 0)
  tarta.left(90)
  coloraacaso(tarta)
  radio(tarta, 50)          # radio vuoto
  tarta.right(90)
  spostati(tarta, 100, 0)
  coloraacaso(tarta)
  tarta.left(90)
  tarta.fill(1)
  radio(tarta, 50)          # radio con
  coloraacaso(tarta)      # bordo e interno
  tarta.fill(0)       # colorati
  tarta.right(90)
  spostati(tarta, 100, 0)
  coloraacaso(tarta)
  tarta.left(90)
  tarta.up()
  tarta.fill(1)
  radio(tarta, 50)          # radio colorato
  coloraacaso(tarta)      # senza bordo
  tarta.fill(0)
  tarta.down()
  tarta.right(90)


def main():
  tp = TurtlePlane()
  tina = Turtle()
  macchie(tina, 50)
  tp = TurtlePlane()
  tina = Turtle()
  cmacchie(tina, 50)
  tp = TurtlePlane()
  tina = Turtle()
  tricircle(tina)
  tp = TurtlePlane()
  tina = Turtle(x=-250, y=98, width=3)
  sequenza1(tina)
  tp = TurtlePlane()
  tina = Turtle(x=-250, y=-98)
  sequenza2(tina)
  tp.mainloop()

if __name__ == "__main__":
    main()
