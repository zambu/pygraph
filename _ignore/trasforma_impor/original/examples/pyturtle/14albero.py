#!/usr/bin/env python
#--------------------------python-pyturtle-------------------albero.py--#
#                                                                       #
#                               Albero                                  #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2003--#

from pyturtle import TurtlePlane, Turtle
from random import randrange

def albero(lung):
  if lung < 2: return
  t.forward(lung)
  t.left(45)
  albero(lung/2)
  t.right(90)
  albero(lung/2)
  t.left(45)
  t.back(lung)

##def alberobin(lung, angolo):
##  if lung<2: return
##  t.forward(lung)
##  t.left(angolo)
##  alberobin(lung/2, angolo)
##  t.right(2*angolo)
##  alberobin(lung/2, angolo)
##  t.left(angolo)
##  t.back(lung)

##def alberobin(lung, angolo):
##  if lung<2: return
##  t.width(lung/5)
##  t.forward(lung)
##  t.left(angolo)
##  alberobin(lung/2, angolo)
##  t.right(2*angolo)
##  alberobin(lung/2, angolo)
##  t.left(angolo)
##  t.back(lung)

##def alberobin(lung, angolo, larg, decsx, decdx):
##  if lung<2: return
##  t.width = lung*larg
##  t.forward(lung)
##  t.left(angolo)
##  alberobin(lung*decsx, angolo, larg, decsx, decdx)
##  t.right(2*angolo)
##  alberobin(lung*decdx, angolo, larg, decsx, decdx)
##  t.left(angolo)
##  t.back(lung)

def alberobincas(lung, angolo, larg, decsx, decdx, caos):
  if lung < 2: return
  var = int(lung*caos)+1
  l = lung-var+randrange(2*var)
  t.width = lung*larg
  t.forward(lung)
  t.left(angolo)
  alberobincas(l*decsx, angolo, larg, decsx, decdx, caos)
  t.right(2*angolo)
  alberobincas(l*decdx, angolo, larg, decsx, decdx, caos)
  t.left(angolo)
  t.back(lung)

def alberoter(lung, angolo):
  if lung < 2: return
  t.width = lung/5
  t.forward(lung)
  t.left(angolo)
  alberoter(lung/2, angolo)
  t.right(angolo)
  alberoter(lung/2, angolo)
  t.right(angolo)
  alberoter(lung/2, angolo)
  t.left(angolo)
  t.back(lung)

tp = TurtlePlane()
t = Turtle(d=90, y=-100)
t.tracer(0)
#albero(100)
#alberobin(100, 60, 0.1, 0.8, 0.6)
alberobincas(90, 60, 0.1, 0.7, 0.6, 0.3)
#alberoter(100, 60)
tp.mainloop()
