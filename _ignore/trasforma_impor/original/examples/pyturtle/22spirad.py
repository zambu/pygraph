#!/usr/bin/env python
#--------------------------python-pyturtle------------------2spirad.py--#
#                                                                       #
#             La spirale delle radici dei numeri interi                 #
#                                                                       #
#--Daniele Zambelli-------------GPL3-------------------------------2012-#

from __future__ import print_function
from pyturtle import TurtlePlane, Turtle

def trirett(n):
    tina.lookat((0, 0))
    tina.left(90)
    tina.forward(1)
    tina.drawsegment(tina.position, (0, 0))
    print('radq({0}) = {1}'.format(n, tina.distance((0, 0))))
          

tp = TurtlePlane(sx=50)
tina = Turtle()
tina.back(1)
for cont in range(2, 20):
    trirett(cont)

tp.mainloop()
