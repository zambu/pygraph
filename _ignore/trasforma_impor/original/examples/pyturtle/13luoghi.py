#!/usr/bin/env python
#-------------------------------python-----------------------luoghi.py--#
#                                                                       #
#                    Luoghi geometrici: coniche                         #
#                                                                       #
#--Bruno Stecca - Daniele Zambelli--------------------------------2003--#

from pyturtle import TurtlePlane, Turtle
from math import pi

class Geometra(Turtle):

  def conica1(self, raggio, p):
    numpassi = 72
    lato = raggio*2*pi/numpassi
    ang = 180./numpassi
    self.up()
    self.forward(raggio)
    self.left(90)
    self.down()
    for i in range(numpassi):
      self.asse(p)
      self.left(ang); self.forward(lato); self.left(ang)

  def conica2(self, p):
    numpassi = 80
    lato = 5
    self.up()
    self.back(200)
    self.down()
    for i in range(numpassi):
      self.asse(p)
      self.forward(lato)

  def asse(self, punto):
    direction = self.direction
    position = self.position
    self.up()
    self.lookat(punto)
    self.forward(self.distance(punto)/2.)
    self.left(90)
    self.back(1000)
    self.down()
    self.forward(2000)
    self.up()
    self.position = position
    self.direction = direction
    self.down()

tp1 = TurtlePlane('Ellisse')  
g1 = Geometra(color='red')
g1.conica1(200, (150, 0))
tp2 = TurtlePlane('Parabola')  
g2 = Geometra(color='blue')
g2.conica2((0, 50))
tp3 = TurtlePlane('Iperbole')  
g3 = Geometra(color='green')
g3.conica1(100, (150, 0))
tp1.mainloop()
