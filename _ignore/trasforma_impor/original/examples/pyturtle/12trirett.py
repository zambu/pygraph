#!/usr/bin/env python
#-------------------------------python----------------------trirett.py--#
#                                                                       #
#                              Trirett                                  #
#                                                                       #
#--Daniele Zambelli-----------------------------------------------2002--#

from pyturtle import TurtlePlane, Turtle  

def trirett(c1, c2):
  t.forward(c1)
  a = t.position
  t.back(c1)
  t.left(90)
  t.forward(c2)
  t.position = a
  t.right(90)
  t.back(c1)

def inviluppo(l1, l2, inc=10):
  if l1 < 0: return
  trirett(l1, l2)
  inviluppo(l1-inc, l2+inc)

def quasar(l):
  for i in range(4):
    inviluppo(l, 0)
    t.left(90)

def occhio(l):
  for i in range(2):
    inviluppo(l, 0)
    t.forward(l)
    t.left(90)
    t.forward(l)
    t.left(90)

def quattrocchi(l):
  for i in range(4):
    occhio(l)
    t.left(90)

tp = TurtlePlane('Inviluppi di triangoli rettangoli', 600, 600)
t = Turtle()
quattrocchi(290)
tp.mainloop()
