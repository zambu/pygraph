#!/usr/bin/env python
#----------------------python-pyturtle-----------------man1.py--#
#                                                               #
#                       Muro iterativo                          #
#                                                               #
#--Daniele Zambelli------License: GPL---------------------2005--#

from pyturtle import TurtlePlane, Turtle
tp = TurtlePlane()
tina = Turtle()

def sposta(avanti=0, sinistra=0):
  """Effettua uno spostamento orizzontale e verticale di Tartaruga
     senza disegnare la traccia."""
  tina.up()
  tina.forward(avanti)
  tina.left(90)
  tina.forward(sinistra)
  tina.right(90)
  tina.down()

def quadrato(lato):
  """Disegna un quadrato di dato lato."""
  for i in range(4):
    tina.forward(lato)
    tina.left(90)

##def fila(lato, colonne, spazio_colonne=5):
##  """Disegna una fila di mattoni quadrati."""
##  for j in range(colonne):
##    quadrato(lato)
##    sposta(o=lato+spazio_colonne)
##  sposta(o=-colonne*(lato+spazio_colonne))
##
##def muro(lato, righe, colonne, spazio_righe=5, spazio_colonne=5):
##  """Disegna un muro di mattoni."""
##  for i in range(righe):
##    fila(lato, colonne, spazio_colonne)
##    sposta(v=lato+spazio_righe)
##  sposta(v=-righe*(lato+spazio_righe))

def muro(lato, righe, colonne, spazio_righe=5, spazio_colonne=5):
  for i in range(righe):
    for j in range(colonne):
      quadrato(lato)
      sposta(avanti=lato+spazio_colonne)
    sposta(avanti=-colonne*(lato+spazio_colonne), sinistra=lato+spazio_righe)
  sposta(sinistra=-righe*(lato+spazio_righe))

sposta(-250, -190)
muro(20, 15, 20)

tp.mainloop()
