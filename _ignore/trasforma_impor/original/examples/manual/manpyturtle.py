# -*- coding: iso-8859-15 -*-
#-------------------------------python------------------manpyturtle.py--#
#                                                                       #
#      Esempi del manuale relativi alla grafica della tartaruga         #
#                                                                       #
#--Daniele Zambelli-----------Licence GPL------------------------2010---#

"""
pyturtle examples for manual.
"""

from __future__ import print_function
import libmanex
from pyturtle import TurtlePlane, Turtle, version

def ex_00():
  """version():
  Controlla la versione della libreria."""
  tp = TurtlePlane()
  print(version())
  libmanex.end(tp)
  
#####################################
#        ``class TurtlePlane``      #
#        ---------------------      #
#####################################

def ex_01():
  """__init__():
  Crea 4 piani, con caratteristiche diverse
  e disegnare in ciascuno di essi un quadrato."""
  tp0 = TurtlePlane()
  tp1 = TurtlePlane("TurtlePlane con gli assi", sx=10, axes=True)
  tp2 = TurtlePlane("TurtlePlane con gli griglia", sx=20, grid=True)
  tp3 = TurtlePlane("TurtlePlane con assi e griglia", sx=30, sy=20, 
                    axes=True, grid=True)
  piani = [tp0, tp1, tp2, tp3]
  for p in piani:
      t = p.newTurtle()
      for cont in range(4):
          t.forward(7)
          t.left(90)
  libmanex.end(tp0)

def ex_02():
  """newTurtle():
  Crea un piano con tre tartarughe e le fa avanzare di 100 unit�."""
  tp = TurtlePlane()
  tina = tp.newTurtle(x=-100, y=-100, d=90, color='red')
  gina = tp.newTurtle(y=-100, d=90, color='green')
  pina = tp.newTurtle(x=100, y=-100, d=90, color='blue')
  tina.forward(100)
  pina.forward(100)
  gina.forward(100)
  libmanex.end(tp)

def ex_03():
  """turtles():
  Crea un piano con tre tartarughe anonime, le fa avanzare di 10 unit�."""
  tp = TurtlePlane("Piano con tre tartarughe",
                   sx=20, sy=20, grid=True, gridcolor="grey")
  tp.newTurtle(x=-10, y=-8, d=90, color='red')
  tp.newTurtle(y=-8, d=90, color='green')
  tp.newTurtle(x=10, y=-8, d=90, color='blue')
  for t in tp.turtles():
      t.forward(10)
  libmanex.end(tp)

def ex_04():
  """delturtles():
  Crea 2 tartarughe e fa muovere tutte le tartarughe, 
  dopo un secondo le elimina,
  crea altre 2 tartarughe e le fa muovere tutte le tartarughe."""
  tp = TurtlePlane(name='2+2 Tartarughe', w=400, h=400,
                   sx=20, sy=20, grid=True)
  tp.newTurtle(x=-3, y=-3, d=225, color='orange red', width=10)
  tp.newTurtle(x=+3, y=-3, d=315, color='orange', width=10)
  tp.after(500)
  for t in tp.turtles():
      t.forward(5)
  tp.after(500)
  tp.delturtles()
  tp.newTurtle(x=+3, y=+3, d= 45, color='pale green', width=10)
  tp.newTurtle(x=-3, y=+3, d=135, color='pale turquoise', width=10)
  tp.after(500)
  for t in tp.turtles():
      t.forward(5)
  libmanex.end(tp)

def ex_05():
  """clean():
  Disegna un pentagono, si sposta, disegna un altro pentagono,
  applica clean e ridisegna un pentagono."""
  from random import random, randrange
  
  def pentagono(lato):
      for cont in range(5):
          tina.forward(lato)
          tina.left(72)

  tp=TurtlePlane(name="Clean", sx=10, grid=True)
  tina=tp.newTurtle(color='pink', width=3)
  tina.tracer(5)
  pentagono(5)
  tina.color = (random(), random(), random())
  tina.width = randrange(2, 20, 2)
  tina.right(randrange(360))
  tina.forward(randrange(5)+5)
  pentagono(5)
  tp.after(700)
  tp.clean()
  pentagono(5)
  libmanex.end(tp)

def ex_06():
  """reset():
  Disegna un pentagono, si sposta, disegna un altro pentagono,
  applica reset e ridisegna un pentagono."""
  from random import random, randrange
  
  def pentagono(lato):
      for cont in range(5):
          tina.forward(lato)
          tina.left(72)

  tp=TurtlePlane(name="reset", sx=10, axes=True)
  tina=tp.newTurtle(color='pink', width=3)
  tina.tracer(5)
  pentagono(5)
  tina.color = (random(), random(), random())
  tina.width = randrange(2, 20, 2)
  tina.right(randrange(360))
  tina.forward(randrange(5)+5)
  pentagono(5)
  tp.after(700)
  tp.reset()
  pentagono(5)
  libmanex.end(tp)

#####################################
#        ``class Turtle``           #
#        ----------------           #
#####################################

def ex_10():
  """__init__():
  Crea un piano con tre tartarughe e le fa avanzare di 15 unit�."""
  tp = TurtlePlane(name='Tre tartarughe', sx=20, sy=20,
                   axes=True, grid=True)
  tina = Turtle(x=-10, y=-8, d=90, width=5, color='red')
  gina = Turtle(y=-8, d=90, width=5, color='green')
  pina = Turtle(x=10, y=-8, d=90, width=5, color='blue')
  tina.forward(15)
  pina.forward(15)
  gina.forward(15)
  libmanex.end(tp)

def ex_11():
  """forward():
  Disegna un quadrato con il lato lungo 47 unit�."""
  def quadrato(lato):
      """Disegna un quadrato con un certo lato"""
      for count in range(4):
          tina.forward(lato)
          tina.left(90)

  tp = TurtlePlane()
  tina = Turtle()
  quadrato(47)
  libmanex.end(tp)

def ex_12():
  """back():
  Disegna una stella con colori fumati."""
  tp = TurtlePlane()
  tina = Turtle()
  n = 200
  for i in range(n):
      tina.color = '#ff{0:02x}00'.format(i*256//n)
      tina.forward(150)
      tina.back(150)
      tina.right(360./n)
  libmanex.end(tp)

def ex_13():
  """left():
  Disegna una stellina dato il numero e la lunghezza dei raggi."""
  def stella(num, lung):
    """Disegna una stella con enne raggi lunghi elle"""
    for i in range(num):
        tina.forward(lung)
        tina.back(lung)
        tina.right(360./num)

  tp = TurtlePlane()
  tina = Turtle()
  stella(20, 50)
  libmanex.end(tp)

def ex_14():
  """right():
  Disegna un albero binario con una procedura ricorsiva."""
  def albero(lung=100):
      """Albero binario"""
      if lung > 2:
          tina.forward(lung)
          tina.left(45)
          albero(lung*0.7)
          tina.right(90)
          albero(lung*0.6)
          tina.left(45)
          tina.back(lung)

  tp = TurtlePlane()
  tina = Turtle(y=-100, d=90)
  albero()
  libmanex.end(tp)

def ex_15():
  """up():
  Trasla la tartaruga delle componenti x e y 
  relative alla propria direzione."""
  def sposta(x, y):
      """Trasla Tartaruga delle componenti x e y"""
      tina.up()
      tina.forward(x)
      tina.left(90)
      tina.forward(y)
      tina.right(90)
      tina.down()

  tp = TurtlePlane()
  tina = Turtle()
  tina.left(45)
  sposta(100, -50)
  libmanex.end(tp)

def ex_16():
  """down():
  Crea una tartaruga che sa tracciare linee tratteggiate e disegna un
  poligono tratteggiato."""
  class MyTurtle(Turtle):
      """Una nuova classe di tartarughe 
      che traccia anche linee tratteggiate."""
      def tratteggia(self,  lung, pieno=5, vuoto=5):
          """Traccia una linea tratteggiata luga lung. """
          q, r = divmod(lung, pieno+vuoto)
          for cont in range(q):
              self.forward(pieno)
              self.up()
              self.forward(vuoto)
              self.down()
          self.forward(r)

  tp = TurtlePlane(name="Pentagono tratteggiato")
  tina = MyTurtle(color='navy', width=3)
  for cont in range(5):
      tina.tratteggia(87)
      tina.left(72)
  libmanex.end(tp)

def ex_17():
  """write():
  Scrive alcune parole nel piano della tartaruga."""
  tp = TurtlePlane(name='Testo nella finestra grafica', w=400, h=200)
  tina = Turtle()
  position = tina.position
  tina.up()
  tina.position = (-140, 80)
  tina.color = 'green'
  tina.write('Parole')
  tina.color = 'pink'
  tina.write('sovrapposte!')
  tina.position = (-140, -80)
  tina.color = 'red'
  tina.write('Parole ', move=True)
  tina.color = 'orange'
  tina.write('scritte di seguito!')
  tina.position = position
  tina.down()
  libmanex.end(tp)

def ex_18():
  """fill():
  Disegna un quadrato con l'interno colorato."""
  def quadratopieno(lato, coloreinterno):
      """Disegna un quadrato dati il lato e il colore della sup. interna."""
      tina.fill(True)
      for count in range(4):
          tina.forward(lato)
          tina.left(90)
      oldcol = tina.color
      tina.color = coloreinterno
      tina.fill(False)
      tina.color = oldcol

  tp = TurtlePlane()
  tina = Turtle(color='blue', width=3)
  quadratopieno(100, 'green')
  libmanex.end(tp)

def ex_19():
  """tracer():
  Disegna una traccia circolare variando la velocita' e lo spessore."""
  from random import random
  
  tp = TurtlePlane(name="Giro della morte")
  tina = Turtle(y=-180)
  tina.color = (random(), random(), random())
  passi = 72
  angolo = 180./72
  for i in range(passi):
    altezza = (tina.position[1]+200)/20
    tina.width = altezza
    tina.tracer(altezza)
    tina.left(angolo)
    tina.forward(15)
    tina.left(angolo)
  libmanex.end(tp)

def ex_20():
  """position:
  Disegna un inviluppo di triangoli rettangoli."""

  def trirett(cateto1, cateto2):
      """Disegna un triangolo rettangolo dati i due cateti"""
      tina.forward(cateto1)
      a = tina.position
      tina.back(cateto1)
      tina.left(90)
      tina.forward(cateto2)
      tina.position = a
      tina.right(90)
      tina.back(cateto1)

  def inviluppo(l1, l2, inc=10):
      """Disegna un inviluppo di triangoli rettangoli."""
      if l1 < 0: return
      trirett(l1, l2)
      inviluppo(l1-inc, l2+inc)

  tp = TurtlePlane(name="Inviluppo di triangoli rettangoli")
  tina = Turtle(color='turquoise')
  for i in range(4):
      inviluppo(200, 0)
      tina.left(90)
  libmanex.end(tp)

def ex_21():
  """direction:
  Disegna un cielo stellato."""
  from random import random, randrange

  def spostaacaso(raggio):
      """Sceglie una direzione e sposta la tartaruga di una
      lunghezza casuale minore a raggio."""
      tina.up()
      tina.position = (0, 0)
      tina.direction = randrange(360)
      tina.forward(randrange(raggio))
      tina.down()

  def stella(lung, n):
      """Disegna una stellina con n raggi lunghi lung."""
      for cont in range(n):
          tina.forward(lung)
          tina.back(lung)
          tina.left(360./n)

  def cielo():
      """Disegna un cielo stellato"""
      tina.fill(1)
      tina.ccircle(200)
      tina.fill(0)
      for cont in range(100):
          spostaacaso(190)
          tina.color = (random(), random(), random())
          stella(10, 5)

  tp = TurtlePlane()
  tina = Turtle()
  cielo()
  libmanex.end(tp)

def ex_22():
  """color:
  Disegna tre tracce colorate su uno sfondo di altro colore."""

  tp = TurtlePlane()
  fina = Turtle()
  fina.color = 'light blue'        # colore passato per nome
  fina.fill(True)
  fina.ccircle(200)
  fina.fill(False)
  fina.delete()
  tina = Turtle(y=-200, d=100)
  tina.color = (1, 0, 0  )         # tupla che contiene 3 numeri
  gina = Turtle(y=-200, d=120)
  gina.color = (1, 1, 1)           # tupla che contiene 3 numeri
  pina = Turtle(y=-200, d=140)
  pina.color = '#00ff00'           # stringa nel formato '#rrggbb'
  for cont in range(80):
      for t in tp.turtles():
          t.width = t.width+1
          t.forward(4)
          t.right(1)
  libmanex.end(tp)

def ex_23():
  """width:
  Disegna una fila di 8 tra quadrati e cerchi alternati."""
  def quadrato(lato):
      """Traccia un quadrato di lato lato."""
      for cont in range(4):
          tina.forward(lato)
          tina.left(90)

  tp = TurtlePlane()
  tina = Turtle()
  tina.up()
  tina.back(290)
  for i in range(8):
      tina.down()
      if i % 2:
          tina.width = 5
          tina.color = 'orange'
          tina.circle(20)
      else:
          tina.width = 10
          tina.color = 'maroon'
          quadrato(40)
      tina.up()
      tina.forward(80)
  tina.back(350)
  tina.down()
  libmanex.end(tp)

#def ex_23():
#  """width:
#  Modifica a caso gli attributi di Tartaruga e li scrive sullo schermo."""
#  from random import random, randrange
#  from math import pi
#
#  def scrividati(tarta):
#      """Scrive, nel piano, gli attributi della tartaruga."""
#      tarta.up()
#      p = tarta.position
#      tarta.write("La posizione di tina e': ({0}; {1})".format(*tarta.position))
#      tarta.position = (p[0], p[1]-1)
#      tarta.write("La direzione di tina e': {0}".format(tarta.direction))
#      tarta.position = (p[0], p[1]-2)
#      tarta.write("Il colore della tartaruga e': {0}".format(tarta.color))
#      tarta.position = (p[0], p[1]-3)
#      tarta.write("La larghezza della penna e': {0}".format(tarta.width))
#      tarta.position = p
#      tarta.down()
#
#  tp = TurtlePlane(name='Stato della tartaruga',
#                sx=20, sy=20, axes=True, grid=True)
#  tina = Turtle()
#  tina.left(randrange(180))
#  tina.width = randrange(10)+1
#  tina.color = (random(), random(), random())
#  tina.forward(randrange(10))
#  scrividati(tina)
#  gina = tp.newTurtle()
#  gina.radians()
#  gina.left(random()*pi+pi)
#  gina.width = randrange(10)+1
#  gina.color = (random(), random(), random())
#  gina.forward(randrange(7))
#  scrividati(gina)
#  libmanex.end(tp)

def ex_28():
  """circle():
  Disegna 25 archi e 25 segmenti circolari alternati."""
  from random import random, randrange

  tp = TurtlePlane(w=600, h=600)
  tina = Turtle(width=5)
  for j in range(50):
      tina.color = (random(), random(), random())
      tina.up()
      tina.position = (randrange(-250, 250), randrange(-250, 250))
      tina.down()
      tina.fill(j % 2 == 1)
      tina.circle(randrange(25)+25, randrange(360))
      tina.color = (random(), random(), random())
      tina.fill(0)
  libmanex.end(tp)

def ex_29():
  """ccircle():
  Disegna 25 archi e 25 settori circolari alternati."""
  from random import random, randrange

  tp = TurtlePlane(w=600, h=600)
  tina = Turtle(width=5)
  for j in range(50):
      tina.color = (random(), random(), random())
      tina.up()
      tina.position = (randrange(-250, 250), randrange(-250, 250))
      tina.down()
      tina.fill(j % 2 == 1)
      tina.ccircle(randrange(25)+25, randrange(360))
      tina.color = (random(), random(), random())
      tina.fill(0)
  libmanex.end(tp)

def ex_30():
  """radians():
  Vedi l'esempio di ``degree``."""

def ex_31():
  """degrees():
  Disegna un orologio analogico che riporta l'ora del sistema."""
  from time import time, localtime
  from math import pi

  def lancette(d, ore, minuti, secondi):
      """Disegna le lancette di un orologio che segna ore:minuti:secondi"""
      tina.degrees()
      aore = round(90 - ore*30 - minuti*0.5)
      aminuti = round(90 - minuti*6 - secondi*0.1)
      asecondi = round(90 - secondi*6)
      tina.direction = aore
      tina.width = d*0.1
      tina.forward(d*0.6)
      tina.back(d*0.6)
      tina.direction = aminuti
      tina.width = d*0.05
      tina.forward(d*0.8)
      tina.back(d*0.8)
      tina.direction = asecondi
      tina.width = d*0.02
      tina.forward(d*0.9)
      tina.back(d*0.9)

  def quadrante(dimensione):
      """Il quadrante di un orologio"""
      tina.radians()
      tina.ccircle(dimensione)
      d1 = dimensione*0.9
      d2 = dimensione-d1
      for i in range(12):
          tina.up()
          tina.forward(d1)
          tina.down()
          tina.forward(d2)
          tina.up()
          tina.back(dimensione)
          tina.down()
          tina.left(pi/6)
      tina.degrees()

  def orologio(dimensione):
      """Disegna un orologio che indica l'ora corrente."""
      quadrante(dimensione)
      ore, minuti, secondi = localtime(time())[3:6]
      lancette(dimensione, ore, minuti, secondi)

  tp = TurtlePlane(name="Ora esatta")
  tina = Turtle()
  orologio(100)
  libmanex.end(tp)

def ex_32():
  """distance():
  Dirige Tartaruga verso un bersaglio emulando l'olfatto."""
  from random import randrange

  def saltaacaso():
      """Sposta Tartaruga in una posizione casuale."""
      tina.up()
      tina.position = (randrange(-280, 280), randrange(-180, 180))
      tina.down()

  def bersaglio():
      """Disegna un bersaglio"""
      saltaacaso()
      for cont in range(4):
          tina.forward(5)
          tina.back(5)
          tina.left(90)
      larghezza = tina.width
      tina.width = 2
      tina.ccircle(20)
      tina.width = larghezza
      tina.ccircle(10)
      return(tina.position)

  def odore(p):
      """Riporta un numero inversamente proporzionale
      al quadrato della distanza da p"""
      return 1./(tina.distance(p)**2)

  def ricerca_per_odore(bersaglio):
      """Muove Tartaruga in base ad una regola olfattiva"""
      tina.tracer(10)
      ricordo_odore = odore(bersaglio)
      while tina.distance(bersaglio)>10:
          tina.forward(randrange(10))
          nuovo_odore = odore(bersaglio)
          if nuovo_odore < ricordo_odore:
              tina.right(randrange(80))
          ricordo_odore = nuovo_odore

  tp = TurtlePlane("Ricerca in base all'odore")
  tina = Turtle()
  b = bersaglio()
  saltaacaso()
  ricerca_per_odore(b)
  libmanex.end(tp)

def ex_33():
  """dirto():
  Simula l'inseguimento tra un gatto e un topo."""
  from random import randrange

  class Topo(Turtle):
      """Classe che simula il comportamento di un topo"""
      def __init__(self, **args):
          Turtle.__init__(self, **args)
          piano = self._plane
          self.up()
          self.position = (piano._s2x(20), piano._s2y(20))
          self.write("inseguimento in base alla vista")
          self.position = (randrange(120)-60, randrange(80)-40)
          self.down()

      def scappa(self, da):
          """Fa muovere il topo di un passo cercando di scappare da da"""
          dir_gatto = self.dirto(da.position)
          if dir_gatto < 170:
              self.right(randrange(20)-10)
          elif dir_gatto > -170:
              self.left(randrange(20)-10)
          else:
              self.left(randrange(80)-40)
          self.forward(1)

  class Gatto(Turtle):
      """Classe che simula il comportamento di un gatto"""
      def __init__(self, **args):
          Turtle.__init__(self, **args)
          self.color = "red"
          self.width = 3

      def insegui(self, chi):
          """Fa muovere il gatto di un passo come se inseguisse chi
          ritorna 0 se ha raggiunto chi"""
          if self.distance(chi.position) < 3:
              self.position = chi.position
              return 0
          dir_topo = self.dirto(chi.position)
          if dir_topo < 180:
              self.left(randrange(5))
          else:
              self.right(randrange(5))
          self.forward(2)
          return 1

  tp = TurtlePlane()
  topo = Topo()
  gatto = Gatto()
  while gatto.insegui(topo):
      topo.scappa(gatto)
  libmanex.end(tp)

def ex_34():
  """whereis():
  Simula la ricerca in base all'udito."""
  from random import randrange
  from math import pi, sin

  def saltaacaso():
      """Sposta tartaruga in una posizione casuale."""
      tina.up()
      tina.position = (randrange(-280, 280), randrange(-180, 180))
      tina.down()

  def bersaglio():
      """Disegna un bersaglio"""
      saltaacaso()
      for cont in range(4):
          tina.forward(5)
          tina.back(5)
          tina.left(90)
      larghezza = tina.width
      tina.width = 2
      tina.ccircle(20)
      tina.swidth = larghezza
      tina.ccircle(10)
      return(tina.position)

  def intensita_destra(distanza, angolo):
      """Restituisce un numero che indica l'intensita' con cui
      l'orecchio destro ode un suono proveniente da un bersaglio
      posto ad una certa distanza e con un certo angolo"""
      senangolo = sin((angolo % 360)*pi/180)
      if senangolo < 0:
          return -10.*senangolo/(distanza**2)
      else:
          return 5.*senangolo/(distanza**2)

  def intensita_sinistra(distanza, angolo):
      """Restituisce un numero che indica l'intensita' con cui
      l'orecchio sinistro ode un suono proveniente da un bersaglio
      posto ad una certa distanza e con un certo angolo"""
      senangolo = sin((angolo % 360)*pi/180)
      if senangolo > 0:
          return 10.*senangolo/(distanza**2)
      else:
          return -5.*senangolo/(distanza**2)

  def ricerca_per_udito(bersaglio):
      """Simula la ricerca di un bersaglio utilizzando l'udito"""
      tina.tracer(10)
      while tina.distance(bersaglio)>10:
          d, a = tina.whereis(bersaglio)
          tina.forward(randrange(5))
          if intensita_destra(d, a) > intensita_sinistra(d, a):
              tina.right(randrange(30))
          else:
              tina.left(randrange(30))

  tp = TurtlePlane("Ricerca in base all'udito")
  tina = Turtle()
  b = bersaglio()
  saltaacaso()
  ricerca_per_udito(b)
  libmanex.end(tp)

def ex_35():
  """lookat():
  Disegna una parabola per mezzo dell'inviluppo di rette."""

  def asse(punto):
      """Disegna l'asse del segmnento che ha per estremi Tartaruga e punto"""
      direzione = tina.direction
      position = tina.position
      tina.up()
      tina.lookat(punto)
      tina.forward(tina.distance(punto)/2.)
      tina.left(90)
      tina.back(1000)
      tina.down()
      tina.forward(2000)
      tina.up()
      tina.position = position
      tina.direction = direzione
      tina.down()

  tp = TurtlePlane(name="Parabola formata da un inviluppo di rette")
  tina = Turtle()
  fuoco = (0, 50)
  numpassi = 80
  lato = 5
  tina.up()
  tina.back(200)
  tina.down()
  for i in range(numpassi):
      asse(fuoco)
      tina.forward(lato)
  libmanex.end(tp)

def ex_36():
  """hide():
      Vedi il metodo ``show``."""

def ex_37():
  """show:
  Traccia una linea mostrando e nascondendo Tartaruga."""
  tp = TurtlePlane("Cucu")
  tina = tp.newTurtle()
  tina.up()
  tina.back(295)
  tina.down()
  tina.tracer(10)
  for i in range(15):
      tina.forward(19)
      tina.hide()
      tina.forward(19)
      tina.show()
  libmanex.end(tp)

def ex_38():
  """clone():
  Fa collaborare alcune tartarughe per disegnare un albero."""
  tp = TurtlePlane("Albero realizzato da una famiglia di tartarughe",
                   w=500, h=500)
  Turtle(y=-200, d=90, color='olive drab')
  dim = 100
  angl = 25
  angr = 35
  while dim > 5:
      for t in tp.turtles():
          t.width = dim//5
          t.forward(dim)
          t1 = t.clone()
          t.left(angl)
          t1.right(angr)
      tp.after(200)
      dim *= 0.7
      print(len(tp.turtles()))
  for t in tp.turtles():
      t.color = 'lime green'
  libmanex.end(tp)

def ex_39():
  """delete():
  Crea 36 tartarughe all'interno dello stesso piano mettendole in una lista,
  poi le fa muovere."""

  tp = TurtlePlane("36 tartarughe che eseguono gli stessi comandi",
                   w=500, h=500, sx=20, sy=20, grid=True)
  n = 36
  l = 4.
  for i in range(n):
      Turtle(color='#ff{0:02x}00'.format(i*256//n), d=(360/n*i), width=5)
  for t in tp.turtles(): t.forward(l)
  for t in tp.turtles(): t.left(90)
  for t in tp.turtles(): t.forward(l/4)
  for t in tp.turtles(): t.right(45)
  for t in tp.turtles(): t.forward(l)
  for t in tp.turtles(): t.right(120)
  for t in tp.turtles(): t.forward(l/2)
  for t in tp.turtles(): t.left(60)
  for t in tp.turtles(): t.forward(l/2)
  tp.after(500)
  tp.clean()
  for t in tp.turtles(): t.forward(2)
  tp.after(500)
  tp.reset()
  for t in tp.turtles(): t.forward(5)
  for t in tp.turtles():
      tp.after(100)
      t.delete()
  tp.after(500)
  tp.clean()
  libmanex.end(tp)

###
# Main
##
#ex_27()
#ex_39()
libmanex.doall(locals(), 'ex_')
