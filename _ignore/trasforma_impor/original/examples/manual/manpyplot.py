# -*- coding: iso-8859-15 -*-
#-------------------------------python--------------------manpyplot.py--#
#                                                                       #
#        Esempi del manuale relativi al plotter di funzioni             #
#                                                                       #
#--Daniele Zambelli-----------Licence GPL------------------------2010---#

'''
pyplot examples for manual.
'''

from __future__ import print_function
import libmanex
from pyplot import PlotPlane, version, Plot
import math

def ex_00():
  """version():
      Controllare la versione della libreria."""
  pp = PlotPlane()
  if version()<"02.05.01":
      print("versione un po' vecchiotta")
  else:
      print(version(), "versione buona!")
  libmanex.end(pp)

#####################################
#        ``class PlotPlane``        #
#        -------------------        #
#####################################

def ex_011():
  """__init__():
  Diversi modi per costruire un oggetto della classe ``Plot``."""
  pp = PlotPlane("Piano 1")
  plot = Plot(color='blue', width=2)
  plot.xy(lambda x: x*x)
  libmanex.end(pp)

def ex_012():
  """__init__():
  Diversi modi per costruire un oggetto della classe ``Plot``."""
  pp = PlotPlane("Piano 2")
  plot = pp.newPlot(color='red', width=2)
  plot.xy(lambda x: x*x)
  libmanex.end(pp)

#####################################
#        ``class Plot``             #
#        --------------             #
#####################################

def ex_11():
  """__init__:
  Disegnare una parabola e un poligono."""
  def parabola(x):
      return x**2-7

  pp = PlotPlane('x^2-7')
  plot = Plot(color='red', width=2)
  plot.xy(parabola)
  plot.drawpoly(((-1, -6), (1, -6), (3, 2), (-3, 2)), 'gray')
  libmanex.end(pp)

def ex_121():
  """xy():
  Disegnare la funzione: y=sin(1./x)."""
  def fun(x):
      return math.sin(1./x)

  pp = PlotPlane('sin(1./x)', w=600, h=250, sx=100, sy=100)
  plot = Plot(color='green', width=2)
  plot.xy(fun)
  libmanex.end(pp)

def ex_122():
  """xy():
  Disegnare la funzione: y=sin(1./x) usando una funzione lambda."""
  pp = PlotPlane('sin(1./x)', w=600, h=250, sx=100, sy=100)
  plot = Plot(color='brown', width=2)
  plot.xy(lambda x: math.sin(1./x))
  libmanex.end(pp)

def ex_13():
  """yx():
  Disegnare una parabola con l'asse di simmetria parallelo all'asse y 
  e una con l'asse di simmetria parallelo all'asse x."""
  def parabola(var):
      return .3*var**2+3*var+4

  pp = PlotPlane("xy e yx", w=600, h=600)
  plot = Plot(width=2)
  plot.xy(parabola, color='green')
  plot.yx(parabola, color='magenta')
  libmanex.end(pp)

def ex_14():
  """polar():
  Disegna tre funzioni polari in tre finestre diverse."""
  pp0 = PlotPlane("quadrifoglio", w=300, h=300)
  pp1 = PlotPlane("cardioide", w=400, h=400)
  pp2 = PlotPlane("spirale", w=500, h=500)
  plot0 = pp0.newPlot(color='navy', width=2)
  plot1 = Plot(plane=pp1, color='maroon', width=4)
  plot2 = Plot(color='gold', width=6)
  plot0.polar(lambda th: 5*math.cos(2*th))
  plot1.polar(lambda th: 8*math.sin(th/2))
  plot2.polar(lambda th: th, 720)
  libmanex.end(pp0)

def ex_15():
  """param():
  Disegnare una funzione parametrica."""
  pp = PlotPlane("param", w=500, h=500)
  plot = Plot(color='blue', width=3)
  plot.param(lambda t: 7*math.cos(3*t/180.)+3*math.cos(3*t/180.+8*t/180.),
             lambda t: 7*math.sin(3*t/180.)-3*math.sin(3*t/180.+8*t/180.),
             0, math.pi*180*2)
  libmanex.end(pp)

def ex_16():
  """ny():
  Disegnare alcune successioni."""
  pp = PlotPlane("ny", sx=10, sy=10, ox=10)
  plot = Plot(color='blue', width=3)
  plot.ny(lambda n: n/2+3)
  plot.ny(lambda n: 3*math.sin(3*n), trace=True, values=True, color='gold')
  plot.ny(lambda n: (-10*n-6)/(n), trace=True, color='maroon')
  libmanex.end(pp)

def ex_17():
  """succ():
  Disegnare alcune successioni."""
  def grandine(an):
      if an % 2: return 3*an+1
      else:      return an//2
    
  pp = PlotPlane("succ", sx=10, sy=10, ox=10)
  plot = Plot(color='green', width=3)
  plot.succ(7, grandine, trace=True, values=True)
  plot.succ(2, lambda an, n: an/10-an, trace=True, 
            values=True, color='blue')
  plot.succ(-2, lambda an, n: an-(n*2-1))
  plot.succ(18, lambda an, n: an**0.5-(-1)**(n+1)*2./3+3, 
            trace=True, values=True, color='maroon')
  plot.succ(50, lambda an, n, a0: (an+a0/an)/2, values=True, color='pink')
  libmanex.end(pp)

###
# Main
##
#ex_17()
#ex_012()
libmanex.doall(locals(), 'ex_')
