I primi comandi
===============
*Dove vengono presentati: il comando print, gli oggetti numero e stringa e le
variabili. E dove si vede anche come si fa a ripetere più volte un comando.*

Comandi ed errori
-----------------
Quando avviamo ``IDLE`` si apre una *shell*, una finestra dove si possono 
eseguire dei comandi. La ``shell di IDLE``, è caratterizzata dalla presenza 
di 3 simboli di maggiore all'inizio della riga::

  >>>

Questi simboli sono il *prompt* di ``Python`` e stanno per la domanda:

*Cosa devo fare?*

Se scriviamo qualcosa e premiamo il tasto ``<invio>`` ``Python`` interpreta
quello che abbiamo scritto come un comando e cerca di eseguirlo.
Non sempre ci riesce! In questo caso ci avvisa con un messaggio
(chissà perché, scritto in inglese). Ad esempio::

  >>> salta
  Traceback (most recent call last):
  File "<pyshell#47>", line 1, in ?
      salta
  NameError: name 'salta' is not defined

In pratica ci comunica che non sa cosa voglia dire "salta". Nel messaggio
di errore, le prime righe indicano dove è avvenuta l'incomprensione, possono
essere utili quando si lavora ad un programma lungo, la riga significativa per
noi è l'ultima.

.. note::
  a seconda della versione di ``Python`` utilizzata ci sono delle differenze;
  se sono trasurabili le trascurerò, altrimenti riporterò sia il comportamento
  di ``Python 2.x`` sia quello di ``Python 3.x``.

Si può provare qualche altro esempio::

  >>> dammi 10 euri
  SyntaxError: invalid syntax

Questa riga non riesce proprio ad interpretarla. Peccato!
Scrivendo programmi ci si imbatte spesso in messaggi di questo genere. Anche
se all'inizio è un po' pesante, bisogna abituarsi a leggerli per capire come
mai l'interprete non fa quello che noi volevamo. Con il tempo si impara a
individuare rapidamente all'interno dei messaggi le informazioni interessanti.
Per riuscire ad ottenere qualcosa di significativo dovremmo operare qualche
cambiamento al nostro modo di dare i comandi. Per prima cosa teniamo conto
che, come i messaggi di errore, anche i comandi di ``Python`` sono tutti in
inglese, poi bisogna conoscere quali sono i comandi che ``Python`` conosce.

Vediamone alcuni.

print
-----

Un primo comando che è utile conoscere è: ``print <qualcosa>``. Esempio::

  #versione 2.x
  >>> print 45
  45

  #versione 3.x
  >>> print(45)
  45

``<qualcosa>`` può essere un numero. Se al posto di un numero però scrivo un
nome, ottengo un errore:

.. note::
  in ``Python 2.x`` ``print`` è un comando che deve essere seguito
  dall'oggetto che vogliamo venga visualizzato sullo schermo,
  in ``Python 3.x``, ``print`` è una funzione che vuole come argomento,
  tra parentesi, l'oggetto da visualizzare.

::

  #versione 2.x
  >>> print Mario
  Traceback (most recent call last):
    File "<pyshell#1>", line 1, in ?
      print Mario
  NameError: name 'Mario' is not defined

  #versione 3.x
  >>> print(Mario)
  Traceback (most recent call last):
    File "<pyshell#6>", line 1, in <module>
      print(Mario)
  NameError: name 'Mario' is not defined

In pratica l'interprete ha cercato il nome "Mario" nel suo vocabolario e non
l'ha trovato. Per stampare un nome o una frase qualsiasi devo utilizzare le
virgolette::

  #versione 2.x
  >>> print "Mario"
  Mario

  #versione 3.x
  >>> print("Mario")
  Mario

Così il comando funziona! Una sequenza di caratteri racchiusa tra virgolette
(semplici o doppie) si chiama "stringa".

.. note::
  IDLE ci mette a disposizione un semplice meccanismo per riscrivere, ed
  eventualmente modificare, dei comandi dati in precedenza: basta portare il 
  cursore sul comando e premere il tasto ``<Invio>``. Quindi, se volessimo 
  modificare il comando precedente, possiamo portare il cursore (con il mouse 
  o con i tasti freccia) sul comando da modificare, premere ``<Invio>`` e 
  fare i cambiamenti che vogliamo, poi farlo eseguire premendo di nuovo
  ``<Invio>``::

    #versione 2.x
    >>> print "Mario ha 45 anni"
    Mario ha 45 anni

    #versione 3.x
    >>> print("Mario ha 45 anni")
    Mario ha 45 anni

Il comando ``print`` è il più semplice modo che un programma ha per
comunicarci qualcosa.

Variabili
---------

Un elemento fondamentale della programmazione è costituito dalle variabili.
Una variabile è una *parola* (in termini tecnici: *identificatore*) a cui è
collegato un *oggetto*.
Questo "oggetto" può, ad esempio, essere un numero o una stringa.
Per associare un *oggetto* ad un *identificatore* si utilizza l'operatore di
assegnazione: "=". Per tirar fuori qualcosa basta scrivere il nome della
variabile. Esempio::

  >>> nome = "Mario"

Ora all'identificatore "nome" è associata la stringa "Mario".

  #versione 2.x
  >>> print nome
  Mario

  #versione 3.x
  >>> print(nome)
  Mario

Il comando ``print`` può accettare più oggetti separati da virgole, li stamperà
uno di seguito all'altro::

  >>> eta = 45

Ora all'identificatore "eta" è associata la stringa "Mario".

  #versione 2.x
  >>> print nome, "ha", eta, "anni."
  Mario ha 45 anni.

  #versione 3.x
  >>> print(nome, "ha", eta, "anni.")
  Mario ha 45 anni.

Il comando ``print``, quando viene utilizzato con più oggetti, mette
automaticamente uno spazio tra un oggetto e l'altro. Non sempre questo
comportamento ci va bene. A volte desideriamo stampare due stringhe una di
seguito all'altra senza spazi in mezzo. Ad esempio::

  >>> primo_pezzo="mario"
  >>> secondo_pezzo="netta"

  #versione 2.x
  >>> print primo_pezzo, secondo_pezzo
  mario netta

  #versione 3.x
  >>> print(primo_pezzo, secondo_pezzo)
  mario netta

Se voglio che le stringhe vengano stampate senza spazi in mezzo, dovrò
concatenarle. L'operatore di concatenazione di stringhe è il simbolo: "+".
Esempio::

  #versione 2.x
  >>> print primo_pezzo + secondo_pezzo
  marionetta

  #versione 3.x
  >>> print(primo_pezzo + secondo_pezzo)
  marionetta

Iterazione
----------

Spesso i programmi devono ripetere più volte le stesse istruzioni. Ad esempio
se dovessi far stampare tre volte “Ciao” potrei dare il comando::

  #versione 2.x
  >>> print "Ciao"; print "Ciao"; print "Ciao"
  Ciao
  Ciao
  Ciao

  #versione 3.x
  >>> print("Ciao"); print("Ciao"); print("Ciao")
  Ciao
  Ciao
  Ciao

Notare che diverse istruzioni devono essere separate o da un ``<Invio>`` o
da un ";".
Questo metodo però non risulta affatto comodo se il numero di ripetizioni di
un comando è elevato.
Tutti i linguaggi di programmazione mettono a disposizione diversi comandi
per ripetere un gruppo di istruzioni. Un modo per far scrivere 3 volte "Ciao"
a ``Python`` è::

  #versione 2.x
  >>> for cont in range(3):
          print "Ciao"

  #versione 3.x
  >>> for cont in range(3):
          print("Ciao")

..note::
  Attenzione ai due punti che terminano la prima riga. Incontrando il
  simbolo ":", l'interprete non esegue il comando ma va a capo spostando
  il cursore verso destra in modo da permettere di scrivere il blocco di
  istruzioni da ripetere.

Ora, premendo una prima volta il tasto ``<Invio>`` non succede niente, perché
venga eseguito il comando, si deve premere una seconda volta ``<Invio>``.
Analizziamo il comando ``for`` che è piuttosto complesso::

  for <variabile> in range(<numero>):
      <comandi da ripetere>

* ``for`` è il nome del comando;
* ``cont`` è il nome di una variabile, al posto di ``cont`` potrei scrivere
  qualunque altro nome. In questa variabile vengono messi, uno alla volta
  i valori restituiti dalla funzione range. In questo caso ``cont`` non viene
  usata, ma spesso è utile avere una variabile che contiene, ad ogni
  ciclo, un valore diverso;
* ``in`` fa parte del comando ``for``;
* ``range`` fornisce l'elenco dei numeri interi tra 0 compreso e il numero
  tra parentesi escluso;
* ``:`` alla fine di questa riga c'è il carattere due punti, indica che
  di seguito è scritta la porzione di codice che verrà ripetuta;
* ``print "Ciao"`` (o ``print("Ciao")``) è la parte di codice che viene
  ripetuta, deve essere *indentata* cioè deve essere scritta più a destra
  rispetto al comando for.
  Può essere formata da più righe, in questo caso devono avere tutte lo
  stesso rientro (trovarsi allo stesso livello di indentazione).

Da questo esempio può non apparire in modo molto chiaro quanto sia comodo
il comando for, ma se il numero di ripetizioni fosse molto più elevato, o il
codice che deve essere ripetuto più complesso, si vedrebbe immediatamente la
differenza. Consideriamo ad esempio il seguente codice::

  >>> for cont in range(3000):
          print "Ciao"

nessuna persona normale si metterebbe a scrivere a mano i singoli comandi per
ottenere lo stesso risultato delle due righe precedenti.

Per vedere il contenuto della variabile ``cont`` durante l'esecuzione del ciclo
possiamo dare il comando::

  #versione 2.x
  >>> for cont in xrange(8):
          print "cont vale", cont

  #versione 3.x
  >>> for cont in xrange(8):
          print("cont vale", cont)

  cont vale 0
  cont vale 1
  cont vale 2
  cont vale 3
  cont vale 4
  cont vale 5
  cont vale 6
  cont vale 7
  >>>

Se volessi un elenco dei primi otto interi e dei loro quadrati e dei loro cubi::

  #versione 2.x
  >>> for n in range(8):
          print n, "\t", n*n, "\t", n*n*n

  #versione 3.x
  >>> for n in range(8):
          print(n, "\t", n*n, "\t", n*n*n)

  0	0	0
  1	1	1
  2	4	8
  3	9	27
  4	16	64
  5	25	125
  6	36	216
  7	49	343
  >>>

.. note::
  alcune stringhe non vengono visualizzate come le abbiamo scritte, ma vengono
  interpretate ad esempio al posto di "\t" verrà visualizzato un salto di 
  tabulazione, al posto di "\n" viene eseguito un *a capo*.

Riassunto
---------

* Nell'ambiente IDLE si dà un comando a ``Python`` scrivendolo dopo il prompt
  e terminandolo con il tasto ``<Invio>``
* Se ``Python`` non è in grado di eseguire un nostro comando ci avvisa con
  un messaggio.
* I più semplici oggetti che possiamo trovare in ``Python`` sono:
  i numeri e le stringhe.
* Le stringhe sono formate da una sequenza di caratteri racchiusa tra apici
  doppi o singoli.
* Una variabile, in ``Python`` è l'associazione tra un ``identificatore``,
  un nome, e un ``oggetto``.
* Si può assegnare un oggetto ad un ``identificatore`` utilizzando
  l'operatore di assegnazione: "=".
* Si ottiene l'oggetto associato ad un identificatore scrivendo semplicemente
  l'identificatore.
* Il comando ``print`` scrive uno o più oggetti sullo schermo.
* Si possono concatenare delle stringhe utilizzando l'operatore di
  concatenazione: "+".
* Posso portare il cursore sulla riga che contiene un comando già scritto,
  premendo una volta ``<Invio>`` il comando viene ricopiato in fondo, posso
  quindi modificarlo e rieseguirlo.
* Per ripetere più volte le stesse istruzioni posso utilizzare il comando::

    for <identificatore> in range(<numero>):
        <istruzioni>
