La grafica della tartaruga
==========================

*Dove si parla di come generare tartarughe virtuali e mandarle in giro per lo
schermo a disegnare.*

Creare una *tartaruga*
----------------------

Per giocare con la grafica della tartaruga bisogna procurarci... una 
tartaruga. Ci sono molti modi per farlo, verranno illustrati più avanti, qui 
ne vediamo uno, il più comune.

I passaggi necessari, in sintesi, sono::
# caricare la libreria che contiene gli oggetti della grafica della tartaruga,
# creare una finestra dove far vivere delle tartarughe,
# creare una tartaruga,
# scrivere i comandi che diamo alla tartaruga,
# rendere attiva la finestra creata.

Di seguito sono illustrati i precedenti passaggi.

Per utilizzare la libreria della grafica della Tartaruga dobbiamo dire a
``Python`` di caricarla in memoria. In realtà, di tutta la libreria ci
interessa, per ora, solo l'ambiente dove vivono le tartarughe. Per caricare
questo ambiente dobbiamo scrivere::

  >>> from pyturtle import TurtlePlane

Questa istruzione legge dalla  libreria ``pyturtle.py`` la classe che permette
di creare un territorio dove vivono tartarughe. Se non sono apparsi messaggi 
di errore vuol dire che ``Python`` ha caricato correttamente la *classe*
``TurtlePlane``. Attenzione questo comando non crea un ``TurtlePlane``, ma 
carica le istruzioni per poterlo creare. Questo vuol dire che ora ``Python`` 
è in grado di creare un oggetto di questa classe, che è proprio quello che ci 
serve per eseguire il punto 2 della precedente lista.

Per crearne un oggetto della classe ``TurtlePlane`` dobbiamo 
inventarci un nome, ad esempio ``tp`` e dare il comando::

  >>> tp = TurtlePlane()

.. note:
  Prestare molta attenzione alle maiuscole e minuscole e alle parentesi.
  
Ora che abbiamo una superficie adatta possiamo passare al punto 3: 
dobbiamo creare una tartaruga. Ci sono diversi modi per farlo, verranno
illustrati nel capitolo reltivo alla libreria ``pyturtle``, qui propongo il 
più semplice. Come prima, dobbiamo intanto pensare un nome, 
ad esempio ``tina``, e dare il comando::

  >>> tina = Turtle()
  
Riassumendo:

* ``tp`` è un piano dove vivono tartarughe;
* ``tina`` è una tartaruga che vive nel piano ``tp``.

.. note:
  Questa versione della grafica della tartaruga è piuttosto spartana, la 
  nostra tartaruga ``tina`` è quel triangolino che si vede al centro del 
  piano delle tartarughe.


A questo punto si possono esplorare i principali comandi della geometria della
tartaruga.
La tartaruga "tina" è ora a nostra disposizione per gli esperimenti. Possiamo
provare a darle qualche comando::

  >>> tina.forward(100)

``tina`` si sposta avanti di 100 passi lasciando una traccia. ::

  >>> tina.right(90)

Non si *sposta* verso destra, ma *ruota* verso destra di 90 gradi. E per
ruotare verso sinistra? ::

  >>> tina.left(90)

Esatto! Ripetiamo ora uno spostamento e una rotazione per un po' di volte::

  >>> for i in range(4):
          tina.forward(100)
          tina.right(90)

``tina`` disegna un quadrato. ::

  >>> tina.up()
  >>> tina.back(200)
  >>> tina.down()

Solleva la penna si sposta indietro di 200 passi e riappoggia la penna, in
questo modo si può far muovere Tartaruga senza disegnare. ::

  >>> tina.color = "blue"

Cambia il colore della penna. ::

  >>> for i in range(180):
          tina.forward(80)
          tina.back(78)
          tina.left(2)

Altro disegno, questa volt blu... Spostiamo ancora ``tina``::

  >>> tina.up()
  >>> tina.back(100)
  >>> tina.down()

Poi cambiamo il colore della penna::

  >>> tina.color = (1, 0, 0)

Si può assegnare il colore della penna o usando un nome di colore in inglese
o fornendo tre numeri compresi tra 0 e 1 ragruppati in una parentesi tonda. 
Il primo indica la componente di rosso, il secondo di verde, il terzo di blu
(o in un terzo modo che è spiegato più avanti).
E ora proviamo a fare una stella con 100 raggi::

  >>> for i in range(100):
          tina.forward(80)
          tina.back(80)
          tina.left(360/100)

I raggi sono proprio 100 (li ho contati!), ma la stella non appare affatto 
regolare, ne manca un pezzo! 

..note:
  se stai lavorando con ``Python 3`` non hai questo problema, perché in 
  ``Python 3`` la divisione tra due interi dà come risultato un numero 
  decimale. (Se stai lavorando con ``Python 2`` i prossimi 3 paragrafi 
  fanno per te).

Questo è dovuto allo strano modo di fare matematica di ``Python 2``.
Se in una divisione tutti gli operandi sono interi il risultato sarà troncato
all'intero::

  >>> print 360/100
  3

Se voglio un risultato più preciso almeno uno degli operandi deve essere un
numero decimale::

  >>> print 360.0/100
  3.6

o anche semplicemente::

  >>> print 360./100
  3.6

Ora spediamo ``tina`` in un altro punto dello schermo e riproviamo::

  >>> tina.up()
  >>> tina.forward(300)
  >>> tina.down()
  >>> for i in range(100):
          tina.forward(80)
          tina.back(80)
          tina.left(360.0/100)

Ora disegna una stella regolare!
Dopo un po' che si fanno dei tentativi con i comandi grafici può succedere
di avere la finestra grafica così piena di scarabocchi da non capirci più
niente. Un altro comando fondamentale è ``reset``. Questo è un metodo del
piano della tartaruga, ripulisce la finestra grafica e rimette a posto
Tartaruga::

  >>> tp.reset()

Per quanto riguarda l'ultimo punto, rendere attivo il piano della tartaruga, 
verrà illustrato più avanti.

Gli oggetti della classe Turtle, oltre ad avere dei metodi 
(come ``forward``, ``right``, ...) che corrispondono a delle azioni hanno 
anche degli attributi che corrispondono a delle proprietà, delle 
caratteristiche. Ad esempio ogni tartaruga ha un colore e uno spessore della 
sua penna. Ad esempio::

  >>> tp.reset()
  >>> tina.color = "green"
  >>> tina.width = 4
  >>> for i in range(20):
          tina.forward(80)
          tina.back(80)
          tina.left(360.0/20)


Riassumendo
-----------

* Per lavorare con la grafica della tartaruga posso scrivere::

    >>> from pyturtle import TurtlePlane
    >>> tp = TurtlePlane()
    >>> tina = Turtle()

  ottengo così due oggetti: un piano delle tartarughe (``tp``) e una 
  tartaruga (``tina``).
* I comandi fondamentali per far muovere la tartaruga creata e disegnare 
  qualcosa sono::

    <tartaruga>.forward(<numero>)
    <tartaruga>.back(<numero>)
    <tartaruga>.left(<numero>)
    <tartaruga>.right(<numero>)
    <tartaruga>.up()
    <tartaruga>.down()
    <piano>.reset()
* Gli oggetti della classe ``Turtle``, hanno anche diversi attributi, due di
  questi sono: ``width`` e ``color``, la sintassi per modificare questi
  attributi è::

    <tartaruga>.width = <numero>
    <tartaruga>.color = <colore>
