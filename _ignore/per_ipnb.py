def in_interactive_session():
    """ check if we're running in an interactive shell

    returns True if running under python/ipython interactive shell
    """
    def check_main():
        import __main__ as main
        return (not hasattr(main, '__file__') or
                get_option('mode.sim_interactive'))

    try:
        return __IPYTHON__ or check_main()
    except:
        return check_main()


def in_qtconsole():
    """
    check if we're inside an IPython qtconsole
    """
    try:
        ip = get_ipython()
        front_end = (ip.config.get('KernelApp',{}).get('parent_appname',"") or
                         ip.config.get('IPKernelApp',{}).get('parent_appname',""))
        if 'qtconsole' in front_end.lower():
            return True
    except:
        return False
    return False

def in_ipnb():
    """
    check if we're inside an IPython Notebook
    """
    try:
        ip = get_ipython()
        front_end = (ip.config.get('KernelApp',{}).get('parent_appname',"") or
                         ip.config.get('IPKernelApp',{}).get('parent_appname',""))
        if 'notebook' in front_end.lower():
            return True
    except:
        return False
    return False

def in_ipython_frontend():
    """
    check if we're inside an an IPython zmq frontend
    """
    try:
        ip = get_ipython()
        return 'zmq' in str(type(ip)).lower()
    except:
        pass

    return False
