..
   Problema relativo agli apostrofi con sphinx.
   
   Dalla Treccani:
   
   https://treccani.it/enciclopedia/apostrofo_%28La-grammatica-italiana%29/
   
   Ma viene usato anche per indicare alcuni casi di ➔troncamento che danno vita a parole terminanti in vocale, e più precisamente:

   – nella 2a persona dell’imperativo dei verbi dare, dire, fare, stare, andare

   dai	▶ da’
   dici	▶ di’
   fai	▶ fa’
   stai ▶ sta’
   vai	▶ va’

   – in qualche altra parola in cui si verifica il troncamento dell’intera sillaba finale

   bene ▶ be’
   poco ▶ po’
   modo ▶ mo’ (a mo’ di = come)

   – secondo un’abitudine ormai in disuso, in alcune preposizioni articolate nelle quali il troncamento riguarda il secondo elemento di un ➔dittongo

   ai ▶ a’
   dei ▶ de’
   coi ▶ co’



Introduzione
============

*Dove si cerca di giustificare il tempo perso per questo lavoro e dove
viene data qualche idea su come installare l'interprete Python e la
libreria grafica.*

Scopo di Pygraph e di questo manuale
------------------------------------

Mi sono imbattuto in Python mentre cercavo un linguaggio di alto livello, 
multi-piattaforma, con una sintassi semplice. Oltre ad uso personale volevo 
utilizzarlo anche a scuola per insegnare qualche elemento di informatica ai 
miei alunni. Ritenendo che la programmazione di elementi grafici possa 
coinvolgere di più dei semplici comandi di testo, ho cercato le librerie 
grafiche messe a disposizione da questo linguaggio. Con sorpresa e grande 
gioia ho scoperto che una delle librerie ufficiali implementa la grafica 
della tartaruga. Dopo averla utilizzata per un po\' ho sentito la necessità 
di modificarla e darle una connotazione più Object-Orienteed. 
Ho incominciato a pasticciarla, modificarla, adattarla alle mie esigenze 
(grazie software libero!). 
L'ho suddivisa in due parti: 
l'implementazione di un piano cartesiano e della grafica della tartaruga. 
Nel frattempo, sempre per esigenze didattiche ho aggiunto il modulo per 
tracciare funzioni matematiche integrandolo con il piano cartesiano. 
Altro strumento didattico interessante è 
costituito dai programmi di geometria interattiva (Cabri Géomètre, Kig, Dr. 
Geo, GeoGebra, CaR, ...). 
Un forte limite di questi programmi è l'assenza, o il difficile uso, di un 
linguaggio al loro interno, ho pensato quindi di aggiungere
a pygraph una libreria per realizzare figure geometriche interattive.
Il risultato finale è abbastanza diverso dalla libreria turtle.py da cui sono 
partito.

Dopo i primi anni di uso in diverse situazioni - uso personale, con alunni 
di scuola media e di scuola superiore - queste librerie mi sono sembrate 
abbastanza robuste da poter essere proposte ad altre persone interessate a 
Python e alla grafica. Così ho riunito un certo numero di esempi di uso e ho 
scritto questo manualetto per documentarle. 
Ora, dopo altre esperienze, ho rivisto un tutto il materiale per rilasciare una nuova versione.

I primi capitoli di questo testo sono pensati per guidare passo passo alla 
scoperta della programmazione in Python. Non vogliono essere una guida 
completa, per questo ci sono altri lavori ben più importanti del mio, ma 
vorrebbero essere uno stimolo e un'indicazione per iniziare l'esplorazione 
del mondo dei linguaggi di programmazione.

Installazione
-------------

Per installare un qualunque software bisogna essere un po\' pratici di 
computer.
Conviene eventualmente farsi aiutare. L'autore non può essere ritenuto
responsabile della perdita di informazioni conseguenti ad errori di 
installazione.

Installare Python
^^^^^^^^^^^^^^^^^

#. Linux: il programma deve essere installato dall'amministratore del 
   sistema, lui sa come fare. Se si utilizza una distribuzione la cosa 
   più semplice è caricare Python al momento dell'installazione del 
   sistema. 
   O comunque installarlo a partire dai *repository* della distribuzione stessa. Sotto Linux a volte Python è distribuito separatamente da IDLE. 
   In questo caso, per utilizzare l'ambiente di sviluppo IDLE (vedi capitolo 1), bisogna installarlo separatamente.
#. Windows: doppio clic sul pacchetto da installare.
#. La versione più aggiornata di Python può essere scaricata da 
   http://www.python.org.
#. Python è un software libero ed è distribuito sotto una licenza 
   compatibile con la licenza GPL quindi è possibile usarlo, copiarlo e 
   distribuirlo senza restrizioni.

Installare pygraph
^^^^^^^^^^^^^^^^^^

#. Procurarsi il file "pygraph_x.xx" (dove al posto dei vari "x" ci saranno
   delle cifre). Lo si può scaricare a partire dal sito  
   http://bitbucket.org/zambu/pygraph/downloads
#. Scompattare il file all'interno di una directory di lavoro.
#. La directory pygraph contiene le quattro directory seguenti:

   * **doc**: documentazione varia,
   * **examples**: esempi d'uso,
   * **test**: test delle varie funzioni della libreria,
   * **pygraph**: le librerie del progetto:
  
     - **pycart.py**: un piano cartesiano
     - **pyturtle.py**: la grafica della tartaruga
     - **pyplot.py**: grafico di funzioni nel piano, cartesiane e polari
     - **pyig**: geometria interattiva
    
#. Eseguire il comando:
   ``python setup install``
..
   .. note::
   * per fare questo, probabilmente, bisogna avere i privilegi di 
     amministratore; 
   * a seconda della versione di Python installata, pythonx.x 
     potrebbe essere python3.5 o python3.6...; 
   * nella mia versione, Python3.5 sotto Debian, la directory in cui 
     spostare la cartella ``pygraph`` e il file ``pygraph.pth`` si 
     chiama: **dist-packages**.

Trovare documentazione su Python
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. https://www.python.org/
#. https://www.python.it/ 
#. https://forumpython.it/
#. https://pytutorial-it.readthedocs.io/it/python3.9
#. Un'ottima introduzione all'informatica usando questo linguaggio di
   programmazione è il testo: 
   "Pensare da informatico: Imparare con Python" di Downey, Allen 
   tradotto magnificamente in italiano (si trova su internet 
   partendo dai link precedenti).
#. Le dispense di un laboratorio di matematica con Python e pygraph si 
   possono trovare a partire dal sito
   http://www.fugamatematica.blogspot.com.

Riassumendo
^^^^^^^^^^^
Per eseguire i programmi scritti in ``Python`` bisogna installare ``Python`` 
(ma dai!).
Per utilizzare le librerie presenti in ``pygraph`` bisogna installare ``pygraph``.

Licenza
--------

Il manuale e gli esempi sono rilasciati sotto la licenza Creative Commons:
CC-BY-SA.

Feedback
--------

Spero che qualcuno trovi interessante questo lavoro e lo usi per imparare o 
per insegnare l'informatica e la geometria con Python. 
Utilizzandolo, senz'altro verranno alla luce molti errori, difetti o carenze 
sia nel software sia nella documentazione; invito chi troverà qualche motivo 
di interesse in questo lavoro a inviarmi:

#. Commenti,
#. Critiche,
#. Suggerimenti.


Ringrazio chiunque si prenderà la briga di mandarmi qualche riscontro.


Daniele Zambelli email: daniele.zambelli at gmail.com

