Un po\' di vocabolario
=====================

*Dove ci si allontana un dalla tastiera per riflettere e capire meglio.*

Computer
--------

Ora vediamo alcune parole che possono essere utili per capire meglio questo
linguaggio di programmazione.

Possiamo pensare il computer formato da:

* Il microprocessore: un circuito in grado di leggere e modificare il 
  contenuto della memoria RAM  e di eseguire alcune operazioni logiche o 
  matematiche.
* La memoria RAM (Random Acces Memory, Memoria ad Accesso Casuale): 
  contiene le istruzioni che devono essere eseguite dal microprocessore e 
  i dati necessari al programma. La memoria RAM è realizzata con circuiti
  elettronici, ha una velocità paragonabile a quella del microprocessore, è
  volatile cioè quando si spegne il computer, perde irrimediabilmente il
  suo contenuto.
* La memoria di massa: generalmente costituita da dischi magnetici
  (hard disk), ottici (CDRom) o memorie flash (USB Disk), mantiene le
  informazioni memorizzate anche quando viene spento il computer, ma è 
  piuttosto lenta.
* Le periferiche: dispositivi che permettono al computer di comunicare con
  l'esterno: tastiera, monitor, mouse, stampante, modem, ...

Linguaggi di programmazione
---------------------------

Le istruzioni che il microprocessore deve eseguire sono scritte nella 
memoria RAM.

Quando un computer funziona, il microprocessore legge una ad una le 
istruzioni scritte nella memoria RAM e le esegue. Scrivere un programma vuol 
dire mettere nella memoria RAM le istruzioni giuste per far fare al 
microprocessore quello che vogliamo noi. Purtroppo il microprocessore non 
capisce comandi sensati, ma solo il linguaggio macchina che è composto da 
numeri binari cioè scritti usando solo le due cifre: zero e uno.

Perciò non potremmo scrivere nella RAM::

  Somma 3 a 5

ma dovremmo scrivere qualcosa di questo genere::

  1000110011101010
  0000000000000011
  0000000000000101

Ovviamente scrivere un programma, magari fatto da migliaia o milioni di
istruzioni, utilizzando numeri binari è un'impresa da super eroi! I
programmatori hanno ben presto scritto dei programmi che leggono comandi
sensati e li traducono in numeri binari. In questo modo si possono scrivere
programmi utilizzando linguaggi più vicini al linguaggio dell'uomo che a quello
della macchina: linguaggi di alto livello.

Ci sono due modi per trasformare un programma scritto con linguaggio di alto
livello in un programma scritto in linguaggio macchina: tradurlo tutto e poi
eseguirlo oppure tradurre ogni singolo comando ed eseguirlo immediatamente.
Nel primo caso si parla di *compilatori* che traducono tutto un programma
scritto con linguaggio di alto livello trasformandolo in un programma scritto
in linguaggio macchina e perciò eseguibile da un dato computer senza più la
necessità della presenza del compilatore stesso. Nel secondo caso si parla di
*interpreti*: per eseguire un programma interpretato, nel computer deve essere
presente l'interprete del linguaggio che legge, traduce e fa eseguire
istruzione per istruzione.

``Python``
----------

``Python`` è un linguaggio interpretato (in realtà le cose sono un 
po\'più complicate di così): 
non si può eseguire un programma scritto in ``Python``
se sul computer non è presente l'interprete. Il nucleo del linguaggio
``Python`` è piuttosto limitato: può capire e far eseguire pochi (si fa per
dire!) comandi. Ma è dotato di un meccanismo per cui può essere ampliato
all'infinito: si possono scrivere librerie che contengono nuove funzioni e
che allargano le possibilità del linguaggio.
In effetti ``Python`` viene fornito con un gran numero di librerie già 
scritte e notevolmente sicure perché già utilizzate e messe alla prova da 
programmatori di tutto il mondo. Molte di queste librerie sono scritte in 
``Python`` stesso e perciò si possono facilmente studiare e, magari, 
modificare. Altre, per ragioni di efficienza e di velocità, sono scritte 
usando linguaggi compilati.

Le librerie sono dunque dei programmi che ampliano le possibilità del
linguaggio mettendo a disposizione del programmatore delle nuove funzioni non
presenti nel nucleo del linguaggio.

Per utilizzare il contenuto di una libreria si deve dire all'interprete 
che la si vuole usare, il comando per includere nell'interprete le funzioni 
di una libreria è:

  ``>>> import <nome della libreria> [as <abbreviazione>]``

Ad esempio, il comando:

  ``>>> import pygraph.pyturtle as tg``

significa: carica tutta la libreria ``pyturtle`` chiamandola ``tg``.

A questo punto possiamo creare tutti gli oggetti forniti dalla libreria 
``pyturtle``. 
Per incominciare ce ne servono due che vogliamo associare a due nomi::

  >>> piano = tg.TurtlePlane()
  >>> tina = tg.Turtle()

Bisogna prestare attenzione alle maiuscole e alle parentesi. 
Se tutto fila liscio a questo punto appare una nuova finestra sullo schermo 
è l'oggetto *piano della tartaruga* collegato all'identificatore ``piano``.

Con, al centro un triangolino che rappresenta la tartaruga ``tina``.

Ora: ``piano`` è un oggetto della classe ``tg.TurtlePlane`` 
e ``tina`` è un oggetto della classe ``tg.Turtle``.

La classe ``TurtlePlane`` ha un metodo che crea e dà come risultato una 
nuova tartaruga. Per creare una tartaruga si può anche scrivere:

  ``>>> tina = piano.newTurtle()``

Gli oggetti possiedono degli *attributi* e dei *metodi*. Gli *attributi*
sono delle caratteristiche che possono variare da un oggetto ad un altro 
come
il colore, la posizione, lo spessore della penna, ... i *metodi* sono i
comandi che l'oggetto è in grado di eseguire. 

Gli attributi fondamentali della classe ``Turtle`` sono::

  <tartaruga>.color
  <tartaruga>.width
  <tartaruga>.position
  <tartaruga>.direction

e i metodi::

  <tartaruga>.forward(<numero>)
  <tartaruga>.back(<numero>)
  <tartaruga>.left(<numero>)
  <tartaruga>.right(<numero>)
  <tartaruga>.up()
  <tartaruga>.down()
  
Della classe ``TurtlePlane`` abbiamo visto i metodi::

  <piano>.reset()
  <piano>.newTurtle()

Riassumendo
-----------

* ``Python`` è un linguaggio (compilato e) interpretato.
* È arricchito da numerose librerie. Una libreria che contiene la grafica
  della tartaruga è ``pyturtle``.
* Si può caricare una libreria con il comando:

    ``import <nome libreria> [as <abbreviazione>]``
    
  Ad esempio: ``import pygraph.pyturtle as tg``
* Nella libreria ``pyturtle`` sono descritte le classi 
  ``TurtlePlane`` e ``Turtle``.
* È possibile creare un oggetto scrivendo un nome, il simbolo di uguale e
  il nome della classe seguito da una coppia di parentesi tonde. 
  Ad esempio::

    mondoditina = tg.TurtlePlane()
    tina = tg.Turtle()
* È possibile fare eseguire ad un oggetto un suo metodo scrivendo il nome
  dell'oggetto seguito dal punto e dal nome del metodo completo di parentesi
  contenenti, se necessario, uno o più valori (argomenti)::
  
    <oggetto>.<nome_meodo>()
  Ad esempio: ``tina.forward(100)``
